package com.gmail.val59000mc.hcgameslib.api;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public interface HCDependenciesAPI {
	
	public boolean isPermissionsExEnabled();
	
	public boolean isSimpleInventoryGUIEnabled();
	
	public boolean isVaultEnabled();
	
	public boolean isNametagEditEnabled();
	
	// PermissionsEx
	public String getPrefix(Player player);
   
	public  String getSuffix(Player player);

	public String getShortPrefix(Player player);

	public String getStrippedPrefix(Player player);

	public void removePerm(String playerName, String permission);

	public void addPerm(String playerName, String permission);

	public double addMoney(OfflinePlayer player, Double amount);
	public double addMoney(Player player, Double amount);

	double getBalance(OfflinePlayer offlinePlayer);
	public double getBalance(Player player);
	
	public void removeMoney(OfflinePlayer offlinePlayer, Double amount);
	public void removeMoney(Player player, Double amount);

	public boolean hasEnoughMoney(OfflinePlayer offlinePlayer, Double amount);
	public boolean hasEnoughMoney(Player player, Double amount);
	
	
}
