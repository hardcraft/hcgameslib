package com.gmail.val59000mc.hcgameslib.api;

import java.util.List;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.boosters.Booster;
import com.gmail.val59000mc.hcgameslib.boosters.BoosterListResponse;
import com.gmail.val59000mc.hcgameslib.boosters.BoosterResponse;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public interface HCBoostersAPI {
	
	/**
	 * Find player current boosters
	 * current = activated and running + not activated
	 * @param playerId
	 */
	public BoosterListResponse findPlayerCurrentBoosters(int playerId);
	
	/**
	 * Find booster by ids
	 * @param boosterId
	 */
	public BoosterResponse findBoosterById(int boosterId);

	/**
	 * Find player current boosters
	 * current = activated and running + not activated
	 * @param playerName
	 */
	public BoosterListResponse findPlayerCurrentBoostersByPlayerName(String playerName);
	
	/**
	 * Display a list of boosters to a player
	 * @param owner the owner of the boosters
	 * @param hcPlayer the player to display the boosters to
	 * @param boosters the boosters list
	 */
	public void displayBoostersTo(String owner, HCPlayer hcPlayer, List<Booster> boosters);
	
	/**
	 * Load a player's booster into the system if exists
	 * Announce the new booster if it is better than the previous one
	 * Should be called async
	 * @param hcPlayer
	 */
	public BoosterResponse loadActiveBooster(HCPlayer hcPlayer);

	/**
	 * Unload active booster if exists from memory
	 * @param hcPlayer
	 */
	public void unloadActiveBooster(HCPlayer hcPlayer);
	
	/**
	 * Create a new booster in the underlyig database
	 * Should be called async
	 */
	public BoosterResponse createBooster(String playerName, double multiplier, int minutesDuration);
	
	/**
	 * Delete a booster by ud
	 * Should be called async
	 */
	public BoosterResponse removeBooster(int id);
	
	/**
	 * Activate a player's booster
	 * @param hcPlayer
	 * @param booster
	 * @return a message telling the success/failure of the operation
	 */
	public BoosterResponse activateBooster(Booster booster);
	
	/**
	 * Return the best currently loaded booster multiplier into memory
	 * @return
	 */
	public double getCurrentBoosterMultiplier();
	
	/**
	 * Return the best currently loaded booster into memory
	 * @return
	 */
	public Booster getBestBooster();
	
	/**
	 * Return the description associated with the best curently loaded booster into memory
	 * @return
	 */
	public HCMessage getBestBoosterMessage();

	
}
