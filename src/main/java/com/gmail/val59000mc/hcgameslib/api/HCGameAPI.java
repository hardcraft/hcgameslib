package com.gmail.val59000mc.hcgameslib.api;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.hcgameslib.tasks.IHCTask;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Event;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.Callable;
import java.util.function.Consumer;

public interface HCGameAPI {

	
	// Game
	
	/**
	 * Load a game
	 */
	public void loadGame();

	/**
	 * Start or force start the game
	 */
	public void startGame();

	/**
	 * End the game
	 */
	public void endGame(HCTeam winningTeam);
	

	/** 
	 * Stop a continuous game
	 */
	public void stopContinuousGame();


	/**
	 * Get continuous game state
	 * @return
	 */
	public boolean isContinuous();
	
	/**
	 * Get a registered command by class
	 * @param clazz a class that extends HCCommand
	 * @return the registered command or null if no command found
	 */
	<T extends HCCommand> T getRegisteredCommand(Class<T> clazz);
	
	/**
	 * Register a listener
	 * @param listener
	 */
	public void registerListener(HCListener listener);
	
	/**
	 * Unregister a listener
	 * @param listener
	 */
	public void unregisterListener(HCListener listener);

	/** 
	 * Get user or default world config
	 * @return
	 */
	public WorldConfig getWorldConfig();
	
	/**
	 * Get game state
	 * @return
	 */
	public GameState getGameState();
	


	/**
	 * Get initial time before start
	 * @return
	 */
	public int getTimeBeforeStart();

	/**
	 * Get initial time before end
	 * @return
	 */
	public int getTimeBeforeEnd();
	
	/**
	 * Check if countdown end of game is enabled
	 */
	public boolean isCountdownEndOfGameEnabled();

	/**
	 * Get remaining time before start
	 * @return
	 */
	public int getRemainingTimeBeforeStart();

	/**
	 * Get remaining time before end
	 * @return
	 */
	public int getRemainingTimeBeforeEnd();
	
	/**
	 * Get players manager
	 * @return
	 */
	public HCPlayersManagerAPI getPlayersManagerAPI();
	
	/**
	 * Get boosters api
	 * @return
	 */
	public HCBoostersAPI getBoostersAPI();

	/**
	 * Get donations api
	 * @return
	 */
	public HCDonationsAPI getDonationsAPI();
	
	/**
	 * Get flows api
	 * @return
	 */
	public HCFlowsAPI getFlowsAPI();
	
	/**
	 * Get Strings api
	 */
	public HCStringsAPI getStringsAPI();
	
	/**
	 * Get Bungee api
	 */
	public HCBungeeAPI getBungeeAPI();
	

	/**
	 * Get Sound api
	 */
	public HCSoundAPI getSoundAPI();
	

	/**
	 * Get Sound api
	 */
	public HCMySQLAPI getMySQLAPI();

	/**
	 * Get plugin's callbacks
	 * @return
	 */
	public HCPluginCallbacksAPI getCallbacksApi();

	/**
	 * Get dependencies
	 * @return
	 */
	public HCDependenciesAPI getDependencies();

	/**
	 * Get items api
	 * @return
	 */
	public HCItemsAPI getItemsAPI();
	
	/**
	 * Get game plugin instance
	 */
	public JavaPlugin getPlugin();
	
	/** 
	 * Get Config
	 * @return
	 */
	public FileConfiguration getConfig();
	

	/**
	 * Force saving the provided config file
	 */
	public void saveConfig();
	
	/**
	 * Get task scheduler
	 * @return
	 */
	public HCTaskScheduler.Builder buildTask(String name, HCTask callback);
	
	/**
	 * Get task scheduler
	 * @return
	 */
	public HCTaskScheduler.Builder buildTask(String name, IHCTask callback);

	/**
	 * Get game name
	 * @return
	 */
	public String getName();
	
	/**
	 * Get game reduced name
	 * @return
	 */
	public String getLowercaseName();
	
	/**
	 * Get mysql game name as in database
	 * @return
	 */
	public String getMySQLGameName();

	/**
	 * Evaluate the state of game
	 * @param state
	 * @return
	 */
	public boolean is(GameState state);
	
	/**
	 * Run a syncchronous task
	 * @param runnable
	 */
	public void sync(Runnable runnable);
	
	/**
	 * Run a synchronousdelayed task
	 * @param runnable
	 * @param delayTicks
	 */
	public void sync(Runnable runnable, int delayTicks);
	
	/**
	 * Run an asynchronous task
	 * @param runnable
	 */
	public void async(Runnable runnable);
	
	/**
	 * Run an asynchronous delayed task
	 * @param runnable
	 * @param delayTicks
	 */
	public void async(Runnable runnable, int delayTicks);
	
	/**
	 * Run an asynchronoys task and consume its output with a consumer
	 * The callable is called async and the consumer is called sync
	 * after the callable has returned
	 * @param callable
	 * @param consumer
	 */
    public <T> void async(Callable<T> callable, Consumer<T> consumer);
	
	/**
	 * Run an asynchronoys delayed task and consume its output with a consumer
	 * The callable is called async and the consumer is called sync
	 * after the callable has returned
	 * @param callable
	 * @param consumer
	 */
    public <T> void async(Callable<T> callable, Consumer<T> consumer, int delayTicks);
    
    /**
     * Call an event
     * @return
     */
    public <T extends Event> T callEvent(T event);
	
	
	
}
