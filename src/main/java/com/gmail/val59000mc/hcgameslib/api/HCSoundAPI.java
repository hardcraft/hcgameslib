package com.gmail.val59000mc.hcgameslib.api;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Sound;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;

public interface HCSoundAPI {
	/**
	 * Play a sound to all players
	 * @param sound
	 * @param volume
	 * @param pitch
	 */
	public void play(Sound sound, float volume, float pitch);
	
	/**
	 * Play a sound to all players from a location
	 * @param sound
	 * @param location
	 * @param volume
	 * @param pitch
	 */
	public void play(Sound sound, Location location, float volume, float pitch);
	
	/**
	 * Play a sound to a player
	 * @param hcPlayer
	 * @param sound
	 * @param volume
	 * @param pitch
	 */
	public void play(HCPlayer hcPlayer,Sound sound, float volume, float pitch);
	
	/**
	 * Play a sound to a player from a location
	 * @param hcPlayer
	 * @param sound
	 * @param location
	 * @param volume
	 * @param pitch
	 */
	public void play(HCPlayer hcPlayer,Sound sound, Location location, float volume, float pitch);
	
	/**
	 * Play a sound to a team
	 * @param team
	 * @param sound
	 * @param volume
	 * @param pitch
	 */
	public void play(HCTeam team,Sound sound, float volume, float pitch);

	/**
	 * Play a sound to a team from a location
	 * @param players
	 * @param sound
	 * @param volume
	 * @param pitch
	 */
	public void play(HCTeam team,Sound sound, Location location, float volume, float pitch);
	
	/**
	 * Play a sound to some players 
	 * @param players
	 * @param sound
	 * @param volume
	 * @param pitch
	 */
	public void play(List<HCPlayer> players, Sound sound, float volume, float pitch);
	
	/**
	 * Play a sound to some players from a location
	 * @param players
	 * @param sound
	 * @param location
	 * @param volume
	 * @param pitch
	 */
	public void play(List<HCPlayer> players,Sound sound, Location location, float volume, float pitch);
	
	
	
}
