package com.gmail.val59000mc.hcgameslib.api.impl.items;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;

public class ItemStoreBuilder {
	
	private List<Material> materials;
	private List<Integer> amounts;
	private List<Integer> durabilities;
	private List<Integer> durabilityPercentages;
	private List<String> names;
	private List<List<String>> lores;
	private Map<Enchantment, List<Integer>> enchantments;
	private List<Integer> enchantmentsNumbers;
	private List<ItemBuilderModifier> modifiers;
	
	////////////////
	// Constructors
	/////////////////
	
	private ItemStoreBuilder(){
		// no public no-arg constructor
		this.materials = new ArrayList<>();
		this.amounts = new ArrayList<>();
		this.durabilities = new ArrayList<>();
		this.durabilityPercentages = new ArrayList<>();
		this.names = new ArrayList<>();
		this.lores = new ArrayList<>();
		this.enchantments = new HashMap<>();
		this.enchantmentsNumbers = new ArrayList<>();
		this.modifiers = new ArrayList<>();
	}
	
	private ItemStoreBuilder(Material material){
		this();
		this.materials.add(material);
	}
	
	private ItemStoreBuilder(Material... materials){
		this();
		this.materials.addAll(Lists.newArrayList(materials));
	}
	
	public static ItemStoreBuilder from(Material material){
		return new ItemStoreBuilder(material);
	}
	
	public static ItemStoreBuilder from(Material... materials){
		return new ItemStoreBuilder(materials);
	}
	
	////////////
	// accessors
	/////////////
	
	public List<Material> getMaterials() {
		return Collections.unmodifiableList(materials);
	}

	public List<Integer> getAmounts() {
		return Collections.unmodifiableList(amounts);
	}

	public List<Integer> getDurabilities() {
		return Collections.unmodifiableList(durabilities);
	}

	public List<Integer> getDurabilityPercentages() {
		return Collections.unmodifiableList(durabilityPercentages);
	}

	public List<String> getNames() {
		return Collections.unmodifiableList(names);
	}

	public List<List<String>> getLores() {
		return Collections.unmodifiableList(lores);
	}

	public Map<Enchantment, List<Integer>> getEnchantments() {
		return Collections.unmodifiableMap(enchantments);
	}
	
	public List<Integer> getEnchantmentsNumbers() {
		return enchantmentsNumbers;
	}

	public List<ItemBuilderModifier> getModifiers() {
		return modifiers;
	}
	
	
	/////////
	// amount
	/////////

	public ItemStoreBuilder withAmount(Integer amount){
		this.amounts.add(amount);
		return this;
	}

	public ItemStoreBuilder withAmounts(Integer... amounts){
		this.amounts.addAll(Lists.newArrayList(amounts));
		return this;
	}
	
	public ItemStoreBuilder withAmountsRange(Integer min, Integer max){
		for(int i = Math.min(min, max) ; i<= Math.max(min, max) ; i++){
			this.amounts.add(i);
		}
		return this;
	}
	
	public ItemStoreBuilder withRandomAmount(Integer min, Integer max){
		this.amounts.add(Randoms.randomInteger(Math.min(min, max), Math.max(min, max)));
		return this;
	}
	
	public ItemStoreBuilder withRandomAmounts(Integer min, Integer max, Integer count){
		for(int i=0 ; i<count ;i++){
			withRandomAmount(min, max);
		}
		return this;
	}
	
	/////////////
	// durability
	/////////////
	
	public ItemStoreBuilder withDurability(Integer durability){
		this.durabilities.add(durability);
		return this;
	}
	
	public ItemStoreBuilder withDurabilities(Integer... durabilities){
		this.durabilities.addAll(Lists.newArrayList(durabilities));
		return this;
	}
	
	public ItemStoreBuilder withDurabilitiesRange(Integer min, Integer max){
		for(int i = Math.min(min, max) ; i<= Math.max(min, max) ; i++){
			this.durabilities.add(i);
		}
		return this;
	}
	
	public ItemStoreBuilder withRandomDurability(Integer min, Integer max){
		this.durabilities.add(Randoms.randomInteger(Math.min(min, max), Math.max(min, max)));
		return this;
	}
	
	public ItemStoreBuilder withRandomDurabilities(Integer min, Integer max, Integer count){
		for(int i=0 ; i<count ;i++){
			withRandomDurability(min, max);
		}
		return this;
	}
	
	//////////////////
	// remaining lives
	///////////////////
	
	public ItemStoreBuilder withDurabilityPercentage(Integer durabilityPercentage){
		this.durabilityPercentages.add(durabilityPercentage);
		return this;
	}
	
	public ItemStoreBuilder withDurabilityPercentages(Integer... durabilityPercentages){
		this.durabilityPercentages.addAll(Lists.newArrayList(durabilityPercentages));
		return this;
	}
	
	public ItemStoreBuilder withDurabilityPercentageRange(Integer min, Integer max){
		for(int i = Math.min(min, max) ; i<= Math.max(min, max) ; i++){
			this.durabilityPercentages.add(i);
		}
		return this;
	}
	
	public ItemStoreBuilder withRandomDurabilityPercentage(Integer min, Integer max){
		this.durabilityPercentages.add(Randoms.randomInteger(Math.min(min, max), Math.max(min, max)));
		return this;
	}
	
	public ItemStoreBuilder withRandomDurabilityPercentages(Integer min, Integer max, Integer count){
		for(int i=0 ; i<count ;i++){
			withRandomDurabilityPercentage(min, max);
		}
		return this;
	}
	
	/////////////
	// custom name
	/////////////
	
	public ItemStoreBuilder withCustomName(String name){
		this.names.add(name);
		return this;
	}
	
	public ItemStoreBuilder withCustomNames(String... name){
		this.names.addAll(Lists.newArrayList(names));
		return this;
	}
	
	/////////////
	// lore
	/////////////
	
	public ItemStoreBuilder withLore(List<String> lore){
		this.lores.add(lore);
		return this;
	}
	
	public ItemStoreBuilder withLores(List<List<String>> lores){
		this.lores.addAll(lores);
		return this;
	}
	
	/////////////
	// enchantments
	/////////////
	
	public ItemStoreBuilder withEnchantment(Enchantment enchantment, Integer level){
		List<Integer> levels = enchantments.get(enchantment);
		if(levels == null){
			enchantments.put(enchantment, Lists.newArrayList(level));
		}else{
			levels.add(level);
		}
		return this;
	}
	
	public ItemStoreBuilder withEnchantments(Enchantment enchantment, Integer... levels){
		List<Integer> oldLevels = enchantments.get(enchantment);
		if(oldLevels == null){
			enchantments.put(enchantment, Lists.newArrayList(levels));
		}else{
			oldLevels.addAll(Lists.newArrayList(levels));
		}
		return this;
	}
	
	///////////////////////
	// enchantments numbers
	////////////////////////

	public ItemStoreBuilder withEnchantmentNumber(Integer number){
		this.enchantmentsNumbers.add(number);
		return this;
	}

	public ItemStoreBuilder withEnchantmentNumbers(Integer... numbers){
		this.enchantmentsNumbers.addAll(Lists.newArrayList(numbers));
		return this;
	}
	
	////////////
	// modifiers
	////////////

	public ItemStoreBuilder withModifier(ItemBuilderModifier modifier){
		this.modifiers.add(modifier);
		return this;
	}

	public ItemStoreBuilder withModifier(ItemBuilderModifier... modifiers){
		this.modifiers.addAll(Lists.newArrayList(modifiers));
		return this;
	}
	
	////////////
	// store
	////////////
	
	/**
	 * Store built items in a existing item store
	 * @param store
	 * @return
	 */
	public ItemStore build(ItemStore store, int maxItems){		
		return store.store(this, maxItems);
	}
	
	/**
	 * Store built items in a new item store
	 * @return
	 */
	public ItemStore build(int maxItems){
		return build(new ItemStoreImpl(), maxItems);
	}
	
}
