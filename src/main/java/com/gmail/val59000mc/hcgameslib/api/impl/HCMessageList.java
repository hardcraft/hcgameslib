package com.gmail.val59000mc.hcgameslib.api.impl;

import java.util.List;

import org.bukkit.command.CommandSender;

import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;

public class HCMessageList{

	private List<String> content;
	
	// api
	private HCStringsAPI api;
	
	public HCMessageList(Strings api, List<String> content) {
		this.content = content;
		this.api = api;
	}

	public HCMessageList replace(String pattern, String replacement){
		for(int i=0 ; i< content.size() ; i++){
			content.set(i, content.get(i).replaceAll(pattern, replacement));
		}
		return this;
	}


	public List<String> toStringList(){
		return content;
	}
	
	public String toString(){
		return String.join("", content);
	}
	
	// apis
	public HCMessageList sendChat() {
		for(String s : content){
			api.getChatAPI().sendChat(new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChat(HCPlayer player) {
		for(String s : content){
			api.getChatAPI().sendChat(player,new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChat(CommandSender sender) {
		for(String s : content){
			api.getChatAPI().sendChat(sender,new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChatP(CommandSender sender) {
		for(String s : content){
			api.getChatAPI().sendChatP(sender,new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChat(HCTeam team) {
		for(String s : content){
			api.getChatAPI().sendChat(team,new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChat(List<HCPlayer> players) {
		for(String s : content){
			api.getChatAPI().sendChat(players,new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChatP() {
		for(String s : content){
			api.getChatAPI().sendChatP(new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChatP(HCPlayer player) {
		for(String s : content){
			api.getChatAPI().sendChatP(player,new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChatP(HCTeam team) {
		for(String s : content){
			api.getChatAPI().sendChatP(team,new HCMessage(api, s, true));
		}
		return this;
	}

	
	public HCMessageList sendChatP(List<HCPlayer> players) {
		for(String s : content){
			api.getChatAPI().sendChatP(players,new HCMessage(api, s, true));
		}
		return this;
	}
}
