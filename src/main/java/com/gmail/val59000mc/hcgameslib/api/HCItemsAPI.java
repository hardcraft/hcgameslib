package com.gmail.val59000mc.hcgameslib.api;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.api.impl.items.ItemStore;
import com.gmail.val59000mc.hcgameslib.api.impl.items.ItemStoreBuilder;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;

public interface HCItemsAPI {

	Stuff getStuff(String name);
	
	void giveStuffItemsToPlayer(HCPlayer hcPlayer);
	
	void addStuff(Stuff stuff);
	
	void addStuffs(List<Stuff> stuffs);

	ItemStoreBuilder buildItemStore(Material material);
	
	ItemStoreBuilder buildItemStore(Material... materials);
	
	void addItemStore(String name, ItemStore store);
	
	ItemStore getItemStore(String name);
	
	void randomFillInventory(Inventory inventory, List<ItemStack> items);

}
