package com.gmail.val59000mc.hcgameslib.api.impl;

import java.util.List;
import java.util.regex.Pattern;

import com.gmail.val59000mc.hcgameslib.api.HCTitleAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Titles;

public class Title implements HCTitleAPI{

	private HCGame game;

	public Title(HCGame game) {
		this.game = game;
	}
	
	/**
	 * Cut the title and the subtitle at the first \\n
	 * @param text
	 * @return
	 */
	private String[] splitTitle(String text){
		if(text.isEmpty()){
			return new String[]{"",""};
		}else{
			String[] split = text.split(Pattern.quote("|||"),2);
			String[] result = new String[2];
			result[0] = split[0];
			result[1] = ( split.length == 2 ? split[1] : "" );
			return result;
		}
	}

	@Override
	public void sendTitle(HCMessage message, int fadeIn, int stay, int fadeOut) {
		String[] split = splitTitle(message.toString());
		for(HCPlayer hcPlayer : game.getPlayersManagerAPI().getPlayers(true, null)){
			Titles.sendTitle(hcPlayer.getPlayer(), fadeIn, stay, fadeOut, split[0], split[1]);
		}
	}

	@Override
	public void sendTitle(HCPlayer hcPlayer, HCMessage message, int fadeIn, int stay, int fadeOut) {
		String[] split = splitTitle(message.toString());
		if(hcPlayer.isOnline()){
			Titles.sendTitle(hcPlayer.getPlayer(), fadeIn, stay, fadeOut, split[0], split[1]);
		}
	}

	@Override
	public void sendTitle(HCTeam hcTeam, HCMessage message, int fadeIn, int stay, int fadeOut) {
		String[] split = splitTitle(message.toString());
		for(HCPlayer hcPlayer : hcTeam.getMembers()){
			if(hcPlayer.isOnline()){
				Titles.sendTitle(hcPlayer.getPlayer(), fadeIn, stay, fadeOut, split[0], split[1]);
			}
		}
	}

	@Override
	public void sendTitle(List<HCPlayer> hcPlayers, HCMessage message, int fadeIn, int stay, int fadeOut) {
		String[] split = splitTitle(message.toString());
		for(HCPlayer hcPlayer : hcPlayers){
			if(hcPlayer.isOnline()){
				Titles.sendTitle(hcPlayer.getPlayer(), fadeIn, stay, fadeOut, split[0], split[1]);
			}
		}
	}

}
