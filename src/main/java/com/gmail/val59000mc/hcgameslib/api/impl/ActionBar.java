package com.gmail.val59000mc.hcgameslib.api.impl;

import java.util.List;

import com.gmail.val59000mc.hcgameslib.api.HCActionBarAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.spigotutils.ActionBars;

public class ActionBar implements HCActionBarAPI{

	private HCGame game;

	public ActionBar(HCGame game) {
		this.game = game;
	}

	@Override
	public void sendActionBar(HCMessage msg) {
		for(HCPlayer hcPlayer : game.getPlayersManagerAPI().getPlayers(true,null)){
			ActionBars.sendActionBar(hcPlayer.getPlayer(), msg.toString());
		}
	}

	@Override
	public void sendActionBar(HCPlayer hcPlayer, HCMessage msg) {
		if(hcPlayer.isOnline()){
			ActionBars.sendActionBar(hcPlayer.getPlayer(), msg.toString());
		}
	}

	@Override
	public void sendActionBar(HCTeam hcTeam, HCMessage msg) {
		for(HCPlayer hcPlayer : hcTeam.getMembers()){
			if(hcPlayer.isOnline()){
				ActionBars.sendActionBar(hcPlayer.getPlayer(), msg.toString());
			}
		}
	}

	@Override
	public void sendActionBar(List<HCPlayer> hcPlayers, HCMessage msg) {
		for(HCPlayer hcPlayer : hcPlayers){
			if(hcPlayer.isOnline()){
				ActionBars.sendActionBar(hcPlayer.getPlayer(), msg.toString());
			}
		}
	}
	
	@Override
	public void sendActionBar(HCMessage msg, int seconds) {
		game.buildTask("send action bar", new HCTask() {
			
			@Override
			public void run() {
				for(HCPlayer hcPlayer : game.getPlayersManagerAPI().getPlayers(true,null)){
					ActionBars.sendActionBar(hcPlayer.getPlayer(), msg.toString());
				}
			}
		})
		.withInterval(20)
		.withIterations(seconds)
		.build()
		.start();
	}

	@Override
	public void sendActionBar(HCPlayer hcPlayer, HCMessage msg, int seconds) {
		game.buildTask("send action bar", new HCTask() {
			
			@Override
			public void run() {
				if(hcPlayer.isOnline()){
					ActionBars.sendActionBar(hcPlayer.getPlayer(), msg.toString());
				}
			}
		})
		.withInterval(20)
		.withIterations(seconds)
		.build()
		.start();
	}

	@Override
	public void sendActionBar(HCTeam hcTeam, HCMessage msg, int seconds) {
		game.buildTask("send action bar", new HCTask() {
			
			@Override
			public void run() {
				for(HCPlayer hcPlayer : hcTeam.getMembers()){
					if(hcPlayer.isOnline()){
						ActionBars.sendActionBar(hcPlayer.getPlayer(), msg.toString());
					}
				}
			}
		})
		.withInterval(20)
		.withIterations(seconds)
		.build()
		.start();

	}

	@Override
	public void sendActionBar(List<HCPlayer> hcPlayers, HCMessage msg, int seconds) {
		game.buildTask("send action bar", new HCTask() {
			
			@Override
			public void run() {
				for(HCPlayer hcPlayer : hcPlayers){
					if(hcPlayer.isOnline()){
						ActionBars.sendActionBar(hcPlayer.getPlayer(), msg.toString());
					}
				}
			}
		})
		.withInterval(20)
		.withIterations(seconds)
		.build()
		.start();

	}
	

}
