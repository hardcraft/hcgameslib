package com.gmail.val59000mc.hcgameslib.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import com.gmail.val59000mc.hcgameslib.api.HCActionBarAPI;
import com.gmail.val59000mc.hcgameslib.api.HCBossBarAPI;
import com.gmail.val59000mc.hcgameslib.api.HCChatAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.api.HCTitleAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;

public class Strings implements HCStringsAPI{

	private FileConfiguration lang;
	
	private HCChatAPI chat;
	private HCBossBarAPI bossBar;
	private HCActionBarAPI actionBar;
	private HCTitleAPI title;

	public Strings(HCGame game) {
		this.lang = game.getLangConfig();
		String prefix = ChatColor.translateAlternateColorCodes('&', lang.getString("messages.prefix", "§a[§f"+game.getName()+"§a]§r ").replace("%game%", game.getName()));
		
		this.chat = new Chat(game,prefix);
		this.bossBar = new BossBarImpl(game);
		this.actionBar = new ActionBar(game);
		this.title = new Title(game);
	}

	@Override
	public HCMessage getNoColor(String code) {
		return getNoColor(code, "["+code+"]");
	}

	@Override
	public HCMessage getNoColor(String code, String defaultString) {
		String str = lang.getString(code);
		if(str == null)
			str = "["+code+"]";
		return new HCMessage(this, ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', str)) , false);
	}

	@Override
	public HCMessage get(String code) {
		return get(code, "["+code+"]");
	}
	
	@Override
	public HCMessageList getList(String code) {
		List<String> list = lang.getStringList(code);
		if(list == null){
			list = new ArrayList<String>();
			list.add("["+code+"]");
		}
		
		for(int i=0 ; i< list.size() ; i++){
			list.set(i, ChatColor.translateAlternateColorCodes('&',list.get(i)));
		}
		
		return new HCMessageList(this, list);
	}

	@Override
	public HCMessage get(String code, String defaultString) {
		String str = lang.getString(code);
		if(str == null)
			str = "["+code+"]";
		
		return new HCMessage(this, ChatColor.translateAlternateColorCodes('&',str), true);
	}

	@Override
	public boolean exists(String code) {
		return lang.getString(code) != null;
	}


	@Override
	public HCMessage getRaw(String content) {
		return new HCMessage(this, content, true);
	}

	@Override
	public HCActionBarAPI getActionBarAPI() {
		return actionBar;
	}

	@Override
	public HCBossBarAPI getBossBarAPI() {
		return bossBar;
	}

	@Override
	public HCChatAPI getChatAPI() {
		return chat;
	}

	@Override
	public HCTitleAPI getTitleAPI() {
		return title;
	}

}
