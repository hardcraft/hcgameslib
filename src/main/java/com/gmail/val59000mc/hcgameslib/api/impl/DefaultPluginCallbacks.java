package com.gmail.val59000mc.hcgameslib.api.impl;

import com.gmail.val59000mc.hcgameslib.api.*;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.hcgameslib.worlds.WorldManager;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.BungeeTpAction;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.*;
import com.gmail.val59000mc.spigotutils.items.ItemUtils;
import com.google.common.collect.Lists;
import org.bukkit.Sound;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultPluginCallbacks implements HCPluginCallbacksAPI {

    private HCGameAPI api;

    public HCGameAPI getApi() {
        return api;
    }

    public void setApi(HCGameAPI api) {
        this.api = api;
    }

    public HCPlayersManagerAPI getPmApi() {
        return api.getPlayersManagerAPI();
    }

    public HCStringsAPI getStringsApi() {
        return api.getStringsAPI();
    }

    public HCBungeeAPI getBungeeApi() {
        return api.getBungeeAPI();
    }

    public HCPluginCallbacksAPI getCallbacksApi() {
        return api.getCallbacksApi();
    }

    public HCDependenciesAPI getDependencies() {
        return api.getDependencies();
    }

    public HCSoundAPI getSoundApi() {
        return api.getSoundAPI();
    }

    public HCItemsAPI getItemsApi() {
        return api.getItemsAPI();
    }

    public FileConfiguration getConfig() {
        return api.getConfig();
    }

    // Callbacks

    @Override
    public HCTeam newHCTeam(String name, ChatColor color, Location spawnpoint) {
        return new HCTeam(name, color, spawnpoint);
    }


    @Override
    public HCPlayer newHCPlayer(Player player) {
        return new HCPlayer(player);
    }

    @Override
    public HCPlayer newHCPlayer(HCPlayer hcPlayer) {
        return new HCPlayer(hcPlayer);
    }


    @Override
    public List<HCTeam> createTeams() {
        List<HCTeam> teams = new ArrayList<HCTeam>();
        teams.add(newHCTeam("Rouge", ChatColor.RED, null));
        teams.add(newHCTeam("Bleu", ChatColor.BLUE, null));
        return teams;
    }

    @Override
    public List<Stuff> createStuffs() {

        return Lists.newArrayList(
            new Stuff.Builder(Constants.DEFAULT_STUFF_NAME)
                .addArmorItem(new ItemStack(Material.IRON_HELMET))
                .addArmorItem(new ItemStack(Material.IRON_CHESTPLATE))
                .addArmorItem(new ItemStack(Material.IRON_LEGGINGS))
                .addArmorItem(new ItemStack(Material.IRON_BOOTS))
                .addInventoryItem(new ItemStack(Material.IRON_SWORD))
                .addInventoryItem(new ItemStack(Material.COOKED_BEEF, 20))
                .build()
        );
    }

    @Override
    public void beforeLoad() {

    }

    @Override
    public WorldConfig configureWorld(WorldManager wm) {

        World mainWorld = Bukkit.getWorlds().get(0);

        File sourceMap = new File(getConfig().getString("config.world.source", mainWorld.getName()));
        File destMap = new File(getConfig().getString("config.world.dest", mainWorld.getName() + "_play"));

        if (!sourceMap.exists()) {
            sourceMap = mainWorld.getWorldFolder();
            destMap = new File(mainWorld.getName() + "_play");
        }

        wm.deleteFiles(destMap);
        wm.deleteOldPlayersFiles(mainWorld.getWorldFolder());
        wm.copyFiles(sourceMap, destMap, "uid.dat");
        World world = wm.loadOrCreateWorld(destMap.getName());

        Location lobby = Parser.parseLocation(world, getConfig().getString("config.world.lobby"));
        Location center = Parser.parseLocation(world, getConfig().getString("config.world.center"));

        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.COMMAND_BLOCK_OUTPUT, false);
        world.setGameRule(GameRule.LOG_ADMIN_COMMANDS, false);
        world.setGameRule(GameRule.SEND_COMMAND_FEEDBACK, false);
        world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        world.setGameRule(GameRule.DISABLE_ELYTRA_MOVEMENT_CHECK, true);
        world.setGameRule(GameRule.DISABLE_RAIDS, true);
        world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        world.setTime(getConfig().getInt("config.world.time", Constants.DEFAULT_WORLD_TIME_TICKS));
        world.setStorm(false);
        world.setWeatherDuration(999999999);
        world.setDifficulty(Difficulty.HARD);
        world.setSpawnLocation(lobby.getBlockX(), lobby.getBlockY(), lobby.getBlockZ());
        world.setKeepSpawnInMemory(false);
        world.save();

        Bukkit.setSpawnRadius(0);

        return new WorldConfig.Builder(world)
            .withLobby(lobby)
            .withCenter(center)
            .build();
    }

    /**
     * Update the scoreboard of a waiting HCPlayer
     * Must return the lines of the scoreboard
     *
     * @return lines
     */
    @Override
    public List<String> updateWaitingScoreboard(HCPlayer hcPlayer) {
        List<String> content = new ArrayList<String>();
        content.add(" ");

        // Team
        content.add(getStringsApi().get("messages.scoreboard.online-players").toString());
        content.add(" " + ChatColor.GREEN + getPmApi().getPlayersCount(true, PlayerState.WAITING) + ChatColor.WHITE + " / " + ChatColor.GREEN + getPmApi().getMaxPlayers());
        content.add(" ");

        int remaining = getApi().getRemainingTimeBeforeStart();

        if (getPmApi().isStartingTaskScheduled()) {
            // Kills / Deaths
            content.add(getStringsApi().get("messages.scoreboard.remaining-time-before-start").toString());
            content.add(" " + ChatColor.GREEN + "" + Time.getFormattedTime(remaining));
            content.add(" ");
        }

        return content;

    }

    /**
     * Update the scoreboard of a playing HCPlayer
     * Must return the lines of the scoreboard
     *
     * @return lines
     */
    @Override
    public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {

        List<String> content = new ArrayList<String>();

        // All teams kills
        content.add(getStringsApi().get("messages.scoreboard.scores").toString());
        for (HCTeam team : getPmApi().getTeams()) {
            content.add(" " + team.getColor() + team.getName() + " " + ChatColor.WHITE + team.getKills());
        }

        // Remaining time (if exists)
        if (getApi().is(GameState.PLAYING) && getApi().isCountdownEndOfGameEnabled()) {
            int remainingTime = getApi().getRemainingTimeBeforeEnd();
            content.add(
                getStringsApi().get("messages.scoreboard.remaining-time-before-end").toString() +
                    " " +
                    ChatColor.GREEN + Time.getFormattedTime(remainingTime)
            );
        }

        // Kills
        content.add(getStringsApi().get("messages.scoreboard.kills-deaths").toString());
        content.add(" " + ChatColor.GREEN + "" + hcPlayer.getKills() + ChatColor.WHITE + "/" + ChatColor.GREEN + hcPlayer.getDeaths());

        // Coins
        content.add(getStringsApi().get("messages.scoreboard.coins").toString());
        content.add(" " + ChatColor.GREEN + "" + hcPlayer.getMoney());

        // Teammate
        if (hcPlayer.getTeam() != null) {
            content.add(getStringsApi().get("messages.scoreboard.teammates").toString());
            for (HCPlayer teammate : hcPlayer.getTeam().getMembers()) {
                if (teammate.isPlaying()) {
                    content.add(" " + teammate.getColor() + "" + teammate.getName());
                } else {
                    content.add(" " + ChatColor.GRAY + "" + teammate.getName());
                }
            }
        }

        return content;
    }


    /**
     * Play a sound and display a title when decreasing the time before start
     */
    @Override
    public void timeBeforeStartDecreased(int time) {

        if ((time % 10 == 0 && time > 10) || (time > 0 && time <= 10)) {
            api.getSoundAPI().play(Sound.BLOCK_NOTE_BLOCK_HARP, 1f, 1f);
            api.getStringsAPI().getRaw((time > 10 ? ChatColor.GREEN : ChatColor.RED) + String.valueOf(time)).sendTitle(0, 21, 0);
        }

    }

    /**
     * Optionally play sound or messages when waiting for players
     */
    @Override
    public void waitForPlayersMessages() {
        Log.info("Waiting for players");
    }

    /**
     * Play a sound and display a title when starting the game
     */
    @Override
    public void startGameMessages() {

        api.getSoundAPI().play(Sound.ENTITY_ENDER_DRAGON_GROWL, 1f, 2f);
        api.getStringsAPI().get("messages.starting-game")
            .sendChatP()
            .sendTitle(20, 50, 20);

    }

    /**
     * Optionally play sound or messages when launching game
     */
    @Override
    public void playGameMessages() {

    }

    /**
     * Performs actions with the remaining time before game ends
     * Called every second (0 included)
     *
     * @param remainingTime remaining time in seconds before end of game
     * @param timeElapsed   time elapsed in seconds since start of game
     */
    @Override
    public void remainingTimeBeforeEnd(int remainingTime, int timeElapsed) {

        if (remainingTime == 0) {
            // Clear action bar
            getStringsApi().getRaw("").sendActionBar();
        } else {

            if (remainingTime <= 120 && (remainingTime % 30 == 0 || remainingTime <= 10)) {
                // Display remaining time in action bar
                getStringsApi()
                    .get("messages.countdown.remaining-time")
                    .replace("%time%", Time.getFormattedTime(remainingTime))
                    .sendActionBar();
                getSoundApi().play(Sound.BLOCK_NOTE_BLOCK_SNARE, 0.8f, 2f);
            }

            getPmApi().updatePlayersScoreboards();

        }

    }

    /**
     * Optionally play sound or messages when ending game
     */
    @Override
    public void endGameMessages(HCTeam winningTeam) {

        getSoundApi().play(Sound.ENTITY_ENDER_DRAGON_GROWL, 0.8f, 2);

        if (winningTeam == null) {
            getStringsApi()
                .get("messages.end.title-draw")
                .sendTitle(20, 60, 20);
        } else {
            getStringsApi()
                .get("messages.end.title-win")
                .replace("%team%", winningTeam.getColor() + winningTeam.getName())
                .sendTitle(20, 60, 20);


            getApi().sync(() -> {

                for (HCPlayer hcPlayer : getPmApi().getPlayers(true, null)) {
                    if (hcPlayer.hasTeam()) {
                        if (hcPlayer.getTeam().equals(winningTeam)) {
                            getStringsApi()
                                .get("messages.end.actionbar-win")
                                .sendActionBar(hcPlayer);
                        } else {
                            getStringsApi()
                                .get("messages.end.actionbar-lose")
                                .sendActionBar(hcPlayer);
                        }

                    }
                }

            }, 10);


        }
    }

    @Override
    public List<String> updateEndedScoreboard(HCPlayer hcPlayer) {
        //  HCTeam team = getPmApi().getWinningTeam();
        return updatePlayingScoreboard(hcPlayer);
    }

    /**
     * Display a joining message and teleport player to lobby
     */
    @Override
    public void waitPlayerAtLobby(HCPlayer hcPlayer) {
        if (hcPlayer.isOnline()) {
            getStringsApi()
                .get("messages.welcome-player")
                .replace("%player%", hcPlayer.getName())
                .replace("%game%", getApi().getName())
                .sendChatP(hcPlayer);

            getStringsApi()
                .getRaw(ChatColor.GREEN + getApi().getName())
                .sendTitle(hcPlayer, 20, 30, 20);

            getStringsApi()
                .get("messages.player-has-joined-the-game")
                .replace("%player%", hcPlayer.getName())
                .replace("%count%", String.valueOf(api.getPlayersManagerAPI().getPlayersCount(true, PlayerState.WAITING)))
                .replace("%total%", String.valueOf(api.getPlayersManagerAPI().getMaxPlayers()))
                .sendChatP();

            Player player = hcPlayer.getPlayer();

            if ((api.is(GameState.WAITING)) && api.getDependencies().isSimpleInventoryGUIEnabled()) {
                Bukkit.getScheduler().runTaskLater(getApi().getPlugin(), new Runnable() {
                    public void run() {
                        SIG.getPlugin().getAPI().giveInventoryToPlayer(player, Constants.DEFAULT_WAITING_INV_NAME);
                        player.updateInventory();
                    }
                }, 1);
            }

            Inventories.clear(player);
            Effects.clear(player);
            Effects.add(player, PotionEffectType.SPEED, 999999, 0, false);
            Effects.add(player, PotionEffectType.SATURATION, 999999, 0, false);

            player.teleport(getApi().getWorldConfig().getLobby());
        }
    }

    /**
     * Display a message to the relogging player
     */
    @Override
    public void relogPlayingPlayer(HCPlayer hcPlayer) {
        if (hcPlayer.isOnline()) {
            getStringsApi()
                .get("messages.player-is-back-online")
                .replace("%player%", hcPlayer.getName())
                .sendChatP();
        }
    }

    /**
     * Clear the inventory and effects
     * Give spectator items
     */
    @Override
    public void spectatePlayer(HCPlayer hcPlayer) {
        if (hcPlayer.isOnline()) {
            getStringsApi()
                .get("messages.spectate-player")
                .sendChatP(hcPlayer);


            Player player = hcPlayer.getPlayer();
            Effects.clear(player);
            Inventories.clear(player);

            if ((api.is(GameState.PLAYING) || api.is(GameState.STARTING)) && api.getDependencies().isSimpleInventoryGUIEnabled()) {

                Bukkit.getScheduler().runTaskLater(getApi().getPlugin(), new Runnable() {
                    public void run() {
                        SIG.getPlugin().getAPI().giveInventoryToPlayer(player, Constants.DEFAULT_SPECTATOR_INV_NAME);
                        player.updateInventory();
                    }
                }, 1);

                getStringsApi()
                    .get("messages.spec.hint")
                    .sendChat(hcPlayer)
                    .sendActionBar(hcPlayer);
            }

            switch (getApi().getGameState()) {
                case PLAYING:
                case ENDED:
                    player.teleport(getApi().getWorldConfig().getCenter());
                    break;
                default:
                    player.teleport(getApi().getWorldConfig().getLobby());
                    break;
            }

            if (api.is(GameState.PLAYING) && getPmApi().isJoinTeamWhilePlayingAllowed()) {
                getCallbacksApi().joinTeamHintOnJoin(hcPlayer);
            }
        }
    }

    /**
     * Send a message to the vanished player
     * Give spectator items
     */
    @Override
    public void vanishedPlayer(HCPlayer hcPlayer) {

        getStringsApi().get("messages.vanished").sendChatP(hcPlayer);
        getStringsApi().get("messages.vanished-title").sendTitle(hcPlayer, 0, 80, 0);
        getStringsApi().get("messages.vanished-bar").sendActionBar(hcPlayer);

        if (hcPlayer.isOnline()) {

            Player player = hcPlayer.getPlayer();

            switch (getApi().getGameState()) {
                case PLAYING:
                case ENDED:
                    player.teleport(getApi().getWorldConfig().getCenter());
                    break;
                default:
                    player.teleport(getApi().getWorldConfig().getLobby());
                    break;
            }

            if (api.getDependencies().isSimpleInventoryGUIEnabled()) {

                Bukkit.getScheduler().runTaskLater(getApi().getPlugin(), new Runnable() {
                    public void run() {
                        SIG.getPlugin().getAPI().giveInventoryToPlayer(hcPlayer.getPlayer(), Constants.DEFAULT_SPECTATOR_INV_NAME);
                        player.updateInventory();
                    }
                }, 1);

            }
        }

        getSoundApi().play(hcPlayer, Sound.ENTITY_ENDERMAN_TELEPORT, 1, 1.5f);

    }

    /**
     * Send a message to the eliminated player
     */
    @Override
    public void eliminatedPlayer(HCPlayer hcPlayer) {

        if (getPmApi().isJoinTeamWhilePlayingAllowed()) {
            getPmApi().removePlayerFromTeamSilently(hcPlayer);
        }

        getStringsApi()
            .get("messages.player-has-been-eliminated")
            .replace("%player%", hcPlayer.getName())
            .sendChatP();

    }


    /**
     * Logic to assign a random team to a player
     * Default implementation chooses the team with the least numebr of players
     */
    @Override
    public HCTeam getAssignableRandomTeam() {
        HCTeam min = getPmApi().getTeams().get(0);
        for (HCTeam team : getPmApi().getTeams()) {
            if (team.getMembers().size() < min.getMembers().size()) {
                min = team;
            }
        }
        return min;
    }

    /**
     * Get Max allowed number of players
     *
     * @return
     */
    @Override
    public int getMaxPlayers() {
        int max = getApi().getConfig().getInt("config.max-players", Constants.DEFAULT_MAX_PLAYERS);
        return max;
    }

    /**
     * Get min number of player to start the game
     *
     * @return
     */
    @Override
    public int getMinPlayers() {
        int min = getApi().getConfig().getInt("config.start-scheduler.min-players-to-start", Constants.DEFAULT_MIN_PLAYERS_TO_START);
        return min;
    }

    /**
     * Optional actions to execute when starting a player
     * ex: adding some effects, send a message
     */
    @Override
    public void startPlayer(HCPlayer hcPlayer) {

        if (hcPlayer.isOnline()) {

            Player player = hcPlayer.getPlayer();
            player.setGameMode(GameMode.ADVENTURE);
            player.setFireTicks(0);
            player.setHealth(20);
            player.setFoodLevel(20);
            player.setSaturation(20);
            Effects.clear(player);
            Inventories.clear(player);

            api.getItemsAPI().giveStuffItemsToPlayer(hcPlayer);

            if (hcPlayer.getSpawnPoint() == null)
                hcPlayer.getPlayer().teleport(getApi().getWorldConfig().getCenter());
            else
                hcPlayer.getPlayer().teleport(hcPlayer.getSpawnPoint());

        }
    }

    /**
     * Called before a player is given his stuff
     * Allow to change the stuff at every respawn
     * Set stuff to null to not give anything
     * Or set inv content to null or armor content to null
     */
    @Override
    public void assignStuffToPlayer(HCPlayer hcPlayer) {
        if (hcPlayer.getStuff() == null)
            hcPlayer.setStuff(getItemsApi().getStuff("default-stuff"));
    }

    /**
     * Optional actions to execute when reviving a player
     */
    @Override
    public void revivePlayer(HCPlayer hcPlayer) {
        startPlayer(hcPlayer);
    }

    /**
     * Optional actions to execute when respawning a player
     */
    @Override
    public void respawnPlayer(HCPlayer hcPlayer) {

        if (hcPlayer.isOnline()) {
            hcPlayer.getPlayer().setSaturation(20);
        }

        api.getItemsAPI().giveStuffItemsToPlayer(hcPlayer);

    }

    /**
     * Called every second when running the countdown before becoming
     * a spectator after typing the spec commmand
     */
    public boolean isAllowedToPerformSpecCommand(HCPlayer hcPlayer) {
        return hcPlayer.isOnline() && !hcPlayer.is(PlayerState.DEAD) && !hcPlayer.is(PlayerState.SPECTATING);
    }

    /**
     * Randomize list of player and assign to team
     * Be careful when changing the default implementation
     */
    public List<HCPlayer> getRandomizedPlayersAndAssignedToTeams(List<HCPlayer> players) {
        HCPlayersManagerAPI pm = getPmApi();

        // Removing offline or not waiting players from their team
        for (HCPlayer hcPlayer : players) {
            if (hcPlayer.getTeam() != null) {
                if (!hcPlayer.isOnline() || !hcPlayer.is(PlayerState.WAITING)) {
                    pm.removePlayerFromTeamSilently(hcPlayer);
                }
            }
        }

        List<HCPlayer> waitingOnline = players
            .stream()
            .filter((p) -> {
                return p.is(PlayerState.WAITING) && p.isOnline();
            })
            .collect(Collectors.toList());


        List<HCPlayer> otherOnlineOrOffline = players
            .stream()
            .filter((p) -> {
                return (!p.isOnline() && p.is(PlayerState.WAITING))
                    || p.is(PlayerState.DEAD)
                    || p.is(PlayerState.SPECTATING)
                    || p.is(PlayerState.PLAYING)
                    || p.is(PlayerState.VANISHED);
            })
            .collect(Collectors.toList());

        // Shuffle players who will play (up to maxPlayers)
        List<HCPlayer> playersWhoWillPlay = new ArrayList<HCPlayer>(waitingOnline.subList(0, getMaxPlayers() > waitingOnline.size() ? waitingOnline.size() : getMaxPlayers()));

        List<HCPlayer> playersWhoWillNotPlay = new ArrayList<HCPlayer>();
        if (waitingOnline.size() > getMaxPlayers()) {
            playersWhoWillNotPlay = new ArrayList<HCPlayer>(waitingOnline.subList(getMaxPlayers(), waitingOnline.size()));
        }
        for (HCPlayer hcPlayer : playersWhoWillNotPlay) {
            pm.removePlayerFromTeam(hcPlayer);
        }
        playersWhoWillNotPlay.addAll(otherOnlineOrOffline);

        List<HCPlayer> randomizedPlayers = new ArrayList<HCPlayer>();

        Collections.shuffle(playersWhoWillPlay);
        randomizedPlayers.addAll(playersWhoWillPlay);
        randomizedPlayers.addAll(playersWhoWillNotPlay);

        int nPlayer = 0;
        for (HCPlayer hcPlayer : players) {
            nPlayer++;

            if (hcPlayer.is(true, PlayerState.WAITING) && nPlayer <= getMaxPlayers()) {

                HCTeam team = hcPlayer.getTeam();

                if (hcPlayer.getTeam() == null) {
                    team = getCallbacksApi().getAssignableRandomTeam();
                    if (team != null) {
                        pm.addPlayerToTeam(hcPlayer, team);
                    }
                }

            }

        }

        return randomizedPlayers;
    }

    /**
     * Modify the death event if needed
     * Default: respawn player after 20 ticks
     *
     * @param event
     * @param hcPlayer
     */
    @Override
    public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {

        getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());

    }

    /**
     * Modify the death event if needed
     * Default: respawn player after 20 ticks
     *
     * @param event
     * @param hcPlayer
     */
    @Override
    public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {

        HCPlayersManagerAPI pmApi = getPmApi();

        pmApi.rewardMoneyTo(hcKiller, getKillReward(hcKiller));

        pmApi.autoRespawnPlayerAfter20Ticks(event.getEntity());

    }

    @Override
    public double getKillReward(HCPlayer hcPlayer) {
        if (hcPlayer.isOnline()) {
            return getConfig().getDouble("config.kill-reward", Constants.DEFAULT_KILL_REWARD);
        }
        return 0;
    }

    @Override
    public double getWinReward(HCPlayer hcPlayer) {
        if (hcPlayer.isOnline()) {
            return getConfig().getDouble("config.win-reward", Constants.DEFAULT_WIN_REWARD);
        }
        return 0;
    }

    @Override
    public void moneyRewardedTo(HCPlayer hcKiller, double amount) {

        if (amount > 0) {
            getStringsApi().getRaw(ChatColor.GREEN + "+" + amount + " " + ChatColor.WHITE + "HC").sendChat(hcKiller);
            getSoundApi().play(hcKiller, Sound.ENTITY_PLAYER_LEVELUP, 0.5f, 2f);
        }

    }

    @Override
    public double getExtraHardcoinsMultiplier(HCPlayer player) {
        return 0;
    }

    /**
     * Format death message showed when a player dies
     * May Return null to keep as vanilla behavior
     */
    @Override
    public void formatDeathMessage(HCPlayer hcKilled, PlayerDeathEvent event) {
        event.setDeathMessage(getStringsApi().get("messages.player-killed")
            .replace("%killed%", hcKilled.getColor() + hcKilled.getName())
            .toString());
    }

    /**
     * Format death message showed when a player is killed by another player
     * May Return null to keep as vanilla behavior
     */
    @Override
    public void formatDeathMessage(HCPlayer hcKilled, HCPlayer hcKiller, PlayerDeathEvent event) {

        HCPlayer hcArrowShooter = getPmApi().getLastDamagingPlayerArrowShooter(hcKilled);

        if (hcArrowShooter != null
            && hcArrowShooter.equals(hcKiller)) {

            int distance = (int) Math.round(hcKilled.getPlayer().getLocation().distance(hcKiller.getPlayer().getLocation()));
            event.setDeathMessage(getStringsApi().get("messages.player-killed-by-with-arrow")
                .replace("%killer%", hcKiller.getColor() + hcKiller.getName())
                .replace("%killed%", hcKilled.getColor() + hcKilled.getName())
                .replace("%distance%", String.valueOf(distance))
                .toString());
        } else {
            event.setDeathMessage(getStringsApi().get("messages.player-killed-by")
                .replace("%killer%", hcKiller.getColor() + hcKiller.getName())
                .replace("%killed%", hcKilled.getColor() + hcKilled.getName())
                .toString());
        }

    }

    /**
     * Format chat message from a player
     */
    @Override
    public void formatChatMessage(AsyncPlayerChatEvent event, HCPlayer hcPlayer) {

        String message = event.getMessage();
        boolean isGlobalChat = hcPlayer.isGlobalChat();
        if (message.startsWith("!")) {
            event.setMessage(message.substring(1, message.length()));
            isGlobalChat = true;
        }

        Player player = hcPlayer.getPlayer();

        if (player.hasPermission("hardcraftpvp.chatcolor")) {
            event.setMessage(ChatColor.translateAlternateColorCodes('&', event.getMessage()));
        }

        String prefix;
        String donationStars = hcPlayer.hasDonated() ? hcPlayer.countDonations() + Constants.STAR_ICON + " " : "";
        String playerName;
        String suffix;

        if (getApi().getConfig().getBoolean("config.colored-ranks", Constants.DEFAULT_ENABLE_COLORED_RANKS)) {
            prefix = ChatColor.translateAlternateColorCodes('&', getDependencies().getPrefix(player));
            suffix = ChatColor.translateAlternateColorCodes('&', getDependencies().getSuffix(player));
            playerName = hcPlayer.getName();
        } else {
            prefix = "§7" + getDependencies().getStrippedPrefix(player);
            suffix = ChatColor.translateAlternateColorCodes('&', getDependencies().getSuffix(player));
            playerName = hcPlayer.getColoredName();
        }

        event.setFormat(prefix + donationStars + playerName + ": " + suffix + "%2$s");

        if (hcPlayer.hasTeam() && !isGlobalChat) {

            HCTeam team = hcPlayer.getTeam();

            Set<Player> recipients = event.getRecipients();
            recipients.clear();
            for (HCPlayer teammate : team.getMembers()) {
                if (teammate.isOnline()) {
                    recipients.add(teammate.getPlayer());
                }
            }
            String teamPrefix = "§f[" + team.getColor() + "TEAM" + "§f] §r";
            event.setFormat(teamPrefix + event.getFormat());
        }


    }

    /**
     * Called at the end of the game for every player
     */
    @Override
    public void printEndMessage(HCPlayer hcPlayer, HCTeam winningTeam) {

        if (hcPlayer.isOnline()) {

            Texts.tellraw(hcPlayer.getPlayer(), getStringsApi().get("messages.end.winning-team")
                .replace("%game%", getApi().getName())
                .replace("%kills%", String.valueOf(hcPlayer.getKills()))
                .replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
                .replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
                .replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
                .replace("%winner%", (winningTeam == null ? "Match nul" : winningTeam.getColor() + winningTeam.getName()))
                .toString()
            );

        }

    }

    /**
     * Must be called by the implementing plugin when it decides that a player is eliminated
     */
    @Override
    public void printDeathMessage(HCPlayer hcPlayer) {
        if (hcPlayer.isOnline()) {

            Texts.tellraw(hcPlayer.getPlayer(), getStringsApi().get("messages.end.dead")
                .replace("%kills%", String.valueOf(hcPlayer.getKills()))
                .replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
                .replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
                .replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
                .toString()
            );
        }
    }

    @Override
    public void rebootServer() {

        HCFlowsAPI flows = getApi().getFlowsAPI();

        boolean autoReboot = getApi().getConfig().getBoolean("config.auto-reboot-after-end.enable", Constants.DEFAULT_ENABLE_AUTO_REBOOT_AFTER_END);
        int delayTicks = 20 * getApi().getConfig().getInt("config.auto-reboot-after-end.delay-seconds", Constants.DEFAULT_AUTO_REBOOT_AFTER_END_SECONDS_DELAY);

        getApi().buildTask("restart server", new HCTask() {

            @Override
            public void run() {

                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-all");

                if (flows.isEnabled()) {
                    flows.executeFlowCopyFilesScript();
                }
                if (autoReboot) {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
                }

            }
        }).withFirstCallback(new HCTask() {

            @Override
            public void run() {
                if (getConfig().getBoolean("config.bungee.enabled")) {
                    String serverName = getConfig().getString("config.bungee.server", Constants.DEFAULT_LOBBY_SERVER);
                    if (flows.isEnabled()) {
                        serverName = flows.getNextFlowServer();
                    }
                    getBungeeApi().sendAllToServer(serverName);
                } else {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.kickPlayer("THE END !");
                    }
                }
            }
        })
            .withIterations(1) // Execute once
            .withDelay(delayTicks) // Start reboot thread in "delayTicks" ticks
            .withInterval(40) // 1 second delay after sending to Lobby
            .build()
            .start();
    }

    @Override
    public void alertGameIsFull(HCPlayer hcPlayer) {
        getStringsApi().get("messages.game-is-full").sendChatP(hcPlayer);
        getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 2f, 1.2f);
    }


    /**
     * Called before starting a player
     */
    @Override
    public Location getFirstRespawnLocation(HCPlayer hcPlayer) {
        return getNextRespawnLocation(hcPlayer);
    }

    /**
     * Called after each death to choos the next respan location (if the game is still running)
     * Can
     */
    @Override
    public Location getNextRespawnLocation(HCPlayer hcPlayer) {
        if (hcPlayer.hasTeam())
            return hcPlayer.getTeam().getSpawnpoint();
        return hcPlayer.getSpawnPoint();
    }

    /**
     * Build an item for simple inventory gui to display in spectator inventory
     */
    @Override
    public ItemBuilder getSpecInvPlayerItem(HCPlayer hcPlayer) {

        HCStringsAPI s = getStringsApi();

        return new ItemBuilder(Material.SKELETON_SKULL)
            .withDamage((short) 3)
            .withName(hcPlayer.getColor() + hcPlayer.getName())
            .withPlayerSkullName(hcPlayer.getName())
            .withLore(Lists.newArrayList(
                " ",
                s.get("messages.spec.player").toString() + hcPlayer.getName(),
                s.get("messages.spec.team").toString() + hcPlayer.getColor() + hcPlayer.getTeam().getName(),
                s.get("messages.spec.kills").toString() + hcPlayer.getKills(),
                s.get("messages.spec.deaths").toString() + hcPlayer.getDeaths(),
                " ",
                s.get("messages.spec.tp").toString()
                )
            );
    }


    @Override
    public String getChooseTeamInventoryName() {
        return Constants.DEFAULT_TEAM_INV_NAME;
    }

    /**
     * Build a SIG inventory to choose a team
     */
    @Override
    public Inventory getChooseTeamInventory() {
        List<HCTeam> teams = getPmApi().getTeams();
        Inventory teamInv = new Inventory(getChooseTeamInventoryName(), getStringsApi().get("messages.waiting-inventory.choose-team").toString(), 1 + (teams.size() / 9));

        Item randomTeam = new Item.ItemBuilder(Material.FIRE_CHARGE)
            .withName(getStringsApi().get("messages.waiting-inventory.random-team").toString())
            .withPosition(0)
            .withHideFlags(true)
            .build();
        randomTeam.setActions(Lists.newArrayList(
            new SelectTeamAction(null)
        ));
        teamInv.addItem(randomTeam);

        int pos = 1;
        for (HCTeam team : teams) {
            Item teamItem = new Item.ItemBuilder(ItemUtils.toWoolMaterial(team.getColor()))
                .withName(getStringsApi().get("messages.waiting-inventory.team").toString() + team.getColor() + team.getName())
                .withPosition(pos)
                .withLore(Lists.newArrayList(
                    "{members." + team.getName() + "}"
                ))
                .build();
            teamItem.setActions(Lists.newArrayList(
                new SelectTeamAction(team)
            ));
            teamInv.addItem(teamItem);

            pos++;
        }

        return teamInv;

    }

    /**
     * Action to exectute when a player clicks on a wool
     * to choose a team when waiting for the game to start
     *
     * @author Valentin
     */
    protected class SelectTeamAction extends Action {

        private HCTeam team;

        public SelectTeamAction(HCTeam team) {
            this.team = team;
        }

        @Override
        public void executeAction(Player player, SigPlayer sigPlayer) {

            HCPlayersManagerAPI pmApi = getPmApi();

            // join team while waiting game
            if (getApi().is(GameState.WAITING)) {
                HCPlayer hcPlayer = pmApi.getHCPlayer(player);
                if (hcPlayer != null && hcPlayer.is(true, PlayerState.WAITING)) {

                    if (team == null) {
                        pmApi.removePlayerFromTeam(hcPlayer);
                        getStringsApi()
                            .get("messages.waiting-inventory.random-team-joined")
                            .sendChat(hcPlayer);
                        getSoundApi().play(hcPlayer, Sound.BLOCK_NOTE_BLOCK_PLING, 1, 2);
                    } else {
                        chooseTeam(team, hcPlayer);
                    }

                }

                // join team while game is running
            } else if (getApi().is(GameState.PLAYING) && pmApi.isJoinTeamWhilePlayingAllowed()) {
                HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
                if (hcPlayer != null && (hcPlayer.is(true, PlayerState.DEAD) || hcPlayer.is(true, PlayerState.SPECTATING))) {

                    HCTeam chosenTeam;
                    if (team == null) {
                        pmApi.removePlayerFromTeamSilently(hcPlayer);
                        chosenTeam = pmApi.getMinMembersTeam();
                    } else {
                        chosenTeam = team;
                    }

                    boolean success = chooseTeam(chosenTeam, hcPlayer);
                    if (success) {
                        pmApi.revivePlayer(hcPlayer);
                        getStringsApi().get("messages.play.joined-the-game")
                            .replace("%player%", hcPlayer.getColoredName())
                            .replace("%team%", hcPlayer.getTeam().getColoredName())
                            .sendChatP();
                    }

                }
            }

            player.closeInventory();

            executeNextAction(player, sigPlayer, true);


        }

        private boolean chooseTeam(HCTeam team, HCPlayer hcPlayer) {

            if (hcPlayer.hasTeam() && hcPlayer.getTeam().equals(team)) {
                getStringsApi()
                    .get("messages.waiting-inventory.cannot-join-same-team")
                    .replace("%team%", team.getColoredName())
                    .sendChat(hcPlayer);
                getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 1, 2);
                return false;
            } else {

                boolean allowedToJoin = getPmApi().canJoinTeam(hcPlayer, team);

                if (allowedToJoin) {
                    getPmApi().addPlayerToTeam(hcPlayer, team);
                    getStringsApi()
                        .get("messages.waiting-inventory.team-joined")
                        .replace("%team%", team.getColoredName())
                        .sendChat(hcPlayer);
                    getSoundApi().play(hcPlayer, Sound.BLOCK_NOTE_BLOCK_PLING, 1, 2);
                } else {
                    getStringsApi()
                        .get("messages.waiting-inventory.cannot-join-full-team")
                        .replace("%team%", team.getColoredName())
                        .replace("%min-team%", getPmApi().getMinMembersTeam().getColoredName())
                        .sendChat(hcPlayer);
                    getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 1, 2);
                }

                return allowedToJoin;

            }
        }
    }


    /**
     * Build waiting inventory
     */
    @Override
    public Inventory getWaitingInventory() {

        Inventory waitInv = new Inventory(Constants.DEFAULT_WAITING_INV_NAME, "§aAttente", 1);
        Log.debug("Registering waiting inventory " + waitInv.getName());
        Material waitingTeamMaterial;
        try {
            waitingTeamMaterial = Material.valueOf(getApi().getConfig().getString("config.waiting-inventory.team-material", Constants.DEFAULT_WAIT_INVENTORY_CHOOSE_TEAM_MATERIAL));
        } catch (IllegalArgumentException ex) {
            waitingTeamMaterial = Material.valueOf(Constants.DEFAULT_WAIT_INVENTORY_CHOOSE_TEAM_MATERIAL);
        }

        // Choose team item
        if (getApi().getConfig().getBoolean("config.waiting-inventory.enable-choose-team", Constants.DEFAULT_ENABLE_CHOOSE_TEAM)) {
            Item chooseTeam = new Item.ItemBuilder(waitingTeamMaterial)
                .withName(getStringsApi().get("messages.waiting-inventory.choose-team").toString())
                .withPosition(0)
                .build();
            chooseTeam.setActions(
                Lists.newArrayList(
                    new OpenInventoryAction(getChooseTeamInventoryName())
                )
            );
            waitInv.addItem(chooseTeam);
        }

        // Book how to play
        Item howToPlay = new Item.ItemBuilder(Material.WRITTEN_BOOK)
            .withName(getStringsApi().get("messages.waiting-inventory.how-to-play").toString())
            .withPosition(1)
            .addBookPages(getStringsApi().getList("messages.waiting-inventory.help-pages").toStringList())
            .build();
        waitInv.addItem(howToPlay);

        // Fish leave game
        if (getApi().getConfig().getBoolean("config.bungee.enabled", Constants.DEFAULT_BUNGEE_ENABLED)) {
            Item leaveGame = new Item.ItemBuilder(Material.COD)
                .withDamage((short) 3)
                .withPosition(8)
                .withName(getStringsApi().get("messages.waiting-inventory.leave-game").toString())
                .build();
            leaveGame.setActions(
                Lists.newArrayList(
                    new BungeeTpAction(getApi().getConfig().getString("config.bungee.server", Constants.DEFAULT_LOBBY_SERVER))
                )
            );
            waitInv.addItem(leaveGame);
        }

        return waitInv;
    }

    @Override
    public HCTeam getForcedOnlineWinningTeam(List<HCTeam> onlineTeams) {
        HCTeam winner = null;
        boolean isSameScore = false;
        for (HCTeam team : onlineTeams) {
            if (team.getKills() > 0) {
                if (winner == null) {
                    winner = team;
                } else {
                    if (team.getKills() == winner.getKills()) {
                        isSameScore = true;
                    } else if (team.getKills() > winner.getKills()) {
                        isSameScore = false;
                        winner = team;
                    }
                }
            }
        }
        if (isSameScore) {
            return null;
        }

        return winner;
    }

    @Override
    public void joinTeamHintOnJoin(HCPlayer hcPlayer) {
        if (getConfig().getBoolean("config.display-hint-join-running-game-on-join", Constants.DEFAULT_DISPLAY_HINT_RUNNING_GAME_ON_JOIN)) {
            api.sync(() -> {
                getStringsApi().get("messages.play.help-after-join").sendChatP(hcPlayer);
            }, 40);
            if (!hcPlayer.hasTeam()) {
                api.getPlugin().getServer().dispatchCommand(hcPlayer.getPlayer(), "play");
            }
        }
    }

    @Override
    public void performPlayCommand(HCPlayer hcPlayer) {

        if (hcPlayer.hasTeam()) {

            getPmApi().revivePlayer(hcPlayer);
            getStringsApi().get("messages.play.joined-the-game")
                .replace("%player%", hcPlayer.getColoredName())
                .replace("%team%", hcPlayer.getTeam().getColoredName())
                .sendChatP();
        } else if (getApi().getDependencies().isSimpleInventoryGUIEnabled()) {
            SIG.getPlugin().getAPI().openInventoryForPlayer(hcPlayer.getPlayer(), getCallbacksApi().getChooseTeamInventoryName());
        }

    }


}
