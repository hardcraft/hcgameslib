package com.gmail.val59000mc.hcgameslib.api.impl;

import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import org.bukkit.ChatColor;
import org.bukkit.boss.BossBar;
import org.bukkit.command.CommandSender;

import java.util.List;

public class HCMessage {

    private String content;

    // api
    private HCStringsAPI api;

    public HCMessage(HCStringsAPI api, String content, boolean colored) {
        this.content = content;
        this.api = api;

        if (colored)
            translateColors();
        else
            stripColors();
    }

    public HCMessage(HCMessage hcMessage, String string) {
        this.api = hcMessage.api;
        this.content = string;
        translateColors();
    }

    public HCMessage replace(String pattern, String replacement) {
        content = content.replaceAll(pattern, replacement);
        return this;
    }

    public void stripColors() {
        this.content = ChatColor.stripColor(this.content);
    }

    public void translateColors() {
        this.content = ChatColor.translateAlternateColorCodes('&', this.content);
    }

    @Override
    public String toString() {
        return content;
    }

    public HCMessage prepend(String text) {
        return new HCMessage(this, text + content);
    }

    public HCMessage append(String text) {
        return new HCMessage(this, content + text);
    }

    public HCMessage prepend(HCMessage msg) {
        return new HCMessage(this, msg.toString() + content);
    }

    public HCMessage append(HCMessage msg) {
        return new HCMessage(this, content + msg.toString());
    }

    // apis

    public HCMessage sendTitle(int fadeIn, int stay, int fadeOut) {
        api.getTitleAPI().sendTitle(this, fadeIn, stay, fadeOut);
        return this;
    }

    public HCMessage sendTitle(HCPlayer player, int fadeIn, int stay, int fadeOut) {
        api.getTitleAPI().sendTitle(player, this, fadeIn, stay, fadeOut);
        return this;
    }

    public HCMessage sendTitle(HCTeam team, int fadeIn, int stay, int fadeOut) {
        api.getTitleAPI().sendTitle(team, this, fadeIn, stay, fadeOut);
        return this;
    }


    public HCMessage sendTitle(List<HCPlayer> players, int fadeIn, int stay, int fadeOut) {
        api.getTitleAPI().sendTitle(players, this, fadeIn, stay, fadeOut);
        return this;
    }


    public HCMessage sendChat() {
        api.getChatAPI().sendChat(this);
        return this;
    }

    public HCMessage sendChat(CommandSender sender) {
        api.getChatAPI().sendChat(sender, this);
        return this;
    }

    public HCMessage sendChatP(CommandSender sender) {
        api.getChatAPI().sendChatP(sender, this);
        return this;
    }

    public HCMessage sendChat(HCPlayer player) {
        api.getChatAPI().sendChat(player, this);
        return this;
    }


    public HCMessage sendChat(HCTeam team) {
        api.getChatAPI().sendChat(team, this);
        return this;
    }


    public HCMessage sendChat(List<HCPlayer> players) {
        api.getChatAPI().sendChat(players, this);
        return this;
    }


    public HCMessage sendChatP() {
        api.getChatAPI().sendChatP(this);
        return this;
    }


    public HCMessage sendChatP(HCPlayer player) {
        api.getChatAPI().sendChatP(player, this);
        return this;
    }


    public HCMessage sendChatP(HCTeam team) {
        api.getChatAPI().sendChatP(team, this);
        return this;
    }


    public HCMessage sendChatP(List<HCPlayer> players) {
        api.getChatAPI().sendChatP(players, this);
        return this;
    }


    public HCMessage sendBossBar(BossBar bossBar) {
        api.getBossBarAPI().sendBossBar(bossBar, this);
        return this;
    }


    public HCMessage sendBossBar(BossBar bossBar, HCPlayer player) {
        api.getBossBarAPI().sendBossBar(bossBar, player, this);
        return this;
    }


    public HCMessage sendBossBar(BossBar bossBar, HCTeam team) {
        api.getBossBarAPI().sendBossBar(bossBar, team, this);
        return this;
    }


    public HCMessage sendBossBar(BossBar bossBar, List<HCPlayer> players) {
        api.getBossBarAPI().sendBossBar(bossBar, players, this);
        return this;
    }


    public HCMessage sendActionBar() {
        api.getActionBarAPI().sendActionBar(this);
        return this;
    }


    public HCMessage sendActionBar(HCPlayer player) {
        api.getActionBarAPI().sendActionBar(player, this);
        return this;
    }


    public HCMessage sendActionBar(HCTeam team) {
        api.getActionBarAPI().sendActionBar(team, this);
        return this;
    }


    public HCMessage sendActionBar(List<HCPlayer> players) {
        api.getActionBarAPI().sendActionBar(players, this);
        return this;
    }

    public HCMessage sendActionBar(int seconds) {
        api.getActionBarAPI().sendActionBar(this, seconds);
        return this;
    }


    public HCMessage sendActionBar(HCPlayer player, int seconds) {
        api.getActionBarAPI().sendActionBar(player, this, seconds);
        return this;
    }


    public HCMessage sendActionBar(HCTeam team, int seconds) {
        api.getActionBarAPI().sendActionBar(team, this, seconds);
        return this;
    }


    public HCMessage sendActionBar(List<HCPlayer> players, int seconds) {
        api.getActionBarAPI().sendActionBar(players, this, seconds);
        return this;
    }
}
