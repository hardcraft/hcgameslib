package com.gmail.val59000mc.hcgameslib.api;

import java.util.List;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;

public interface HCTitleAPI {
	
	/**
	 * Send a title to all players
	 * @param message
	 * @param fadeIn
	 * @param stay
	 * @param fadeOut
	 */
	public void sendTitle(HCMessage message, int fadeIn, int stay, int fadeOut);
	
	/**
	 * Send a title to a specific player
	 * @param player
	 * @param message
	 * @param fadeIn
	 * @param stay
	 * @param fadeOut
	 */
	public void sendTitle(HCPlayer player , HCMessage message, int fadeIn, int stay, int fadeOut);
	
	/**
	 * Send a title to a team
	 * @param team
	 * @param message
	 * @param fadeIn
	 * @param stay
	 * @param fadeOut
	 */
	public void sendTitle(HCTeam team , HCMessage message, int fadeIn, int stay, int fadeOut);
	
	/**
	 * Send a title to a list of players
	 * @param players
	 * @param message
	 * @param fadeIn
	 * @param stay
	 * @param fadeOut
	 */
	public void sendTitle(List<HCPlayer> players , HCMessage message, int fadeIn, int stay, int fadeOut);
	
	
}
