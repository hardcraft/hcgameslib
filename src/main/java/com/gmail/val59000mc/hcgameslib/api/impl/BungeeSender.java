package com.gmail.val59000mc.hcgameslib.api.impl;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCBungeeAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Bungee;

public class BungeeSender implements HCBungeeAPI{
	
	private HCGame game;
	
	public BungeeSender(HCGame game) {
		this.game = game;
	}

	@Override
	public void sendToServer(String server) {
		for(HCPlayer hcPlayer : game.getPlayersManagerAPI().getPlayers(true, null)){
			Bungee.sendPlayerToServer(game.getPlugin(), hcPlayer.getPlayer(), server);
		}
	}

	@Override
	public void sendToServer(HCPlayer hcPlayer, String server) {
		if(hcPlayer.isOnline()){
			Bungee.sendPlayerToServer(game.getPlugin(), hcPlayer.getPlayer(), server);
		}
	}

	@Override
	public void sendToServer(List<HCPlayer> hcPlayers, String server) {
		for(HCPlayer hcPlayer : hcPlayers){
			if(hcPlayer.isOnline()){
				Bungee.sendPlayerToServer(game.getPlugin(), hcPlayer.getPlayer(), server);
			}
		}
	}

	@Override
	public void sendToServer(HCTeam hcTeam, String server) {
		for(HCPlayer hcPlayer : hcTeam.getMembers()){
			if(hcPlayer.isOnline()){
				Bungee.sendPlayerToServer(game.getPlugin(), hcPlayer.getPlayer(), server);
			}
		}
	}

	@Override
	public void sendAllToServer(String server) {
		for(Player player : Bukkit.getOnlinePlayers()){
			Bungee.sendPlayerToServer(game.getPlugin(), player, server);
		}
	}

}
