package com.gmail.val59000mc.hcgameslib.api;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import org.bukkit.command.CommandSender;

import java.util.List;

public interface HCChatAPI {
	/**
	 * Send a raw message to all players
	 * @param message
	 */
	public void sendChat(HCMessage msg);
	

	/** 
	 * Send a raw message to a command sender
	 * @param sender
	 * @param msg
	 */
	public void sendChat(CommandSender sender, HCMessage msg);
	
	/** 
	 * Send a game prefixed message to a command sender
	 * @param sender
	 * @param msg
	 */
	public void sendChatP(CommandSender sender, HCMessage msg);
	
	/**
	 * Send a raw message to a specific player
	 * @param player
	 * @param message
	 */
	public void sendChat(HCPlayer player , HCMessage msg);
	
	/**
	 * Send a raw message to a team
	 * @param team
	 * @param message
	 */
	public void sendChat(HCTeam team , HCMessage msg);
	
	/**
	 * Send a raw message to a list of players
	 * @param players
	 * @param message
	 */
	public void sendChat(List<HCPlayer> players , HCMessage msg);
	
	/**
	 * Send a game prefixed message to all players
	 * @param message
	 */
	public void sendChatP(HCMessage msg);
	
	/**
	 * Send a game prefixed message to a specific player
	 * @param player
	 * @param message
	 */
	public void sendChatP(HCPlayer player , HCMessage msg);
	
	/**
	 * Send a game prefixed message to a team
	 * @param team
	 * @param message
	 */
	public void sendChatP(HCTeam team , HCMessage msg);
	
	/**
	 * Send a game prefixed message to a list of players
	 * @param players
	 * @param message
	 */
	public void sendChatP(List<HCPlayer> players , HCMessage msg);
	

}
