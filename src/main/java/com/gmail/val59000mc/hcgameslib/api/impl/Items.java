package com.gmail.val59000mc.hcgameslib.api.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.api.HCItemsAPI;
import com.gmail.val59000mc.hcgameslib.api.impl.items.ItemStore;
import com.gmail.val59000mc.hcgameslib.api.impl.items.ItemStoreBuilder;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;

public class Items implements HCItemsAPI {

	private Map<String,Stuff> stuffs;
	private Map<String,ItemStore> itemsStores;
	
	public Items(){
		this.stuffs = new HashMap<>();
		this.itemsStores = new HashMap<>();
	}
	
	@Override
	public void giveStuffItemsToPlayer(HCPlayer hcPlayer) {
		Stuff stuff = hcPlayer.getStuff();
		if(hcPlayer.isOnline() && stuff != null){
			Log.debug("Giving stuff '"+stuff.getName()+"' to "+hcPlayer.getName());
			Player player = hcPlayer.getPlayer();
			if(stuff.getArmor() != null)
				Inventories.setArmor(player, stuff.getArmor());
			if(stuff.getInventory() != null)
				Inventories.give(player, stuff.getInventory());
		}
	}

	@Override
	public Stuff getStuff(String name) {
		return stuffs.get(name);
	}

	@Override
	public void addStuff(Stuff stuff) {
		this.stuffs.put(stuff.getName(), stuff);
	}

	@Override
	public void addStuffs(List<Stuff> stuffs) {
		for(Stuff stuff : stuffs){
			this.stuffs.put(stuff.getName(), stuff);
		}
	}

	@Override
	public ItemStoreBuilder buildItemStore(Material material) {
		return ItemStoreBuilder.from(material);
	}

	@Override
	public ItemStoreBuilder buildItemStore(Material... materials) {
		return ItemStoreBuilder.from(materials);
	}

	@Override
	public void addItemStore(String name, ItemStore store) {
		this.itemsStores.put(name, store);
	}

	@Override
	public ItemStore getItemStore(String name) {
		return itemsStores.get(name);
	}

	@Override
	public void randomFillInventory(Inventory inventory, List<ItemStack> items) {
		List<ItemStack> clones = Lists.newArrayList(items);
		List<Integer> availableSlots = new ArrayList<>();
		for(int i=0 ; i<inventory.getSize() ; i++){
			ItemStack item = inventory.getItem(i);
			if(item == null || item.getType().equals(Material.AIR))
				availableSlots.add(i);
		}
		
		
		while(!clones.isEmpty() && !availableSlots.isEmpty()){
			Integer slot = availableSlots.remove(Randoms.randomInteger(0, availableSlots.size()-1));
			ItemStack item = clones.remove(Randoms.randomInteger(0, clones.size()-1));
			inventory.setItem(slot, item);
		}
		
	}

}
