package com.gmail.val59000mc.hcgameslib.api.impl.items;

import java.util.Collection;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public interface ItemStore {
	
	ItemStack getRandomItem();
	
	ItemStack getRandomItem(Material material);
	
	ItemStack getRandomItem(Material... materials);
	
	List<ItemStack> getRandomItems(int amount);
	
	List<ItemStack> getRandomItems(int amount, Material material);
	
	List<ItemStack> getRandomItems(int amount, Material... materials);
	
	List<ItemStack> getAllItems(Material material);
	
	List<ItemStack> getAllItems(Material... materials);
	
	ItemStore store(ItemStoreBuilder builder, int totalItems);
	
	ItemStore store(ItemStack item);
	
	ItemStore store(ItemStack... items);
	
	ItemStore store(Collection<ItemStack> items);
}
