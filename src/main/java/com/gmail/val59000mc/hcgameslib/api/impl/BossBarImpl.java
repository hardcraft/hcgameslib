package com.gmail.val59000mc.hcgameslib.api.impl;

import com.gmail.val59000mc.hcgameslib.api.HCBossBarAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Stream;

/**
 * Allow to send boss bar to players
 * Not implemented yet
 * Maybe look into https://www.spigotmc.org/resources/api-bossbarapi-1-7-1-8.7504/
 */
public class BossBarImpl implements HCBossBarAPI {

    private HCGame game;

    public BossBarImpl(HCGame game) {
        this.game = game;
    }

    @Override
    public BossBar createBossBar(BarColor barColor, BarStyle barStyle, BarFlag... flags) {
        return Bukkit.createBossBar("", barColor, barStyle, flags);
    }

    @Override
    public void updateBossBar(BossBar bossBar, BarColor barColor) {
        bossBar.setColor(barColor);
    }

    @Override
    public void updateBossBar(BossBar bossBar, BarStyle barStyle) {
        bossBar.setStyle(barStyle);
    }

    @Override
    public void updateBossBar(BossBar bossBar, double progress) {
        bossBar.setProgress(progress);
    }

    @Override
    public void deleteBossBar(BossBar bossBar) {
        bossBar.setVisible(false);
        bossBar.removeAll();
    }

    @Override
    public void sendBossBar(BossBar bossBar, HCMessage msg) {
        Stream<Player> players = game.getPlayersManagerAPI().getPlayers()
            .stream()
            .map(HCPlayer::getPlayer);
        sendBossBar(bossBar, players, msg);
    }

    @Override
    public void sendBossBar(BossBar bossBar, HCPlayer hcPlayer, HCMessage msg) {
        sendBossBar(bossBar, Stream.of(hcPlayer.getPlayer()), msg);
    }

    @Override
    public void sendBossBar(BossBar bossBar, HCTeam team, HCMessage msg) {
        Stream<Player> players = team.getMembers().stream().map(HCPlayer::getPlayer);
        sendBossBar(bossBar, players, msg);
    }

    @Override
    public void sendBossBar(BossBar bossBar, List<HCPlayer> hcPlayers, HCMessage msg) {
        Stream<Player> players = hcPlayers.stream().map(HCPlayer::getPlayer);
        sendBossBar(bossBar, players, msg);
    }

    private void sendBossBar(BossBar bossBar, Stream<Player> players, HCMessage msg) {
        players.forEach(p -> {
            if (p != null) {
                bossBar.addPlayer(p);
            }
        });
        bossBar.setVisible(true);
        bossBar.setTitle(msg.toString());
    }


}
