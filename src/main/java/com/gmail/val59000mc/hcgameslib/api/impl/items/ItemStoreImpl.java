package com.gmail.val59000mc.hcgameslib.api.impl.items;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.gmail.val59000mc.spigotutils.items.MetaBuilder;
import com.google.common.collect.Lists;

public class ItemStoreImpl implements ItemStore{

	private Map<Material,List<ItemStack>> items;
	private List<ItemStack> flattenedItems;

	public ItemStoreImpl(){
		this.items = new HashMap<>();
		this.flattenedItems = new ArrayList<>();
		flattenItemsMap();
	}
	
	////////////
	// internal
	////////////
	
	private void flattenItemsMap(){
		List<ItemStack> flattenedItems = new ArrayList<>();
		for(List<ItemStack> itemsList : items.values()){
			for(ItemStack item : itemsList){
				flattenedItems.add(item.clone());				
			}
		}
		this.flattenedItems = Collections.unmodifiableList(flattenedItems);
	}
	
	private void storeNoFlatten(ItemStack item){
		List<ItemStack> itemsList = items.get(item.getType());
		if(itemsList == null)
			items.put(item.getType(), Lists.newArrayList(item));
		else
			itemsList.add(item);
	}
	
	private void validateNotEmpty(Material material){
		List<ItemStack> list = items.get(material);
		if(list == null)
			throw new IllegalArgumentException("items list of material "+material.name()+" has not been set.");
		if(list.size() == 0)
			throw new IllegalArgumentException("items list of material "+material.name()+" is empty.");
	}
	
	private void validateNotEmpty(List<ItemStack> items){
		if(items == null)
			throw new IllegalArgumentException("items list is null");
		if(items.size() == 0)
			throw new IllegalArgumentException("items list is empty");
	}
	
	private <T extends Object> T randomElement(T...elements){
		return elements[Randoms.randomInteger(0, elements.length-1)];
	}
	
	private <T extends Object> T randomElement(List<T> elements){
		return elements.get(Randoms.randomInteger(0, elements.size()-1));
	}
	
	private <T extends Object> T randomElement(List<T> elements, T defaultValueIfEmpty){
		if(elements.size() == 0)
			return defaultValueIfEmpty;
		return elements.get(Randoms.randomInteger(0, elements.size()-1));
	}

	
	//////////////////////
	// ItemStore interface
	///////////////////////
	
	@Override
	public ItemStack getRandomItem() {
		validateNotEmpty(flattenedItems);
		return flattenedItems.get(Randoms.randomInteger(0, flattenedItems.size()-1)).clone();
	}

	@Override
	public ItemStack getRandomItem(Material material) {
		validateNotEmpty(material);
		List<ItemStack> list = items.get(material);
		return list.get(Randoms.randomInteger(0, list.size()-1)).clone();
	}

	@Override
	public ItemStack getRandomItem(Material... materials) {
		for(Material mat : materials){
			validateNotEmpty(mat);
		}
		return getRandomItem(randomElement(materials));
	}

	@Override
	public List<ItemStack> getRandomItems(int amount) {
		List<ItemStack> items = new ArrayList<>();
		for(int i=0 ; i<amount ; i++){
			items.add(getRandomItem());
		}
		return items;
	}

	@Override
	public List<ItemStack> getRandomItems(int amount, Material material) {
		List<ItemStack> items = new ArrayList<>();
		for(int i=0 ; i<amount ; i++){
			items.add(getRandomItem(material));
		}
		return items;
	}

	@Override
	public List<ItemStack> getRandomItems(int amount, Material... materials) {
		List<ItemStack> items = new ArrayList<>();
		for(int i=0 ; i<amount ; i++){
			items.add(getRandomItem(materials));
		}
		return items;
	}

	@Override
	public List<ItemStack> getAllItems(Material material) {
		validateNotEmpty(material);
		List<ItemStack> items = new ArrayList<>();
		for(ItemStack item : this.items.get(material)){
			items.add(item.clone());
		}
		return items;
	}

	@Override
	public List<ItemStack> getAllItems(Material... materials) {
		List<ItemStack> items = new ArrayList<>();
		for(Material mat : materials){
			validateNotEmpty(mat);
			for(ItemStack item : this.items.get(mat)){
				items.add(item.clone());
			}
		}
		return items;
	}

	@Override
	public ItemStore store(ItemStoreBuilder isb, int maxItems) {
		int itemsBuilt = 0;
		while(itemsBuilt < maxItems){
			Material material = randomElement(isb.getMaterials());
			ItemBuilder itemBuilder = new ItemBuilder(material)
				.withAmount(randomElement(isb.getAmounts(), 1))
				.withDurability(randomElement(isb.getDurabilities(), 0));

			if(!isb.getDurabilityPercentages().isEmpty()){
				Integer maxDurability = (int) material.getMaxDurability();
				Double percentage = (double) randomElement(isb.getDurabilityPercentages());
				itemBuilder.withDurability((short) (maxDurability - (Math.round((double) maxDurability * (double) percentage / 100d))));
			}
			
			MetaBuilder metaBuilder = itemBuilder.buildMeta();
			
			if(!isb.getNames().isEmpty()){
				metaBuilder.withDisplayName(randomElement(isb.getNames()));
			}
			
			
			if(!isb.getLores().isEmpty()){
				metaBuilder.withLore(randomElement(isb.getLores()));
			}
			
			if(!isb.getEnchantments().isEmpty()){
				int enchNumber = randomElement(isb.getEnchantmentsNumbers(), 1);
				for(int i=0 ; i<enchNumber ; i++){
					Enchantment ench = randomElement(Lists.newArrayList(isb.getEnchantments().keySet()));
					Integer level = randomElement(isb.getEnchantments().get(ench), 1);
					metaBuilder.withEnchant(ench, level, true);
				}
			}
			
			ItemStack item = itemBuilder.withMeta(metaBuilder.build()).build();
			
			if(!isb.getModifiers().isEmpty()){
				randomElement(isb.getModifiers()).modify(isb, item);
			}
			
			storeNoFlatten(item);
			
			itemsBuilt++;
		}
		
		flattenItemsMap();

		return this;
	}

	@Override
	public ItemStore store(ItemStack item) {
		storeNoFlatten(item);
		flattenItemsMap();
		return this;
	}

	@Override
	public ItemStore store(ItemStack... items) {
		return store(Lists.newArrayList(items));
	}

	@Override
	public ItemStore store(Collection<ItemStack> items) {
		for(ItemStack item : items){
			storeNoFlatten(item);
		}
		flattenItemsMap();
		return this;
	}
	
	
	
}
