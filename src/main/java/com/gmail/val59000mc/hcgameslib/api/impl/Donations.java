package com.gmail.val59000mc.hcgameslib.api.impl;

import com.gmail.val59000mc.hcgameslib.HCGamesLib;
import com.gmail.val59000mc.hcgameslib.api.HCDonationsAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.donations.Donation;
import com.gmail.val59000mc.hcgameslib.donations.DonationResponse;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import org.bukkit.event.EventHandler;

import javax.sql.rowset.CachedRowSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Donations extends HCListener implements HCDonationsAPI {

    private String selectPlayerDonations;
    private String insertPlayerDonation;

    private static ZoneId SYSTEM_TIMEZONE = ZoneId.of(ZoneOffset.systemDefault().getId());

    public Donations(HCGameAPI api) {
        setApi(api);
    }

    private void delayInit() {
    }

    private void readQueries() {
    }

    @EventHandler
    public void onPlayerInsertedInDb(HCPlayerDBInsertedEvent e){
        getApi().async(()->refreshDonations(e.getHcPlayer()));
    }

    @Override
    public List<Donation> findDonations(HCPlayer hcPlayer) {

        List<Donation> donations = new ArrayList<>();

        HCMySQLAPI sql = getMySQLAPI();
        if (sql.isEnabled()) {

            try {

                CachedRowSet res = sql.query(sql.prepareStatement(selectPlayerDonations, hcPlayer.getId()));
                while (res.next()) {
                    Donation donation = fetchDonation(res, hcPlayer);
                    donations.add(donation);
                }

            } catch (SQLException e) {
                String err = getStringsApi().get("messages.donations.could-not-find-donations").toString();
                Log.severe(err);
                e.printStackTrace();
            }
        }

        return donations;

    }

    @Override
    public List<Donation> findDonations(String playerName) {

        HCMySQLAPI sql = getMySQLAPI();

        List<Donation> donations = new ArrayList<>();

        if (sql.isEnabled()) {

            ZonedDateTime now = ZonedDateTime.now(SYSTEM_TIMEZONE);

            int playerId = 0;
            UUID playerUUID = null;

            try {

                CachedRowSet resPlayer = sql.selectPlayerByName(playerName);
                if (resPlayer.first()) {
                    playerId = resPlayer.getInt("id");
                    playerUUID = UUID.fromString(resPlayer.getString("uuid"));
                } else {
                    return donations;
                }

                CachedRowSet res = sql.query(sql.prepareStatement(selectPlayerDonations, playerId));
                while (res.next()) {
                    Donation donation = fetchDonation(res, playerName, playerUUID, playerId);
                    donations.add(donation);
                }


            } catch (SQLException e) {
                e.printStackTrace();
                Log.severe("Unable to find donations for player name="+playerName+" uuid="+playerUUID+" id="+playerId);
            }
        }

        return donations;


    }

    @Override
    public DonationResponse createDonation(String playerName) {

        HCMySQLAPI sql = getMySQLAPI();

        if (sql.isEnabled()) {

            ZonedDateTime now = ZonedDateTime.now(SYSTEM_TIMEZONE);

            try {

                CachedRowSet resPlayer = sql.selectPlayerByName(playerName);
                int playerId;
                UUID playerUUID;
                if (resPlayer.first()) {
                    playerId = resPlayer.getInt("id");
                    playerUUID = UUID.fromString(resPlayer.getString("uuid"));
                } else {
                    return new DonationResponse(false,
                            getStringsApi().get("messages.donations.unknown-player-name").replace("%player%", playerName),
                            null);
                }

                PreparedStatement insert = sql.prepareStatement(
                        insertPlayerDonation,
                        playerId,
                        new Timestamp(now.toEpochSecond() * 1000L)
                );

                CachedRowSet insertedId = sql.executeWithGeneratedIds(insert);
                insertedId.first();
                int donationId = insertedId.getInt(1);

                insert.close();
                insert.getConnection().close();

                Donation donation = new Donation(donationId, playerName, playerUUID, playerId, now);

                Log.info("New donation created : "+donation);

                return new DonationResponse(
                        true,
                        getStringsApi().get("messages.donations.created")
                                .replace("%player%", playerName),
                        donation
                );

            } catch (SQLException e) {
                e.printStackTrace();

                String err = getStringsApi()
                        .get("messages.donations.could-not-create")
                        .replace("%player%", playerName)
                        .toString();

                Log.severe(err);

                return new DonationResponse(
                        false,
                        getStringsApi()
                            .get("messages.donations.could-not-create")
                            .replace("%player%", playerName),
                        null
                );
            }
        }

        return new DonationResponse(false, getStringsApi().get("messages.donations.mysql-disabled"), null);

    }

    @Override
    public void refreshDonations(HCPlayer hcPlayer) {
        List<Donation> donations = findDonations(hcPlayer);
        hcPlayer.setDonations(donations);
        getPmApi().refreshRankNameTag(hcPlayer);
    }

    private Donation fetchDonation(CachedRowSet row, HCPlayer hcPlayer) throws SQLException {

        int id = row.getInt("id");
        Date donationDateSql = row.getTimestamp("donation_date");
        ZonedDateTime donationDate = ZonedDateTime.ofInstant(donationDateSql.toInstant(), SYSTEM_TIMEZONE);
        return new Donation(id, hcPlayer.getName(), hcPlayer.getUuid(), hcPlayer.getId(), donationDate);

    }

    private Donation fetchDonation(CachedRowSet row, String playerName, UUID playerUuid, int playerId) throws SQLException {

        int id = row.getInt("id");
        Date donationDateSql = row.getTimestamp("donation_date");
        ZonedDateTime donationDate = ZonedDateTime.ofInstant(donationDateSql.toInstant(), SYSTEM_TIMEZONE);
        return new Donation(id, playerName, playerUuid, playerId, donationDate);

    }
}
