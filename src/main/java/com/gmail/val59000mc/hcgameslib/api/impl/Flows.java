package com.gmail.val59000mc.hcgameslib.api.impl;

import com.gmail.val59000mc.hcgameslib.api.HCFlowsAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Flows implements HCFlowsAPI {

    private final HCGameAPI api;
    private final boolean enabled;
    private final String nextFlowServer;
    private final FlowOrder flowOrder;


    public Flows(HCGameAPI api) {
        this.api = api;
        FileConfiguration cfg = api.getConfig();
        this.enabled = cfg.getBoolean("config.flow.enable", Constants.DEFAULT_ENABLE_GAME_ROTATION);
        this.nextFlowServer = cfg.getString("config.flow.next-server", Constants.DEFAULT_LOBBY_SERVER);
        FlowOrder order = FlowOrder.ALPHABETICAL;
        try {
            order = FlowOrder.valueOf(cfg.getString("config.flow.order"));
        } catch (IllegalArgumentException ex) {
            // swallow : keep default value
        }
        this.flowOrder = order;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getNextFlowServer() {
        return nextFlowServer;
    }

    public String randomizeNextGame() {
        if (!isEnabled())
            return null;

        Log.info("Setting next flow game...");

        File flowsDir = new File("flows");
        if (flowsDir.exists() && flowsDir.isDirectory()) {
            File flowCfg = new File(flowsDir, "flow.cfg");
            if (flowCfg.exists() && flowCfg.isFile()) {

                try {
                    String currentFlow = Files.readFirstLine(flowCfg, StandardCharsets.UTF_8);
                    Log.info("Current flow is " + currentFlow);

                    List<String> otherFlows = Arrays.stream(flowsDir.listFiles((file) -> file.isDirectory()))
                        .map(File::getName)
                        .filter(fileName -> fileName.startsWith("flow_") && !fileName.equals(currentFlow))
                        .sorted(String::compareToIgnoreCase)
                        .collect(Collectors.toList());

                    if (otherFlows.size() > 0) {
                        String nextFlow = getNextFlow(currentFlow, otherFlows);
                        Log.info("Next flow will be " + nextFlow);
                        java.nio.file.Files.write(
                            flowCfg.toPath(),
                            Lists.newArrayList(nextFlow),
                            StandardCharsets.UTF_8,
                            StandardOpenOption.WRITE
                        );
                        return currentFlow;
                    } else {
                        Log.severe("There must be at least two flows directories inside " + flowsDir.getAbsolutePath());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    Log.severe("An error occured while processing flows files. Aborting.");
                }
            } else {
                Log.severe(flowCfg.getAbsolutePath() + " file couldn't be found. Aborting.");
            }
        } else {
            Log.severe(flowsDir.getAbsolutePath() + " directory couldn't be found. Aborting.");
        }

        return null;
    }

    private String getNextFlow(String currentFlow, List<String> otherFlows) {
        switch (flowOrder) {
            case ALPHABETICAL: {
                ArrayList<String> allFlows = Lists.newArrayList(otherFlows);
                allFlows.add(currentFlow);
                allFlows.sort(String::compareToIgnoreCase);
                int nextFlowIndex = allFlows.indexOf(currentFlow) + 1;
                if (nextFlowIndex == allFlows.size()) {
                    nextFlowIndex = 0;
                }
                return allFlows.get(nextFlowIndex);
            }
            case RANDOM:
                return otherFlows.get(Randoms.randomInteger(0, otherFlows.size() - 1));
            default:
                return currentFlow;
        }
    }

    public void executeFlowCopyFilesScript() {
        if (!isEnabled())
            return;

        try {
            System.out.println("Executing flow-stop script");
            Runtime.getRuntime().exec(new String[]{"./flows/rotate.sh"});
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private enum FlowOrder {
        ALPHABETICAL,
        RANDOM
    }

}
