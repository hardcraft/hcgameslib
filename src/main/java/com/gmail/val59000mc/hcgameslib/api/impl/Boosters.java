package com.gmail.val59000mc.hcgameslib.api.impl;

import com.gmail.val59000mc.hcgameslib.HCGamesLib;
import com.gmail.val59000mc.hcgameslib.api.HCBoostersAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.boosters.Booster;
import com.gmail.val59000mc.hcgameslib.boosters.BoosterInventoryManager;
import com.gmail.val59000mc.hcgameslib.boosters.BoosterListResponse;
import com.gmail.val59000mc.hcgameslib.boosters.BoosterResponse;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import org.bukkit.Sound;

import javax.sql.rowset.CachedRowSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Boosters implements HCBoostersAPI {

	private HCGameAPI api;
	private HCStringsAPI s;

	private String insertPlayerBooster;
	private String deletePlayerBooster;
	private String selectPlayerBoosterById;
	private String selectPlayerActiveBoosters;
	private String selectPlayerCurrentBoosters;
	private String updateStartPlayerBooster;

	private BoosterInventoryManager inventoryManager;
	private Map<Integer, Booster> activeBoosters;
	private Booster bestBooster;
	private boolean broadcastBoosterChange;

	private static ZoneId SYSTEM_TIMEZONE = ZoneId.of(ZoneOffset.systemDefault().getId());

	public Boosters(HCGameAPI api) {
		this.api = api;
		this.s = api.getStringsAPI();
	}

	public HCGameAPI getApi() {
		return api;
	}

	public void initialize() {
		HCMySQLAPI sql = api.getMySQLAPI();
		if (sql.isEnabled()) {
			insertPlayerBooster = sql.readQueryFromResource(HCGamesLib.getPlugin(),
					"sql/insert_player_booster.sql");
			deletePlayerBooster = sql.readQueryFromResource(HCGamesLib.getPlugin(),
					"sql/delete_player_booster.sql");
			selectPlayerBoosterById = sql.readQueryFromResource(HCGamesLib.getPlugin(),
					"sql/select_player_booster_by_id.sql");
			selectPlayerActiveBoosters = sql.readQueryFromResource(HCGamesLib.getPlugin(),
					"sql/select_player_active_boosters.sql");
			selectPlayerCurrentBoosters = sql.readQueryFromResource(HCGamesLib.getPlugin(),
					"sql/select_player_current_boosters.sql");
			updateStartPlayerBooster = sql.readQueryFromResource(HCGamesLib.getPlugin(),
					"sql/update_start_player_boosters.sql");
		}

		this.activeBoosters = new ConcurrentHashMap<>();
		this.inventoryManager = new BoosterInventoryManager(api);
		this.broadcastBoosterChange = api.getConfig().getBoolean("config.boosters.broadcast-change",
				Constants.DEFAULT_BROADCAST_BOOSTER_CHANGE);

		api.buildTask("clear expired boosters", new HCTask() {

			@Override
			public void run() {
				clearOldBoosters();
				replaceAndBroadcastBestBooster();
			}
		}).withDelay(1200).withIterations(-1).withInterval(1200).build().start();
	}

	private void clearOldBoosters() {
		Set<Integer> idsToRemove = new HashSet<Integer>();
		ZonedDateTime now = ZonedDateTime.now(SYSTEM_TIMEZONE);
		for (Booster booster : activeBoosters.values()) {
			if (now.isEqual(booster.getDateEnd()) || now.isAfter(booster.getDateEnd())) {
				HCPlayer hcPlayer = api.getPlayersManagerAPI().getHCPlayer(booster.getPlayerUUID());
				if (hcPlayer != null) {
					hcPlayer.setActiveBooster(null);
					api.sync(() -> api.getStringsAPI().get("messages.boosters.expired")
							.replace("%multiplier%", String.valueOf(booster.getMultiplier()))
							.replace("%time%", String.valueOf(booster.getMinutesDuration())).sendChatP(hcPlayer));

				}
				idsToRemove.add(booster.getId());
			}
		}
		for (Integer idToRemove : idsToRemove) {
			activeBoosters.remove(idToRemove);
		}
	}

	private Booster fetchBooster(CachedRowSet row) throws SQLException {
		int playerId = row.getInt("player_id");
		CachedRowSet playerRes = api.getMySQLAPI().selectPlayerById(playerId);
		playerRes.first();
		String playerName = playerRes.getString("name");
		UUID playerUUID = UUID.fromString(playerRes.getString("uuid"));
		return fetchBooster(row, playerName, playerUUID, playerId);
	}

	private Booster fetchBooster(CachedRowSet row, String playerName, UUID playerUUID, int playerId)
			throws SQLException {

		Booster booster = new Booster(row.getInt("id"), playerName, playerUUID, playerId, row.getDouble("multiplier"),
				row.getInt("minutes_duration"));

		Date dateBoughtSql = row.getTimestamp("date_bought");
		Date dateStartSql = row.getTimestamp("date_start");
		Date dateEndSql = row.getTimestamp("date_end");

		if (dateBoughtSql != null) {
			booster.setDateBought(ZonedDateTime.ofInstant(dateBoughtSql.toInstant(), SYSTEM_TIMEZONE));
		}

		if (dateStartSql != null && dateEndSql != null) {
			booster.setDateStart(ZonedDateTime.ofInstant(dateStartSql.toInstant(), SYSTEM_TIMEZONE));
			booster.setDateEnd(ZonedDateTime.ofInstant(dateEndSql.toInstant(), SYSTEM_TIMEZONE));
		}

		return booster;
	}

	private void replaceAndBroadcastBestBooster() {
		Booster originalBestBooster = bestBooster;
		Booster newBestBooster = bestBooster;

		// verify old best booster matches an online player is still active
		if (newBestBooster != null) {
			ZonedDateTime now = ZonedDateTime.now(SYSTEM_TIMEZONE);
			if (newBestBooster.getDateEnd().isBefore(now)) {
				newBestBooster = null;
			} else {
				HCPlayer hcPlayer = getApi().getPlayersManagerAPI().getHCPlayer(bestBooster.getPlayerUUID());
				if (hcPlayer == null || !hcPlayer.isOnline()) {
					newBestBooster = null;
				}
			}
		}

		for (Booster booster : activeBoosters.values()) {
			if (newBestBooster == null || booster.getMultiplier() > newBestBooster.getMultiplier()) {
				newBestBooster = booster;
			}
		}

		// change and announce new best booster if has changed
		if ((originalBestBooster == null && newBestBooster != null)
				|| (originalBestBooster != null && newBestBooster == null) || (originalBestBooster != null
						&& newBestBooster != null && !originalBestBooster.equals(newBestBooster))) {
			bestBooster = newBestBooster;
			if (broadcastBoosterChange) {
				getBestBoosterMessage().sendChatP();
				api.getSoundAPI().play(Sound.BLOCK_NOTE_BLOCK_HARP, 2, 2f);
			}
		}

	}

	private BoosterResponse mysqlDisabledBoosterResponse() {
		return new BoosterResponse(false, s.get("messages.boosters.mysql-disabled"), null);
	}

	//////////////
	// API
	//////////////

	@Override
	public BoosterResponse loadActiveBooster(HCPlayer hcPlayer) {

		HCMySQLAPI sql = api.getMySQLAPI();

		if (sql.isEnabled()) {
			try {
				CachedRowSet res = sql.query(sql.prepareStatement(selectPlayerActiveBoosters, hcPlayer.getId()));
				if (res.first()) {
					final Booster booster = fetchBooster(res, hcPlayer.getName(), hcPlayer.getUuid(), hcPlayer.getId());
					hcPlayer.setActiveBooster(booster);
					activeBoosters.put(booster.getId(), booster);
					api.sync(() -> replaceAndBroadcastBestBooster());
					return new BoosterResponse(true, new HCMessage(s, "Player has a booster", true), booster);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				String err = "Couldn't load active boosters for player " + hcPlayer.getName();
				Log.severe(err);
				return new BoosterResponse(false, new HCMessage(s, err, true), null);
			}
		}

		return mysqlDisabledBoosterResponse();
	}

	@Override
	public void unloadActiveBooster(HCPlayer hcPlayer) {
		Booster playerActiveBooster = hcPlayer.getActiveBooster();
		if (playerActiveBooster != null) {
			Log.debug("Unload player active booster hcPlayer=" + hcPlayer.getName() + " booster="
					+ hcPlayer.getActiveBooster());
			inventoryManager.destroyInventory(hcPlayer.getName());
			activeBoosters.remove(playerActiveBooster.getId());
			hcPlayer.setActiveBooster(null);
			replaceAndBroadcastBestBooster();
		}
	}

	@Override
	public BoosterListResponse findPlayerCurrentBoosters(int playerId) {

		List<Booster> boosters = new ArrayList<Booster>();

		HCMySQLAPI sql = api.getMySQLAPI();
		if (sql.isEnabled()) {

			try {
				// find player
				CachedRowSet resPlayer = sql.selectPlayerById(playerId);
				String playerName;
				UUID playerUuid;
				if (resPlayer.first()) {
					playerName = resPlayer.getString("name");
					playerUuid = UUID.fromString(resPlayer.getString("uuid"));
				} else {
					return new BoosterListResponse(false, s.get("messages.boosters.unknown-player"), null);
				}

				CachedRowSet res = sql.query(sql.prepareStatement(selectPlayerCurrentBoosters, playerId));
				while (res.next()) {
					boosters.add(fetchBooster(res, playerName, playerUuid, playerId));
				}

			} catch (SQLException e) {
				String err = s.get("messages.boosters.could-not-find-boosters").toString();
				Log.severe(err);
				e.printStackTrace();
				return new BoosterListResponse(false, new HCMessage(s, err, true), null);
			}
		}

		return new BoosterListResponse(true, new HCMessage(s, "Liste", true), boosters);

	}

	@Override
	public BoosterListResponse findPlayerCurrentBoostersByPlayerName(String name) {

		List<Booster> boosters = new ArrayList<Booster>();

		HCMySQLAPI sql = api.getMySQLAPI();
		if (sql.isEnabled()) {

			try {
				// find player
				CachedRowSet resPlayer = sql.selectPlayerByName(name);
				if (resPlayer.first()) {
					return findPlayerCurrentBoosters(resPlayer.getInt("id"));
				} else {
					return new BoosterListResponse(false,
							s.get("messages.boosters.unknown-player-name").replace("%player%", name), null);
				}

			} catch (SQLException e) {
				String err = s.get("messages.boosters.could-not-find-boosters").toString();
				Log.severe(err);
				e.printStackTrace();
				return new BoosterListResponse(false, new HCMessage(s, err, true), null);
			}
		}

		return new BoosterListResponse(true, new HCMessage(s, "Liste", true), boosters);

	}

	@Override
	public BoosterResponse findBoosterById(int boosterId) {
		HCMySQLAPI sql = api.getMySQLAPI();

		if (sql.isEnabled()) {
			try {
				CachedRowSet res = sql.query(sql.prepareStatement(selectPlayerBoosterById, boosterId));
				if (res.first()) {
					final Booster booster = fetchBooster(res);
					return new BoosterResponse(true, new HCMessage(s, "Booster trouvé", true), booster);
				} else {
					return new BoosterResponse(false,
							s.get("messages.boosters.unknown-booster").replace("%id%", String.valueOf(boosterId)),
							null);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				String err = "Couldn't load boosters by id = " + boosterId;
				Log.severe(err);
				return new BoosterResponse(false, new HCMessage(s, err, true), null);
			}
		}

		return mysqlDisabledBoosterResponse();
	}

	@Override
	public BoosterResponse createBooster(String playerName, double multiplier, int minutesDuration) {

		HCMySQLAPI sql = api.getMySQLAPI();

		if (sql.isEnabled()) {

			ZonedDateTime now = ZonedDateTime.now(SYSTEM_TIMEZONE);

			try {

				CachedRowSet resPlayer = sql.selectPlayerByName(playerName);
				int playerId;
				UUID playerUUID;
				if (resPlayer.first()) {
					playerId = resPlayer.getInt("id");
					playerUUID = UUID.fromString(resPlayer.getString("uuid"));
				} else {
					return new BoosterResponse(false,
							s.get("messages.boosters.unknown-player-name").replace("%player%", playerName), null);
				}

				PreparedStatement insert = sql.prepareStatement(insertPlayerBooster, playerId, multiplier,
						minutesDuration, new Timestamp(now.toEpochSecond() * 1000L));
				CachedRowSet insertedId = sql.executeWithGeneratedIds(insert);
				insertedId.first();
				int boosterId = insertedId.getInt(1);

				insert.close();
				insert.getConnection().close();

				CachedRowSet resBooster = sql.query(sql.prepareStatement(selectPlayerBoosterById, boosterId));
				if (resBooster.next()) {
					final Booster booster = fetchBooster(resBooster, playerName, playerUUID, playerId);
					return new BoosterResponse(true,
							s.get("messages.boosters.created").replace("%multiplier%", String.valueOf(multiplier))
									.replace("%time%", String.valueOf(minutesDuration)).replace("%player%", playerName),
							booster);
				}
			} catch (SQLException e) {
				e.printStackTrace();

				String err = s.get("messages.boosters.could-not-create")
						.replace("%multiplier%", String.valueOf(multiplier))
						.replace("%time%", String.valueOf(minutesDuration)).replace("%player%", playerName).toString();

				Log.severe(err);

				return new BoosterResponse(false, new HCMessage(s, err, true), null);
			}
		}

		return mysqlDisabledBoosterResponse();
	}

	@Override
	public BoosterResponse removeBooster(int id) {
		HCMySQLAPI sql = api.getMySQLAPI();

		if (sql.isEnabled()) {

			try {

				int rowsDeleted = sql.execute(sql.prepareStatement(deletePlayerBooster, id));

				HCMessage message = rowsDeleted == 0 ? s.get("messages.command.boosters.booster-not-deleted")
						: s.get("messages.command.boosters.booster-deleted");
				message.replace("%id%", String.valueOf(id));
				return new BoosterResponse(true, message, null);

			} catch (SQLException e) {
				e.printStackTrace();

				String err = s.get("messages.command.boosters.cannot-delete-booster").toString();

				Log.severe(err);

				return new BoosterResponse(false, new HCMessage(s, err, true), null);
			}
		}

		return mysqlDisabledBoosterResponse();
	}

	@Override
	public BoosterResponse activateBooster(Booster booster) {
		HCMySQLAPI sql = api.getMySQLAPI();

		clearOldBoosters();

		if (sql.isEnabled()) {

			ZonedDateTime now = ZonedDateTime.now(SYSTEM_TIMEZONE);

			try {

				// check if there is already an activate booster for this player
				CachedRowSet resBooster = sql
						.query(sql.prepareStatement(selectPlayerActiveBoosters, booster.getPlayerId()));
				if (resBooster.first()) {
					return new BoosterResponse(false,
							s.get("messages.boosters.already-activated").replace("%player%", booster.getPlayerName()),
							null);
				}

				// check if this booster is activable
				CachedRowSet boosterRes = sql.query(sql.prepareStatement(selectPlayerBoosterById, booster.getId()));
				if (!boosterRes.first()) {
					return new BoosterResponse(false,
							s.get("messages.boosters.unknown-booster").replace("%id%", String.valueOf(booster.getId())),
							null);
				}

				Booster boosterInDb = fetchBooster(boosterRes, booster.getPlayerName(), booster.getPlayerUUID(),
						booster.getPlayerId());
				if (boosterInDb.getDateStart() != null || boosterInDb.getDateEnd() != null) {
					return new BoosterResponse(false, s.get("messages.boosters.already-used"), null);
				}

				// activate the booster
				sql.execute(sql.prepareStatement(updateStartPlayerBooster, new Timestamp(now.toEpochSecond() * 1000L),
						new Timestamp(now.plusMinutes(booster.getMinutesDuration()).toEpochSecond() * 1000L),
						booster.getId()));

				return new BoosterResponse(true,
						s.get("messages.boosters.activated")
								.replace("%multiplier%", String.valueOf(booster.getMultiplier()))
								.replace("%time%", String.valueOf(booster.getMinutesDuration())),
						booster);

			} catch (SQLException e) {
				e.printStackTrace();

				String err = s.get("messages.boosters.could-not-activate")
						.replace("%multiplier%", String.valueOf(booster.getMultiplier()))
						.replace("%time%", String.valueOf(booster.getMinutesDuration())).toString();

				Log.severe(err);

				return new BoosterResponse(false, new HCMessage(s, err, true), null);
			}
		}

		return mysqlDisabledBoosterResponse();

	}

	@Override
	public Booster getBestBooster() {
		return bestBooster;
	}

	@Override
	public HCMessage getBestBoosterMessage() {
		HCMessage hcMessage;
		if (bestBooster == null) {
			hcMessage = api.getStringsAPI().get("messages.boosters.no-active-booster");
		} else {
			ZonedDateTime now = ZonedDateTime.now(SYSTEM_TIMEZONE);

			hcMessage = api.getStringsAPI().get("messages.boosters.active-booster")
					.replace("%multiplier%", String.valueOf(bestBooster.getMultiplier()))
					.replace("%player%", String.valueOf(bestBooster.getPlayerName()))
					.replace("%time%", String.valueOf(now.until(bestBooster.getDateEnd(), ChronoUnit.MINUTES)));
		}

		return hcMessage;
	}

	@Override
	public double getCurrentBoosterMultiplier() {
		return bestBooster == null ? 0 : bestBooster.getMultiplier();
	}

	@Override
	public void displayBoostersTo(String owner, HCPlayer hcPlayer, List<Booster> boosters) {
		inventoryManager.createAndDisplay(owner, hcPlayer, boosters);
	}

}
