package com.gmail.val59000mc.hcgameslib.api;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.bukkit.plugin.java.JavaPlugin;

public interface HCMySQLAPI {

	public boolean isEnabled();
	
	public String readQueryFromResource(JavaPlugin plugin, String resourceName); 
	
	public CachedRowSet query(final PreparedStatement preparedStatement) throws SQLException;
	
	public CachedRowSet executeWithGeneratedIds(final PreparedStatement preparedStatement) throws SQLException;
	
	public int execute(final PreparedStatement preparedStatement) throws SQLException;
	
	public PreparedStatement prepareStatement(String query, Object... vars) throws SQLException;

	public void handleException(SQLException e);

	public void refreshMonthAndYear();
	
	public String getMonth();
	
	public String getYear();
	
	
	// game related request
	
	/**
	 * Select a game from database by name
	 * Should be called asynchronously to avoid blocking main thread
	 * @param name
	 * @return
	 */
	public CachedRowSet selectGameByName(String name) throws SQLException;
	
	/**
	 * Select a player from database by uuid
	 * Should be called asynchronously to avoid blocking main thread
	 * @param uuid
	 * @return
	 */
	public CachedRowSet selectPlayerByUUID(String uuid) throws SQLException;
	
	/**
	 * Select a player from database by id
	 * Should be called asynchronously to avoid blocking main thread
	 * @param uuid
	 * @return
	 */
	public CachedRowSet selectPlayerById(int id) throws SQLException;
	
	/**
	 * Select a player from database by name
	 * The underlying implementation should find the most recent player having this name
	 * because there may be duplicate player names
	 * Should be called asynchronously to avoid blocking main thread
	 * @param uuid
	 * @return
	 */
	public CachedRowSet selectPlayerByName(String name) throws SQLException;
	
	/**
	 * Select many players at once from database
	 * Should be called asynchronously to avoid vlocking main thread
	 * @param uuid
	 * @return
	 */
	public CachedRowSet selectMultiplePlayersByUUID(List<String> uuid) throws SQLException;

	/**
	 * Read value from global config
	 * @param key
	 * @return
	 */
	public String selectGlobalConfig(String key) throws SQLException;

	/**
	 * Write value to global config
	 * @param key
	 * @param value
	 * @return
	 */
	public void writeGlobalConfig(String key, String value) throws SQLException;

}
