package com.gmail.val59000mc.hcgameslib.api;

import com.gmail.val59000mc.hcgameslib.donations.Donation;
import com.gmail.val59000mc.hcgameslib.donations.DonationResponse;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

import java.util.List;

public interface HCDonationsAPI {

    List<Donation> findDonations(HCPlayer hcPlayer);

    List<Donation> findDonations(String playerName);

    DonationResponse createDonation(String playerName);

    void refreshDonations(HCPlayer hcPlayer);
}
