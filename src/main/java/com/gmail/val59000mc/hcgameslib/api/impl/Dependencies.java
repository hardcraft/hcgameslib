package com.gmail.val59000mc.hcgameslib.api.impl;

import com.gmail.val59000mc.hcgameslib.api.HCDependenciesAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Dependencies implements HCDependenciesAPI {

    private HCGame game;

    private boolean pexEnabled;
    private boolean sigEnabled;
    private boolean vaultEnabled;
    private boolean nametageditEnabled;


    public Dependencies(HCGame game) {

        this.game = game;

        // Loading SimpleInventoryGUI
        Plugin sigPlugin = Bukkit.getPluginManager().getPlugin("SimpleInventoryGUI");
        if (sigPlugin != null && sigPlugin instanceof SIG) {
            sigEnabled = true;
        } else {
            sigEnabled = false;
        }

    }

    @Override
    public boolean isPermissionsExEnabled() {
        return pexEnabled;
    }

    @Override
    public boolean isSimpleInventoryGUIEnabled() {
        return sigEnabled;
    }

    @Override
    public boolean isVaultEnabled() {
        return vaultEnabled;
    }

    @Override
    public boolean isNametagEditEnabled() {
        return nametageditEnabled;
    }


    // PermissionsEx

    @Override
    public String getPrefix(Player player) {
        return "";
    }

    @Override
    public String getSuffix(Player player) {
        return "";
    }

    @Override
    public String getShortPrefix(Player player) {
        if (isPermissionsExEnabled()) {
            String prefix = getPrefix(player);
            return prefix.replace("Modérateur", "Modo")
                .replace("Administrateur", "Admin")
                .replace("Community", "Commun");
        }
        return "";
    }

    @Override
    public String getStrippedPrefix(Player player) {
        if (isPermissionsExEnabled()) {
            return ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', getPrefix(player)));
        }
        return "";
    }

    @Override
    public void removePerm(String playerName, String permission) {
    }

    @Override
    public void addPerm(String playerName, String permission) {
    }

    // SimpleInventoryGUI

    // Vault


    @Override
    public double addMoney(OfflinePlayer offlinePlayer, Double amount) {
        if (amount == null)
            return 0;
        return 0;
    }

    @Override
    public double addMoney(Player player, Double amount) {
        if (amount == null)
            return 0;
        return 0;
    }

    @Override
    public double getBalance(OfflinePlayer offlinePlayer) {
        return 0;
    }

    @Override
    public double getBalance(Player player) {
        return 0;
    }

    @Override
    public void removeMoney(OfflinePlayer offlinePlayer, Double amount) {
        if (amount == null)
            return;
    }

    @Override
    public void removeMoney(Player player, Double amount) {
        if (amount == null)
            return;
    }

    @Override
    public boolean hasEnoughMoney(OfflinePlayer offlinePlayer, Double amount) {
        if (amount == null) {
            return false;
        }

        return getBalance(offlinePlayer) >= amount;
    }

    @Override
    public boolean hasEnoughMoney(Player player, Double amount) {
        if (amount == null) {
            return false;
        }

        return getBalance(player) >= amount;
    }

}
