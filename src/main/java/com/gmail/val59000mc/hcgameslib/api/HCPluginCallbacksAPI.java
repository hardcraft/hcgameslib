package com.gmail.val59000mc.hcgameslib.api;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.hcgameslib.worlds.WorldManager;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;

public interface HCPluginCallbacksAPI {

	public void setApi(HCGameAPI hcGame);

	public HCTeam newHCTeam(String name, ChatColor color, Location spawnpoint);

	public HCPlayer newHCPlayer(Player player);

	public HCPlayer newHCPlayer(HCPlayer hcPlayer);
	
	public List<HCTeam> createTeams();

	public List<Stuff> createStuffs();
	
	public void beforeLoad();
	
	public WorldConfig configureWorld(WorldManager worldManager);

	public List<String> updateWaitingScoreboard(HCPlayer hcPlayer);
	
	public List<String> updatePlayingScoreboard(HCPlayer hcPlayer);

	public List<String> updateEndedScoreboard(HCPlayer hcPlayer);

	public void timeBeforeStartDecreased(int time);

	public void waitForPlayersMessages();
	
	public void startGameMessages();

	public void playGameMessages();
	
	public void remainingTimeBeforeEnd(int remainingTime, int timeElapsed);
	
	public void endGameMessages(HCTeam winningTeam);

	public void waitPlayerAtLobby(HCPlayer hcPlayer);

	public void relogPlayingPlayer(HCPlayer hcPlayer);

	public void spectatePlayer(HCPlayer hcPlayer);

	public void vanishedPlayer(HCPlayer hcPlayer);

	public void eliminatedPlayer(HCPlayer hcPlayer);

	public void startPlayer(HCPlayer hcPlayer);

	public void respawnPlayer(HCPlayer hcPlayer);

	public void revivePlayer(HCPlayer hcPlayer);

	public boolean isAllowedToPerformSpecCommand(HCPlayer hcPlayer);

	public List<HCPlayer> getRandomizedPlayersAndAssignedToTeams(List<HCPlayer> players);

	public void assignStuffToPlayer(HCPlayer hcPlayer);

	public HCTeam getAssignableRandomTeam();

	public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcPlayer);

	public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller);

	public double getKillReward(HCPlayer player);

	public double getWinReward(HCPlayer player);

	public void moneyRewardedTo(HCPlayer hcKiller, double amount);
	
	public double getExtraHardcoinsMultiplier(HCPlayer player);
	
	public void formatDeathMessage(HCPlayer hcKilled, HCPlayer hcKiller, PlayerDeathEvent event);
	
	public void formatDeathMessage(HCPlayer hcKilled, PlayerDeathEvent event);

	public void formatChatMessage(AsyncPlayerChatEvent event, HCPlayer hcPlayer);

	public void printEndMessage(HCPlayer hcPlayer, HCTeam winningTeam);

	public void printDeathMessage(HCPlayer hcPlayer);

	public void rebootServer();

	public void alertGameIsFull(HCPlayer hcPlayer);

	public int getMaxPlayers();

	public int getMinPlayers();

	public Location getFirstRespawnLocation(HCPlayer hcPlayer);

	public Location getNextRespawnLocation(HCPlayer hcPlayer);

	public ItemBuilder getSpecInvPlayerItem(HCPlayer hcPlayer);

	public String getChooseTeamInventoryName();
	
	public Inventory getChooseTeamInventory();

	public Inventory getWaitingInventory();

	public HCTeam getForcedOnlineWinningTeam(List<HCTeam> onlineTeams);

	public void joinTeamHintOnJoin(HCPlayer hcPlayer);
	
	public void performPlayCommand(HCPlayer hcPlayer);
	
	 
}
