package com.gmail.val59000mc.hcgameslib.api.impl;

import java.util.List;

import org.bukkit.Location;

import com.gmail.val59000mc.hcgameslib.api.HCSoundAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Sounds;

public class Sound implements HCSoundAPI{

	private HCGame game;
	
	public Sound(HCGame game) {
		this.game = game;
	}

	@Override
	public void play(org.bukkit.Sound sound, float volume, float pitch){
		for(HCPlayer hcPlayer : game.getPlayersManagerAPI().getPlayers(true,null)){
			Sounds.play(hcPlayer.getPlayer(), sound, volume, pitch);
		}
	}

	@Override
	public void play(org.bukkit.Sound sound, Location location, float volume, float pitch) {
		for(HCPlayer hcPlayer : game.getPlayersManagerAPI().getPlayers(true,null)){
			Sounds.play(hcPlayer.getPlayer(), location, sound, volume, pitch);
		}
	}

	@Override
	public void play(HCPlayer hcPlayer, org.bukkit.Sound sound, float volume, float pitch) {
		if(hcPlayer.isOnline()){
			Sounds.play(hcPlayer.getPlayer(), sound, volume, pitch);
		}
	}

	@Override
	public void play(HCPlayer hcPlayer, org.bukkit.Sound sound, Location location, float volume, float pitch) {
		if(hcPlayer.isOnline()){
			Sounds.play(hcPlayer.getPlayer(), location, sound, volume, pitch);
		}
	}

	@Override
	public void play(HCTeam team, org.bukkit.Sound sound, float volume, float pitch) {
		for(HCPlayer hcPlayer : team.getMembers()){
			if(hcPlayer.isOnline()){
				Sounds.play(hcPlayer.getPlayer(), sound, volume, pitch);
			}
		}
	}

	@Override
	public void play(HCTeam team, org.bukkit.Sound sound, Location location, float volume, float pitch) {
		for(HCPlayer hcPlayer : team.getMembers()){
			if(hcPlayer.isOnline()){
				Sounds.play(hcPlayer.getPlayer(), location, sound, volume, pitch);
			}
		}
	}

	@Override
	public void play(List<HCPlayer> players, org.bukkit.Sound sound, float volume, float pitch) {
		for(HCPlayer hcPlayer : players){
			if(hcPlayer.isOnline()){
				Sounds.play(hcPlayer.getPlayer(), sound, volume, pitch);
			}
		}
	}

	@Override
	public void play(List<HCPlayer> players, org.bukkit.Sound sound, Location location, float volume, float pitch) {
		for(HCPlayer hcPlayer : players){
			if(hcPlayer.isOnline()){
				Sounds.play(hcPlayer.getPlayer(), location, sound, volume, pitch);
			}
		}
	}

}
