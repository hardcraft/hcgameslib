package com.gmail.val59000mc.hcgameslib.api.impl;

import java.util.List;

import org.bukkit.command.CommandSender;

import com.gmail.val59000mc.hcgameslib.api.HCChatAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;

public class Chat implements HCChatAPI{

	private String prefix;
	private HCGame game;
	
	public Chat(HCGame game, String prefix) {
		this.prefix = prefix;
		this.game = game;
	}
	
	@Override
	public void sendChat(HCMessage msg) {
		for(HCPlayer hcPlayer : game.getPlayersManagerAPI().getPlayers(true,null)){
			hcPlayer.getPlayer().sendMessage(msg.toString());
		}
	}

	@Override
	public void sendChat(HCPlayer player, HCMessage msg) {
		if(player.isOnline()){
			player.getPlayer().sendMessage(msg.toString());
		}
	}

	@Override
	public void sendChat(CommandSender sender, HCMessage msg) {
		sender.sendMessage(msg.toString());
	}

	@Override
	public void sendChatP(CommandSender sender, HCMessage msg) {
		sendChat(sender, msg.prepend(prefix));
	}

	@Override
	public void sendChat(HCTeam team, HCMessage msg) {
		for(HCPlayer player : team.getMembers()){
			sendChat(player, msg);
		}
	}

	@Override
	public void sendChat(List<HCPlayer> players, HCMessage msg) {
		for(HCPlayer player : players){
			sendChat(player, msg);
		}
	}

	@Override
	public void sendChatP(HCMessage msg) {
		sendChat(msg.prepend(prefix));
	}

	@Override
	public void sendChatP(HCPlayer player, HCMessage msg) {
		sendChat(player, msg.prepend(prefix));
	}

	@Override
	public void sendChatP(HCTeam team, HCMessage msg) {
		sendChat(team, msg.prepend(prefix));
	}

	@Override
	public void sendChatP(List<HCPlayer> players, HCMessage msg) {
		sendChat(players, msg.prepend(prefix));
	}



}
