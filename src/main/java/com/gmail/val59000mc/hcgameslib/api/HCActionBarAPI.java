package com.gmail.val59000mc.hcgameslib.api;

import java.util.List;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;

public interface HCActionBarAPI {
	/**
	 * Send an action bar to all players
	 * @param message
	 */
	public void sendActionBar(HCMessage msg);
	
	/**
	 * Send an action bar to a specific player
	 * @param player
	 * @param message
	 */
	public void sendActionBar(HCPlayer player , HCMessage msg);
	
	/**
	 * Send an action bar to a team
	 * @param team
	 * @param message
	 */
	public void sendActionBar(HCTeam team , HCMessage msg);
	
	/**
	 * Send an action bar to a list of players
	 * @param players
	 * @param message
	 */
	public void sendActionBar(List<HCPlayer> players , HCMessage msg);
	
	
	/**
	 * Send an action bar to all players for the specified duration
	 * @param message
	 * @param seconds
	 */
	public void sendActionBar(HCMessage msg, int seconds);
	
	/**
	 * Send an action bar to a specific player for the specified duration
	 * @param player
	 * @param message
	 * @param seconds
	 */
	public void sendActionBar(HCPlayer player , HCMessage msg, int seconds);
	
	/**
	 * Send an action bar to a team for the specified duration
	 * @param team
	 * @param message
	 * @param seconds
	 */
	public void sendActionBar(HCTeam team , HCMessage msg, int seconds);
	
	/**
	 * Send an action bar to a list of players for the specified duration
	 * @param players
	 * @param message
	 * @param seconds
	 */
	public void sendActionBar(List<HCPlayer> players , HCMessage msg, int seconds);
	

}
