package com.gmail.val59000mc.hcgameslib.api;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;

public interface HCPlayersManagerAPI {

	/**
	 * Add a team
	 * @param name
	 * @param color
	 * @param spawnpoint (may be null)
	 * @return
	 */
	public HCTeam addTeam(String name, ChatColor color, Location spawnpoint);
	

	/**
	 * Add all teams from the main config file
	 * The config must follow this syntax
	 * spawnpoint is optional
	 * 
	 * <pre>
	 * teams:
	 *   '1':
	 *     name: teams.red
	 *     color: RED
	 *     spawnpoint: -50 60 120 0 0
	 *   '2':
	 *     name: teams.blue
	 *     color: BLUE
	 *     spawnpoint: -20 60 120 2 3
	 * </pre>
	 */
	public List<HCTeam> getTeamsFromConfig();

	/**
	 * Remove a player from its current team if it exists
	 * @param hcPlayer
	 */
	public void removePlayerFromTeam(HCPlayer hcPlayer);
	
	/**
	 * Remove a player from its current team if it exists
	 * Doesn't trigger leave team event call
	 * @param hcPlayer
	 */
	public void removePlayerFromTeamSilently(HCPlayer hcPlayer);
	
	/**
	 * Add a player to the specified team after removing him from his current team if it exists
	 * @param hcPlayer
	 * @param hcTeam
	 */
	public void addPlayerToTeam(HCPlayer hcPlayer, HCTeam hcTeam);


	/**
	 * Check if player is allowed to join a team based on the number of online 
	 * player and the max allowed players. The calculation won't allow
	 * joining if there are too many players in the team compared
	 * to the other teams.
	 * @param hcPlayer
	 * @param hcTeam
	 * @return
	 */
	boolean canJoinTeam(HCPlayer hcPlayer, HCTeam hcTeam);

	/**
	 * Make a player join the game
	 * @param player
	 */
	public void playerJoinsTheGame(Player player);

	/**
	 * Get HCPlayer from Bukkit player
	 * @param player
	 * @return HCPlayer
	 */
	public HCPlayer getHCPlayer(Player player);

	/**
	 * Get HCPlayer from Bukkit entity
	 * If entity is not a player, null will be returned
	 * @param entity
	 * @return HCPlayer or null if not a player or hcPlayer not found
	 */
	public HCPlayer getHCPlayer(Entity entity);
	
	/**
	 * Get HCPlayer from player name
	 * @param player
	 * @return HCPlayer
	 */
	@Deprecated
	public HCPlayer getHCPlayer(String name);


	/**
	 * Get HCPlayer by uuid
	 * @param uuid
	 * @return
	 */
	public HCPlayer getHCPlayer(UUID uuid);
	
	/**
	 * Get HCPlayer by hcPlayer
	 * @param uuid
	 * @return
	 */
	public HCPlayer getHCPlayer(HCPlayer hcPlayer);

	/**
	 * Get a team by its name
	 * @param name
	 * @return
	 */
	public HCTeam getHCTeam(String name);
	

	/**
	 * Get winning team, null while not set
	 * may also be null when game has ended if there is no winner
	 * @return
	 */
	public HCTeam getWinningTeam();
	
	
	/**
	 * Get players in the game
	 * Include offline players
	 * @return players
	 */
	public List<HCPlayer> getPlayers();
	

	/**
	 * Gets players in a specific state
	 * @param online (may be null to get all online and offline)
	 * @param state (may be null to get all state)
	 * @return
	 */
	public List<HCPlayer> getPlayers(Boolean online, PlayerState state);
	

	
	/**
	 * Gets players in a specific state
	 * @param online (may be null to get all online and offline)
	 * @param states (may be empty or null to get all state)
	 * @return
	 */
	public List<HCPlayer> getPlayersMultiplesStates(Boolean online, Set<PlayerState> states);
	
	/**
	 * Get teams in the game
	 * @return teams
	 */
	public List<HCTeam> getTeams();
	

	/**
	 * Finds if a team has won
	 * @param pmApi
	 * @return
	 */
	public boolean hasWon(HCTeam team);
	
	/**
	 * Get current player count
	 * Includes offline players and in all player states
	 * @return
	 */
	public int getPlayersCount();
	

	/**
	 * Get a specific player count
	 * @param online (may be null to get all online and offline)
	 * @param state  (may be null to get all state)
	 * @return
	 */
	int getPlayersCount(Boolean online, PlayerState state);

	/**
	 * Get the max number of players allowed to play the game
	 * @return
	 */
	public int getMaxPlayers();
	

	/**
	 * Get min players to start the game
	 * @return
	 */
	public int getMinPlayers();

	/**
	 * Refresh the base hardcoins earnings of a player according to his rank
	 * 
	 */
	public void refreshRankHardcoinsMultiplier(HCPlayer hcPlayer);
	
	/**
	 * Refresh the nametag of a player after 20 ticks
	 * @param hcPlayer
	 */
	public void refreshRankNameTag(HCPlayer hcPlayer);

	/**
	 * Refresh the nametag of a player after
	 * the specified delay in ticks
	 * @param hcPlayer
	 */
	public void refreshRankNameTag(HCPlayer hcPlayer, long delay);

	/**
	 * Refresh the nametag of all players
	 * @param hcPlayer
	 */
	public void refreshRankNameTags(long delay);

	/**
	 * Refresh the visibility of a player with others players
	 * Useful to fix a visibility issue ocurring on Spigot 1.8
	 * @param hcPlayer
	 */
	public void refreshVisiblePlayers(HCPlayer hcPlayer);



	/**
	 * Update All players waiting or playing scoreboard
	 */
	public void updatePlayersScoreboards();


	/**
	 * Auto respawn a dead player after 20 ticks
	 * @param entity
	 */
	public void autoRespawnPlayerAfter20Ticks(Player entity);

	/**
	 * Reward money to a player without any rank or extra bonus
	 * @param hcPlayer
	 * @param amount
	 * @return the amount actually rewarded without bonus calculation
	 */
	public double rewardMoneyNoBonusTo(HCPlayer hcPlayer, double amount);
	
	/**
	 * Reward money to a player
	 * @param hcPlayer
	 * @param amount
	 * @return the amount actually rewarded with bonus calculation
	 */
	public double rewardMoneyTo(HCPlayer hcPlayer, double amount);


	/**
	 * Mark player as eliminated
	 * Makes him in PlayerState DEAD or SPECTATING depending if he was playing before
	 * @param hcPlayer
	 */
	public void eliminatePlayer(HCPlayer hcPlayer);


	/**
	 * Start the starting task with the default delay from the config file
	 */
	public void startStartingTask();
	
	/**
	 * Start the starting task with the specified delay
	 * @param delay a positive value
	 */
	public void startStartingTask(int delay);


	/**
	 * Force stopping the starting task
	 * If scheduled
	 */
	public void stopStartingTask();


	/**
	 * Gets if the starting task is running
	 * @return
	 */
	public boolean isStartingTaskScheduled();
	
	/**
	 * Check if joining team while playing is allowed
	 * @return
	 */
	public boolean isJoinTeamWhilePlayingAllowed();


	/**
	 * Force checking if there is no team or only one team online
	 * Will end the game if it detects such a state
	 */
	public void checkIfLastTeamOnline();

	/**
	 * Force declare a winner
	 * Only called by implemented plugin or by the lib itself if countdown is enabled
	 */
	public void forceDeclareWinner();
	
	/**
	 * Gets the first team found with the less number of members
	 * @return
	 */
	public HCTeam getMinMembersTeam();


	/**
	 * Respawn a player that was in DEAD state
	 * @param gladiatorPlayer
	 */
	public void revivePlayer(HCPlayer hcPlayer);
	
	/**
	 * Get last damager of this hcPlayer if it was an hcPlayer shooting an arrow
	 * @param event
	 * @return
	 */
	HCPlayer getLastDamagingPlayerArrowShooter(HCPlayer hcPlayer);
	
	/**
	 * Get last damager of this hcPlayer if it was an entity shooting an arrow
	 * @param event
	 * @return
	 */
	Entity getLastDamagingEntityArrowShooter(HCPlayer hcPlayer);

	/**
	 * Get last damager of this hcPlayer if it was an tntPrimed
	 * @param event
	 * @return
	 */
	TNTPrimed getLastDamagingTnt(HCPlayer hcPlayer);

	/**
	 * Get last damager of this hcPlayer if it was a fireball
	 * @param event
	 * @return
	 */
	Fireball getLastDamagingFireball(HCPlayer hcPlayer);

	/**
	 * Get last entity damager of this hcPlayer
	 * @param event
	 * @return
	 */
	Entity getLastDamagingEntity(HCPlayer hcPlayer);




	
}
