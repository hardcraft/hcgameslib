package com.gmail.val59000mc.hcgameslib.api;

public interface HCFlowsAPI {

	public String getNextFlowServer();
	
	public  boolean isEnabled();

	public String randomizeNextGame();

	public void executeFlowCopyFilesScript();
	
}
