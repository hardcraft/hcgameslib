package com.gmail.val59000mc.hcgameslib.api.impl.items;

import org.bukkit.inventory.ItemStack;

@FunctionalInterface
public interface ItemBuilderModifier {
	
	void modify(ItemStoreBuilder builder, ItemStack item);
	
}
