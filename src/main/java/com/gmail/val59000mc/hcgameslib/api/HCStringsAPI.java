package com.gmail.val59000mc.hcgameslib.api;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.api.impl.HCMessageList;

public interface HCStringsAPI {

	
	/**
	 * Get a message from the lang file without color
	 */
	public HCMessage getNoColor(String code);
	
	/**
	 * Get a message from the lang file with colors
	 */
	public HCMessage get(String code);
	
	/**
	 * Get a message from the lang file with colors
	 */
	public HCMessageList getList(String code);
	

	/**
	 * Get a message from the lang file without color
	 * Provide a default string if not found
	 */
	public HCMessage getNoColor(String code, String defaultString);
	
	/**
	 * Get a message from the lang file with colors
	 * Provide a default string if not found
	 */
	public HCMessage get(String code, String defaultString);


	/**
	 * Check if an entry exists
	 * @param string
	 * @return
	 */
	public boolean exists(String string);

	/**
	 * Get a message from a raw text
	 */
	public HCMessage getRaw(String content);
	
	
	public HCActionBarAPI getActionBarAPI();
	public HCBossBarAPI getBossBarAPI();
	public HCChatAPI getChatAPI();
	public HCTitleAPI getTitleAPI();

}
