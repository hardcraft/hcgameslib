package com.gmail.val59000mc.hcgameslib.api;

import java.util.List;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;

public interface HCBungeeAPI {
	
		/**
		 * Send all hc players to a server
		 * @param server
		 */
		public void sendToServer(String server);
		
		
		/**
		 * Send a specific hc player to a server
		 * @param player
		 * @param server
		 */
		public void sendToServer(HCPlayer player, String server);
		
		/**
		 * Send a list of hc players to a server
		 * @param players
		 * @param server
		 */
		public void sendToServer(List<HCPlayer> players, String server);
		
		/**
		 * Send a hc team to a server
		 * @param team
		 * @param server
		 */
		public void sendToServer(HCTeam team, String server);


		/**
		 * Send all online players to a server
		 */
		void sendAllToServer(String server);
}
