package com.gmail.val59000mc.hcgameslib.api;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;

import java.util.List;

public interface HCBossBarAPI {

    BossBar createBossBar(BarColor barColor, BarStyle barStyle, BarFlag... flags);

    void updateBossBar(BossBar bossBar, BarColor barColor);

    void updateBossBar(BossBar bossBar, BarStyle barStyle);

    void updateBossBar(BossBar bossBar, double progress);

    void deleteBossBar(BossBar bossBar);

    /**
     * Send a boss bar to all players
     *
     * @param message
     */
    void sendBossBar(BossBar bossBar, HCMessage msg);

    /**
     * Send a boss bar to a specific player
     *
     * @param player
     * @param message
     */
    void sendBossBar(BossBar bossBar, HCPlayer player, HCMessage msg);

    /**
     * Send a boss bar to a team
     *
     * @param team
     * @param message
     */
    void sendBossBar(BossBar bossBar, HCTeam team, HCMessage msg);

    /**
     * Send a boss bar to a list of players
     *
     * @param players
     * @param message
     */
    void sendBossBar(BossBar bossBar, List<HCPlayer> players, HCMessage msg);

}
