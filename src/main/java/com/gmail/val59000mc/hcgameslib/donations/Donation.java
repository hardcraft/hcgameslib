package com.gmail.val59000mc.hcgameslib.donations;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.time.ZonedDateTime;
import java.util.UUID;

public class Donation {

    private int id;
    private String playerName;
    private UUID playerUUID;
    private int playerId;
    private ZonedDateTime donationDate;

    public Donation(int id, String playerName, UUID playerUUID, int playerId, ZonedDateTime donationDate) {
        this.id = id;
        this.playerName = playerName;
        this.playerUUID = playerUUID;
        this.playerId = playerId;
        this.donationDate = donationDate;
    }

    public int getId() {
        return id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public UUID getPlayerUUID() {
        return playerUUID;
    }

    public int getPlayerId() {
        return playerId;
    }

    public ZonedDateTime getDonationDate() {
        return donationDate;
    }

    @Override
    public String toString(){
        return MoreObjects.toStringHelper(getClass())
            .add("id", id)
            .add("playerName", playerName)
            .add("playerUUID", playerUUID)
            .add("playerId", playerId)
            .add("donationDate", donationDate)
            .toString();
    }
}
