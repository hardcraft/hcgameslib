package com.gmail.val59000mc.hcgameslib.donations;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;

public class DonationResponse {

	private boolean success;
	private HCMessage message;
	private Donation donation;
	
	public DonationResponse(boolean success, HCMessage message, Donation donation){
		this.success = success;
		this.message = message;
		this.donation = donation;
	}

	public boolean isSuccess() {
		return success;
	}

	public HCMessage getMessage() {
		return message;
	}

	public Donation getDonation() {
		return donation;
	}
	
	
}
