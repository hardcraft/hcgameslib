package com.gmail.val59000mc.hcgameslib.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDeathEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledByPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;

public class HCPlayerDeathListener extends HCListener {
	
	private boolean friendlyFire;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		friendlyFire = getApi().getConfig().getBoolean("config.friendly-fire",Constants.DEFAULT_ENABLE_FRIENDLY_FIRE);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(final PlayerDeathEvent event) {

		HCPlayer hcPlayer = getPmApi().getHCPlayer(event.getEntity());
		
		if(hcPlayer != null && hcPlayer.isOnline()){
			handleDeathPlayer(event, hcPlayer);
		}
		
	}
	
	public void handleDeathPlayer(final PlayerDeathEvent event, HCPlayer hcKilled){
		
		if(getApi().is(GameState.PLAYING) || getApi().is(GameState.CONTINUOUS)){
			
			HCPlayer hcKiller = (hcKilled.getPlayer().getKiller() == null) ? null : getPmApi().getHCPlayer(hcKilled.getPlayer().getKiller());
			
			hcKilled.addDeath();
						
			if(hcKiller != null 
				&& hcKiller.isOnline() 
				&& !hcKiller.equals(hcKilled)
				&& !friendlyFire 
				&& !hcKiller.isInTeamWith(hcKilled)
				){
				
				hcKiller.addKill();
				getCallbacksApi().formatDeathMessage(hcKilled, hcKiller, event);
				getCallbacksApi().handleKillEvent(event,hcKilled, hcKiller);
				Bukkit.getPluginManager().callEvent(new HCPlayerKilledByPlayerEvent(getApi(), hcKilled, hcKiller));
			}else{
				getCallbacksApi().formatDeathMessage(hcKilled, event);
				getCallbacksApi().handleDeathEvent(event,hcKilled);
				Bukkit.getPluginManager().callEvent(new HCPlayerKilledEvent(getApi(), hcKilled));
			}

			Bukkit.getPluginManager().callEvent(new HCPlayerDeathEvent(getApi(), hcKilled));
			
			getPmApi().updatePlayersScoreboards();

		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		
		HCPlayer hcPlayer = getPmApi().getHCPlayer(event.getPlayer());
		
		if(hcPlayer != null)
			handleRespawnPlayer(event,hcPlayer);
		
		
	}
	
	public void handleRespawnPlayer(PlayerRespawnEvent event, HCPlayer hcPlayer){
		
		Log.debug("handleRespawnPlayer player="+hcPlayer.getName());

		hcPlayer.setSpawnPoint(getCallbacksApi().getNextRespawnLocation(hcPlayer));
		
		switch(getApi().getGameState()){
			case WAITING:
					event.setRespawnLocation(getApi().getWorldConfig().getLobby());
					Bukkit.getScheduler().runTaskLater(getApi().getPlugin(), new Runnable() {
					
					public void run() {
						getCallbacksApi().waitPlayerAtLobby(hcPlayer);
					}
				}, 1);
				break;
			case LOADING:
			case STARTING:
			case ENDED:
			default:
			case PLAYING:
			case CONTINUOUS:
				if(hcPlayer.getSpawnPoint() == null){
					event.setRespawnLocation(getApi().getWorldConfig().getCenter());
				}else{
					event.setRespawnLocation(hcPlayer.getSpawnPoint());
				}
				
				Bukkit.getScheduler().runTaskLater(getApi().getPlugin(), new Runnable() {
					
					public void run(){
						if(hcPlayer.is(PlayerState.PLAYING)){
							((PlayersManager) getPmApi()).respawnPlayer(hcPlayer);
						}else if(hcPlayer.is(PlayerState.VANISHED)){
							((PlayersManager) getPmApi()).vanishPlayer(hcPlayer);
						}else{
							((PlayersManager) getPmApi()).spectatePlayer(hcPlayer);
						}
						
					}
				}, 1);
				
				break;
		}
		
	}
	
}
