package com.gmail.val59000mc.hcgameslib.listeners;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCBungeeAPI;
import com.gmail.val59000mc.hcgameslib.api.HCDependenciesAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCItemsAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.api.HCPlayersManagerAPI;
import com.gmail.val59000mc.hcgameslib.api.HCPluginCallbacksAPI;
import com.gmail.val59000mc.hcgameslib.api.HCSoundAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;

public abstract class HCListener implements Listener{
	
	private HCGameAPI api;
	
	protected HCGameAPI getApi(){
		return api;
	}

	public void setApi(HCGameAPI api) {
		this.api = api;
	}

	protected HCPlayersManagerAPI getPmApi(){
		return api.getPlayersManagerAPI();
	}

	protected FileConfiguration getConfig(){
		return api.getConfig();
	}

	protected HCStringsAPI getStringsApi(){
		return api.getStringsAPI();
	}

	protected HCBungeeAPI getBungeeApi(){
		return api.getBungeeAPI();
	}

	protected HCPluginCallbacksAPI getCallbacksApi(){
		return api.getCallbacksApi();
	}

	protected HCSoundAPI getSoundApi(){
		return api.getSoundAPI();
	}

	protected HCItemsAPI getItemsApi(){
		return api.getItemsAPI();
	}

	protected HCDependenciesAPI getDependenciesApi(){
		return api.getDependencies();
	}

	protected HCMySQLAPI getMySQLAPI(){
		return api.getMySQLAPI();
	}

	protected JavaPlugin getPlugin(){
		return api.getPlugin();
	}
	
}
