package com.gmail.val59000mc.hcgameslib.listeners;

import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.events.*;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import net.minecraft.server.v1_14_R1.EntityPlayer;
import net.minecraft.server.v1_14_R1.EntityTNTPrimed;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftTNTPrimed;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.lang.reflect.Field;
import java.util.Collection;

public class HCPlayerDamageListener extends HCListener {

    private boolean friendlyFire;
    private boolean disableFireworkDamage;

    @EventHandler
    public void afterLoad(HCAfterLoadEvent e) {
        FileConfiguration config = getApi().getConfig();
        friendlyFire = config.getBoolean("config.friendly-fire", Constants.DEFAULT_ENABLE_FRIENDLY_FIRE);
        disableFireworkDamage = config.getBoolean("config.disable-firework-damage", Constants.DEFAULT_DISABLE_FIREWORK_DAMAGE);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDamage(EntityDamageByEntityEvent event) {
        handleHit(event);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDamage(EntityDamageEvent event) {
        cancelDamageIfWaiting(event);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPotionSplash(PotionSplashEvent event) {
        handlePotionSplash(event);
    }

    private void handlePotionSplash(PotionSplashEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {

            Player damager = (Player) event.getEntity().getShooter();
            HCPlayer hcDamager = getPmApi().getHCPlayer(damager);

            if (hcDamager != null) {

                // Cancelling bad potion damage for teamates
                if (isAttackPotion(event.getPotion())) {
                    for (LivingEntity living : event.getAffectedEntities()) {
                        if (living instanceof Player) {
                            Player damaged = (Player) living;
                            HCPlayer hcDamaged = getPmApi().getHCPlayer(damaged);

                            HCPlayerDamagedByPotionEvent eventWrapper = new HCPlayerDamagedByPotionEvent(getApi(), hcDamaged, hcDamager, event);

                            if (!friendlyFire && (hcDamager.equals(hcDamaged) || hcDamager.isInTeamWith(hcDamaged)))
                                event.setIntensity(living, 0);

                            getApi().callEvent(eventWrapper);

                        }
                    }
                }

            }
        }

    }

    // Only checking the first potion effect, considering vanilla potions
    private boolean isAttackPotion(ThrownPotion potion) {
        Collection<PotionEffect> effects = potion.getEffects();
        if (effects.size() > 0) {
            PotionEffectType effect = effects.iterator().next().getType();
            return (
                effect.equals(PotionEffectType.HARM) ||
                    effect.equals(PotionEffectType.POISON) ||
                    effect.equals(PotionEffectType.WEAKNESS) ||
                    effect.equals(PotionEffectType.SLOW) ||
                    effect.equals(PotionEffectType.SLOW_DIGGING) ||
                    effect.equals(PotionEffectType.CONFUSION) ||
                    effect.equals(PotionEffectType.BLINDNESS) ||
                    effect.equals(PotionEffectType.HUNGER) ||
                    effect.equals(PotionEffectType.WITHER)
            );
        }
        return false;
    }


    private void cancelDamageIfWaiting(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) event.getEntity());
            if (hcPlayer != null) {

                HCPlayerDamagedEvent eventWrapper = new HCPlayerDamagedEvent(getApi(), hcPlayer, event);

                if (hcPlayer.getState().equals(PlayerState.WAITING))
                    event.setCancelled(true);

                getApi().callEvent(eventWrapper);
            }
        }
    }

    private void handleHit(EntityDamageByEntityEvent event) {

        // Hit by player
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {

            Player damager = (Player) event.getDamager();
            Player damaged = (Player) event.getEntity();

            HCPlayer hcDamager = getPmApi().getHCPlayer(damager);
            HCPlayer hcDamaged = getPmApi().getHCPlayer(damaged);

            if (hcDamaged != null && hcDamager != null) {
                HCPlayerDamagedByHCPlayerEvent eventWrapper = new HCPlayerDamagedByHCPlayerEvent(getApi(), hcDamaged, hcDamager, event);
                if (!friendlyFire && (hcDamager.equals(hcDamaged) || hcDamager.isInTeamWith(hcDamaged)))
                    event.setCancelled(true);

                getApi().callEvent(eventWrapper);
            }
        }

        // Hit by projectile
        else if (event.getEntity() instanceof Player && event.getDamager() instanceof Projectile) {
            Projectile projectile = (Projectile) event.getDamager();
            final Player shot = (Player) event.getEntity();

            if (projectile.getShooter() instanceof Player) {

                final Player shooter = (Player) projectile.getShooter();
                HCPlayer hcDamager = getPmApi().getHCPlayer(shooter);
                HCPlayer hcDamaged = getPmApi().getHCPlayer(shot);

                if (hcDamager != null && hcDamaged != null) {
                    HCPlayerDamagedByHCPlayerEvent eventWrapper = new HCPlayerDamagedByHCPlayerEvent(getApi(), hcDamaged, hcDamager, event);
                    if (!friendlyFire && (hcDamager.equals(hcDamaged) || hcDamager.isInTeamWith(hcDamaged)))
                        event.setCancelled(true);

                    getApi().callEvent(eventWrapper);
                }
            } else if (projectile.getShooter() instanceof Entity) {
                HCPlayer hcDamaged = getPmApi().getHCPlayer(shot);
                HCPlayerDamagedByNonPlayerEntityEvent eventWrapper = new HCPlayerDamagedByNonPlayerEntityEvent(getApi(), hcDamaged, (Entity) projectile.getShooter(), event);
                getApi().callEvent(eventWrapper);
            }
        }

        // Hit by TNT
        else if (event.getEntity() instanceof Player && event.getDamager() instanceof TNTPrimed) {

            HCPlayer hcDamaged = getPmApi().getHCPlayer((Player) event.getEntity());

            if (hcDamaged != null) {

                TNTPrimed tnt = (TNTPrimed) event.getDamager();
                EntityTNTPrimed nmsTNT = (((CraftTNTPrimed) tnt).getHandle());

                try {
                    Field source = EntityTNTPrimed.class.getDeclaredField("source");
                    source.setAccessible(true);
                    if (source.get(nmsTNT) != null && source.get(nmsTNT) instanceof EntityPlayer) {
                        EntityPlayer entityPlayer = (EntityPlayer) source.get(nmsTNT);
                        HCPlayer hcDamager = getPmApi().getHCPlayer(entityPlayer.getUniqueID());
                        if (hcDamager != null) {
                            HCPlayerDamagedByHCPlayerEvent eventWrapper = new HCPlayerDamagedByHCPlayerEvent(getApi(), hcDamaged, hcDamager, event);
                            if (!friendlyFire && (hcDamager.equals(hcDamaged) || hcDamager.isInTeamWith(hcDamaged)))
                                event.setCancelled(true);

                            getApi().callEvent(eventWrapper);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

        } else if (disableFireworkDamage
            && event.getEntity() instanceof Player
            && event.getDamager() instanceof Firework) {
            event.setCancelled(true);

            // damage by other
        } else if (event.getEntity() instanceof Player) {

            HCPlayer hcDamaged = getPmApi().getHCPlayer((Player) event.getEntity());

            if (hcDamaged != null) {
                getApi().callEvent(new HCPlayerDamagedByNonPlayerEntityEvent(getApi(), hcDamaged, event.getDamager(), event));
            }
        }

    }

}
