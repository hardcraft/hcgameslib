package com.gmail.val59000mc.hcgameslib.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCChatListener extends HCListener{
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onPlayerChat(AsyncPlayerChatEvent event){
		
		HCPlayer hcPlayer = getPmApi().getHCPlayer(event.getPlayer());
		if(hcPlayer != null && hcPlayer.isOnline()){
			getCallbacksApi().formatChatMessage(event, hcPlayer);
		}
		
	}
}
