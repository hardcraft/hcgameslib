package com.gmail.val59000mc.hcgameslib.listeners;

import java.sql.SQLException;

import javax.sql.rowset.CachedRowSet;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.HCGamesLib;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerPersistSessionEvent;
import com.gmail.val59000mc.hcgameslib.database.HCStatsMonthChangeEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerJoinEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;

public class HCMySQLListener extends HCListener{
	
	// Queries
	private String insertGameSQL;
	private String insertPlayerSQL;
	private String insertPlayerStatsSQL;
	private String updatePlayerNameAndGameSQL;
	private String updatePlayerStatsSQL;
	private String updatePlayerGlobalStatsSQL;
	
	private Integer gameId;
	
	/**
	 * Load sql queries from resources files
	 */
	public void readQueries() {
		HCMySQLAPI sql = getMySQLAPI();
		if(sql.isEnabled()){
			insertGameSQL = sql.readQueryFromResource(HCGamesLib.getPlugin(),"sql/insert_game.sql");
			insertPlayerSQL = sql.readQueryFromResource(HCGamesLib.getPlugin(),"sql/insert_player.sql");
			insertPlayerStatsSQL = sql.readQueryFromResource(HCGamesLib.getPlugin(),"sql/insert_player_stats.sql");
			updatePlayerNameAndGameSQL = sql.readQueryFromResource(HCGamesLib.getPlugin(), "sql/update_player_name_and_game.sql");
			updatePlayerStatsSQL = sql.readQueryFromResource(HCGamesLib.getPlugin(), "sql/update_player_stats.sql");
			updatePlayerGlobalStatsSQL = sql.readQueryFromResource(HCGamesLib.getPlugin(), "sql/update_player_global_stats.sql");
		}
	}
	
	/**
	 * Insert game if not exists
	 * @param e
	 */
	@EventHandler
	public void onGameFinishedLoading(HCAfterLoadEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		readQueries();
		
		if(sql.isEnabled()){
			
			// data
			String name = getApi().getMySQLGameName();
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try {
						sql.execute(sql.prepareStatement(insertGameSQL, name));
						CachedRowSet row = sql.selectGameByName(name);
						if(row.first()){
							gameId = row.getInt("id");
						}
					} catch (SQLException e) {
						Log.severe("Couldn't find game "+name+" in database");
						e.printStackTrace();
					}
					
				}
			});
		}
	}
	
	/**
	 * Add new player if not exists when joining
	 * Update name (because it may change)
	 * Update current played game 
	 * @param e
	 */
	@EventHandler
	public void onPlayerJoin(HCPlayerJoinEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		HCPlayer hcPlayer = e.getHcPlayer();
		
		if(sql.isEnabled()){
			
			// data
			String playerName = hcPlayer.getName();
			String uuid = hcPlayer.getUuid().toString();
			
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					if(hcPlayer.getId() == 0){
						
						int playerId = 0;

						try{
							
							// Select player by uuid
							CachedRowSet resPlayer = sql.selectPlayerByUUID(uuid);
							
							if(!resPlayer.first()){
								// Insert player if not exists
								sql.execute(sql.prepareStatement(insertPlayerSQL, playerName, uuid));
								
								// Re-execute select by uuid after player insert
								resPlayer = sql.selectPlayerByUUID(uuid);
								if(resPlayer.first()){
									playerId = resPlayer.getInt("id");
								}else{
									Log.warn("Couldn't find player id after insert in database uuid="+uuid+" name="+playerName);
								}
								
							}else{
								// Get id if exists
								playerId = resPlayer.getInt("id");
							}

						}catch(SQLException e) {
							
							// Log on error
							Log.severe("Couldn't initialize player in database uuid="+uuid+" name="+playerName);
							e.printStackTrace();
							
						}finally{
							
							// Asign database id to hcPlayer
							hcPlayer.setId(playerId);
							
						}
						
					}
					
					try{
						
						// Update player game and last known name
						sql.execute(sql.prepareStatement(updatePlayerNameAndGameSQL, playerName, String.valueOf(gameId), uuid));
					
					}catch(SQLException e) {
						
						// Log on error
						Log.severe("Couldn't update player name and game when joining game , uuid="+uuid+" name="+playerName);
						e.printStackTrace();
						
					}
					
					if(hcPlayer.getId() != 0){
						
						try{
							
							// Insert current month player stats if not exists
							sql.execute(sql.prepareStatement(insertPlayerStatsSQL, String.valueOf(hcPlayer.getId()), sql.getMonth(), sql.getYear()));
							
							// Forward player join sql event to game plugin implementation
							callEvent(new HCPlayerDBInsertedEvent(getApi(),	hcPlayer));
							
						}catch(SQLException e) {
							
							// Log on error
							Log.severe("Couldn't insert player monthg stats uuid="+uuid+" name="+playerName+" month="+sql.getMonth()+" year="+sql.getYear());
							e.printStackTrace();
							
						}
						
					}
					
					
					
				}
			});
		}
	}
	
	/**
	 * Remove player current game
	 * @param e
	 */
	@EventHandler
	public void onPlayerLeave(HCPlayerQuitEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		HCPlayer hcPlayer = e.getHcPlayer();
		
		if(sql.isEnabled()){
			
			// data
			String playerName = hcPlayer.getName();
			String uuid = hcPlayer.getUuid().toString();
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {
					
					try{
						// Update player game and last known name
						sql.execute(sql.prepareStatement(updatePlayerNameAndGameSQL, playerName, null, uuid));
					}catch(SQLException e){
						Log.severe("Couldn't update player game and name when leaving game , uuid="+uuid+" name="+playerName);
						e.printStackTrace();
					}
					
				}
			});
		}
	}
	
	@EventHandler
	public void onPlayerChangeState(HCPlayerStateChangeEvent e){
		// when online and passing from PLAYING to DEAD or VANISHED while the game is still PLAYING
		// usually with /spec or /vanish commands
		// but also in games where players gets eliminated and become specs
		if( getApi().is(GameState.PLAYING)
			&& e.getHcPlayer().isOnline()
		    && e.getOldState().equals(PlayerState.PLAYING) 
			&& (e.getNewState().equals(PlayerState.DEAD) || e.getNewState().equals(PlayerState.VANISHED))
			){
				((PlayersManager) getPmApi()).dispatchPersistPlayer(e.getHcPlayer());
		}
	}
	
	@EventHandler
	public void onPersistPlayer(HCPlayerPersistSessionEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			HCPlayer hcPlayerToPersist = e.getHcPlayer();

			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try {
							
						// Update player global stats
						//UPDATE player SET play_time=play_time+?, kills=kills+?, deaths=deaths+?, coins_earned=coins_earned+?, wins=wins+? WHERE uuid=?;
						sql.execute(sql.prepareStatement(updatePlayerGlobalStatsSQL, 
								String.valueOf(hcPlayerToPersist.getTimePlayed()),
								String.valueOf(hcPlayerToPersist.getKills()),
								String.valueOf(hcPlayerToPersist.getDeaths()),
								String.valueOf(hcPlayerToPersist.getMoney()),
								String.valueOf(hcPlayerToPersist.getWins()),
								String.valueOf(hcPlayerToPersist.getUuid())
						));
						
						if(hcPlayerToPersist.getId() == 0){
							
							CachedRowSet row = sql.selectPlayerByUUID(hcPlayerToPersist.getUuid().toString());
							try {
								if(row.first()){
									hcPlayerToPersist.setId(row.getInt("id"));
								}
							} catch (SQLException e) {
								Log.severe("Couldn't find player "+hcPlayerToPersist.getName()+" in database");
								throw e;
							}
							
						}
						
						if(hcPlayerToPersist.getId() != 0){
							// Update player month stats
							// UPDATE player_stats SET play_time=play_time+?, kills=kills+?, deaths=deaths+?, coins_earned=coins_earned+?, wins=wins+? WHERE id=? AND month=? AND year=?;
							sql.execute(sql.prepareStatement(updatePlayerStatsSQL, 
									String.valueOf(hcPlayerToPersist.getTimePlayed()),
									String.valueOf(hcPlayerToPersist.getKills()),
									String.valueOf(hcPlayerToPersist.getDeaths()),
									String.valueOf(hcPlayerToPersist.getMoney()),
									String.valueOf(hcPlayerToPersist.getWins()),
									String.valueOf(hcPlayerToPersist.getId()),
									sql.getMonth(),
									sql.getYear()
							));
							
							callEvent(new HCPlayerDBPersistedSessionEvent(getApi(),	hcPlayerToPersist));
								
							
						}
					

					}catch(SQLException e){
						Log.severe("Couldn't persist player session stats player="+hcPlayerToPersist.getName()+" uuid="+hcPlayerToPersist.getUuid()+" in database");
						e.printStackTrace();
					}
					
				}
					
			});
				
			
		}
	}
	
	@EventHandler
	public void onMonthChanged(HCStatsMonthChangeEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			for(HCPlayer hcPlayer : getPmApi().getPlayers()){
				
				Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
					@Override
					public void run() {

						try{
							
							if(hcPlayer.getId() == 0){
								
								CachedRowSet row = sql.selectPlayerByUUID(hcPlayer.getUuid().toString());
								try {
									if(row.first()){
										hcPlayer.setId(row.getInt("id"));
									}
								} catch (SQLException e) {
									Log.severe("Couldn't find player "+hcPlayer.getName()+" in database");
									throw e;
								}
							}
							

							if(hcPlayer.getId() != 0){
								// Insert current month player stats if not exists
								sql.execute(sql.prepareStatement(insertPlayerStatsSQL, String.valueOf(hcPlayer.getId()), sql.getMonth(), sql.getYear()));
								
								callEvent(new HCPlayerDBInsertedEvent(getApi(),	hcPlayer));
							}
						
						} catch(SQLException e){
							Log.severe("Couldn't insert new month player stats in database player="+hcPlayer.getName()+" uuid="+hcPlayer.getUuid()+" in database");
							e.printStackTrace();
						}
						
						
					}
				});
				
			}
			
			
		}
	}


	private void callEvent(HCEvent event) {
		Bukkit.getScheduler().runTask(getPlugin(),  new Runnable() {
			
			@Override
			public void run() {
				Bukkit.getPluginManager().callEvent(event);
			}
		});
	}
}
