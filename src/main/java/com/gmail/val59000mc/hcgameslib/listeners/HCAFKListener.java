package com.gmail.val59000mc.hcgameslib.listeners;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerMoveEvent;

import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCGameStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerJoinEvent;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;

public class HCAFKListener extends HCListener{

	private boolean isEnabled;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		isEnabled = getApi().getConfig().getBoolean("config.kick-if-afk.enable", Constants.DEFAULT_ENABLE_KICK_IF_AFK);
		if(isEnabled){
			long millisecondsBeforeKick = 1000 * getApi().getConfig().getLong("config.kick-if-afk.seconds-before-kick", Constants.DEFAULT_KICK_IF_AFK_SECONDS);
			AfkMoveListener afkMoveListener = new AfkMoveListener(millisecondsBeforeKick);
			getApi().registerListener(afkMoveListener);
			
			getApi().buildTask("kick afk players", new HCTask() {
				
				@Override
				public void run() {
					afkMoveListener.checkAfk();
				}
			})
			.withInterval(600) // check every 30s
			.build()
			.start();
		}
	}
	
	private class AfkMoveListener extends HCListener{

		private long millisBeforeKick;
		
		public AfkMoveListener(long millisecondsBeforeKick) {
			this.millisBeforeKick = millisecondsBeforeKick;
			
			
		}
		
		public void checkAfk() {

			long now = System.currentTimeMillis();
			for(HCPlayer hcPlayer : getPmApi().getPlayers(true, null)){
				long lastMovement = hcPlayer.getLastMovementt();
				Log.debug("check afk "+hcPlayer.getName()+" now="+now+" lastMovement="+lastMovement+" diff="+(now-lastMovement));
				if(lastMovement != 0 && Math.abs(now - lastMovement) > millisBeforeKick){
					if(!hcPlayer.getPlayer().hasPermission("hardcraftpvp.no-afk")){
						kickAfk(hcPlayer);
					}
				}
			}
			
			
		}

		@EventHandler(priority = EventPriority.MONITOR)
		public void onGameStateChange(HCGameStateChangeEvent e){
			for(HCPlayer hcPlayer : getPmApi().getPlayers(true, null)){
				hcPlayer.refreshLastMovement();
			}
		}
		
		@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
		public void onJoin(HCPlayerJoinEvent e){
			e.getHcPlayer().refreshLastMovement();
		}
		
		@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
		public void onMove(PlayerMoveEvent e){
			
			if(isRealMove(e.getFrom(), e.getTo())){
				HCPlayer hcPlayer = getPmApi().getHCPlayer(e.getPlayer());
				if(hcPlayer != null){
					hcPlayer.refreshLastMovement();
				}
			}
			
		}
		
		private boolean isRealMove(Location from, Location to){
			
			return Math.abs(from.getX() - to.getX()) > 0.1
				|| Math.abs(from.getZ() - to.getZ()) > 0.1;
		}
		
		private void kickAfk(HCPlayer hcPlayer){
			hcPlayer.getPlayer().kickPlayer(getStringsApi().get("messages.player-kicked-for-afk.self").toString());
		}
		
	}
	
	
}
