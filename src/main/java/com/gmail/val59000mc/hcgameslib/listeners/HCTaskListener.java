package com.gmail.val59000mc.hcgameslib.listeners;

import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;

public class HCTaskListener extends HCListener{
	
	private HCTaskScheduler taskScheduler;
	
	public HCTaskScheduler getScheduler(){
		return taskScheduler;
	}

	public void setTaskScheduler(HCTaskScheduler hcTaskScheduler) {
		this.taskScheduler = hcTaskScheduler;
	}
	
}
