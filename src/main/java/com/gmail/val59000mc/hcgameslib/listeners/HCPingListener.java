package com.gmail.val59000mc.hcgameslib.listeners;

import java.util.Iterator;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.server.ServerListPingEvent;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;

public class HCPingListener extends HCListener{
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPing(ServerListPingEvent event){
		
		switch(getApi().getGameState()){
			case INITIALIZING:
				event.setMotd(getStringsApi().get("messages.ping.initializing").toString());
				break;
			case LOADING:
				event.setMotd(getStringsApi().get("messages.ping.loading").toString());
				break;
			case WAITING:
				event.setMotd(getStringsApi().get("messages.ping.waiting").toString());
				break;
			case STARTING:
				event.setMotd(getStringsApi().get("messages.ping.starting").toString());
				break;
			case CONTINUOUS:
			case PLAYING:
				event.setMotd(getStringsApi().get("messages.ping.playing").toString());
				break;
			case ENDED:
				event.setMotd(getStringsApi().get("messages.ping.ended").toString());
				break;
			default:
				event.setMotd(getStringsApi().get("messages.ping.loading").toString());
				break;
		}
		
		Iterator<Player> it = event.iterator();
		while(it.hasNext()){
			HCPlayer hcPlayer = getPmApi().getHCPlayer(it.next());
			if(hcPlayer != null && hcPlayer.is(PlayerState.VANISHED)){
				it.remove();
			}
		}
	}
}
