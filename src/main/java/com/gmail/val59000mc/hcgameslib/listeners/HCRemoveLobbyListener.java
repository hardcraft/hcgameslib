package com.gmail.val59000mc.hcgameslib.listeners;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterStartEvent;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Parser;

public class HCRemoveLobbyListener extends HCListener {

	private boolean isEnabled;
	private int maxBlocksPerSecond;
	private int ticksInterval;
	private LocationBounds bounds;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		
		World world = getApi().getWorldConfig().getWorld();
		
		FileConfiguration cfg = getConfig();
		this.isEnabled = cfg.getBoolean("config.world.remove-lobby-after-start.enable", Constants.DEFAULT_REMOVE_LOBBY_AFTER_START);
		this.maxBlocksPerSecond = cfg.getInt("config.world.remove-lobby-after-start.max-blocks-per-second", Constants.DEFAULT_REMOVE_LOBBY_MAX_BLOCKS_PER_SECOND);
		this.ticksInterval = cfg.getInt("config.world.remove-lobby-after-start.ticks-interval", Constants.DEFAULT_REMOVE_LOBBY_TICKS_INTERVAL);
		
		Location min = Parser.parseLocation(world, cfg.getString("config.world.remove-lobby-after-start.min"));
		Location max = Parser.parseLocation(world, cfg.getString("config.world.remove-lobby-after-start.max"));
		
		this.bounds = new LocationBounds(min, max);
		
	}
	
	@EventHandler
	public void afterStart(HCAfterStartEvent e){
		
		if(!isEnabled)
			return;
		
		World world = getApi().getWorldConfig().getWorld();
		
		List<BlockState> lobbyBlocks = new ArrayList<>();
		
		// build block list to remove
		for(int i=bounds.getMin().getBlockX(); i<= bounds.getMax().getBlockX(); i++){
			for(int j=bounds.getMin().getBlockY(); j<= bounds.getMax().getBlockY(); j++){
				for(int k=bounds.getMin().getBlockZ(); k<= bounds.getMax().getBlockZ(); k++){
					Block block = world.getBlockAt(i, j, k);
					if(!block.getType().equals(Material.AIR)){
						lobbyBlocks.add(block.getState());
					}
				}
			}
		}
		
		// remove all non players entities withing lobby bounds
		for(Entity entity : world.getEntities()){
			if(bounds.contains(entity.getLocation()) && !entity.getType().equals(EntityType.PLAYER)){
				entity.remove();
			}
		}
		
		// remove with delayed task (100 by 100 blocks)
		final Iterator<BlockState> blockIterator = lobbyBlocks.iterator();
		
		int maxBlocksPerTicksInterval = (int) Math.round((double) ticksInterval * ((double) maxBlocksPerSecond / 20d));
		
		getApi().buildTask("remove lobby blocks", new HCTask() {
			
			private Iterator<BlockState> it = blockIterator;
			
			@Override
			public void run() {
				for(int i=0 ; i<maxBlocksPerTicksInterval ; i++){
					if(it.hasNext()){
						BlockState state = it.next();
						state.setType(Material.AIR);
						state.update(true);
					}
				}
				if(!it.hasNext()){
					stop();
				}
			}
		})
		.withInterval(ticksInterval)
		.build()
		.start();
		
	}
}
