package com.gmail.val59000mc.hcgameslib.listeners;

import java.time.ZoneId;

import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.api.HCBoostersAPI;
import com.gmail.val59000mc.hcgameslib.api.impl.Boosters;
import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerJoinEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;

public class HCBoosterListener extends HCListener{

	private HCBoostersAPI boosters;
	private static ZoneId PARIS = ZoneId.of("Europe/Paris");
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		boosters = getApi().getBoostersAPI();
		((Boosters) boosters).initialize();
	}
	
	@EventHandler
	public void onPlayerDBInserted(HCPlayerDBInsertedEvent e){
		getApi().async(()->boosters.loadActiveBooster(e.getHcPlayer()));
	}
	
	@EventHandler
	public void onPlayerJoin(HCPlayerJoinEvent e){
		// display current booster after 3 seconds
		getApi().sync(()->{
			HCMessage message = boosters.getBestBoosterMessage();
			message.sendActionBar(e.getHcPlayer(),2);
		}, 60);
	}
	
	@EventHandler
	public void onPlayerQuit(HCPlayerQuitEvent e){
		getApi().sync(()->boosters.unloadActiveBooster(e.getHcPlayer()),1);
	}
}
