package com.gmail.val59000mc.hcgameslib.listeners;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterInventoryEvent;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterPlaceholderEvent;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;
import com.gmail.val59000mc.simpleinventorygui.placeholders.SimplePlaceholder;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.google.common.collect.Lists;

public class HCSIGListener extends HCListener{

	@EventHandler
	public void onSigInventoryLoad(SIGRegisterInventoryEvent e){
		InventoryManager im = e.getIm();
		
		///////////////////////////////
		// Register spectator inventory
		///////////////////////////////
		Inventory specinv = new Inventory(Constants.DEFAULT_SPECTATOR_INV_NAME, "§aSpectateur", 3);
		Log.debug("Registering spectator inventory "+specinv.getName());
		
		Item specitem = new Item.ItemBuilder(Material.SKELETON_SKULL)
			.withPosition(13)
			.withDamage((short) 3)
			.withCooldown(1)
			.withName(getStringsApi().get("messages.spec.item-name").toString())
			.withLore(Lists.newArrayList(
					getStringsApi().get("messages.spec.item-lore-1").toString(),
					getStringsApi().get("messages.spec.item-lore-2").toString()
			))
			.withPlayerSkullName("{player}")
			.build();

		specitem.setActions(Lists.newArrayList(new SpecInventoryOpenAction()));
		
		specinv.addItem(specitem);

		im.addInventory(specinv);
		

		///////////////////////////////
		// Register waiting inventory
		///////////////////////////////
		im.addInventory(getCallbacksApi().getWaitingInventory());
		

		//////////////////////////////////
		// Register choose team inventory
		//////////////////////////////////
		im.addInventory(getCallbacksApi().getChooseTeamInventory());
		
	}
	
	/**
	 * Register team placeholders
	 * Usage: {members.team-name}
	 * @param e
	 */
	@EventHandler
	public void onRegisterPlaceholder(SIGRegisterPlaceholderEvent e){
		PlaceholderManager pm = e.getPm();
		for(HCTeam hcTeam : getPmApi().getTeams()){
			pm.registerNewPlaceholder(new TeamPlaceholder("{members."+hcTeam.getName()+"}", hcTeam));
		}
	}
	/**
	 * Action to execute when a spectator click on his player head to open
	 * the spectator inventory
	 * @author Valentin
	 *
	 */
	private class SpecInventoryOpenAction extends Action{

		@Override
		public void executeAction(Player player, SigPlayer sigPlayer) {

			InventoryManager im = InventoryManager.instance();
			String invName = Constants.DEFAULT_SPECTATOR_INV_NAME_PLAYER_PREFIX+player.getName();
			Inventory invToRemove = im.getInventoryByName(invName);
			if(invToRemove != null)
				im.removeInventory(invToRemove);
			
			Inventory invToAdd = new Inventory(Constants.DEFAULT_SPECTATOR_INV_NAME_PLAYER_PREFIX+player.getName(), "§aSpectateur", 6);
			
			int row = 0;
			int posInLine = 0;
			for(HCTeam team : getPmApi().getTeams()){
				for(HCPlayer hcPlayer : team.getMembers()){
					if(hcPlayer.is(true, PlayerState.PLAYING)){
						Item playerHeadItem = getCallbacksApi().getSpecInvPlayerItem(hcPlayer).withPosition(9*row+posInLine).build();
						playerHeadItem.setActions(Lists.newArrayList(new SpecInventoryTpAction(hcPlayer)));
						invToAdd.addItem(playerHeadItem);
						posInLine++;
						if(posInLine>8){
							row++;
							posInLine++;
						}
					}
				}
				row++;
				posInLine=0;
			}
			
			im.addInventory(invToAdd);
			invToAdd.openInventory(player, sigPlayer);
			executeNextAction(player, sigPlayer, true);
		}
		
	}
	
	/**
	 * Action to execute when teleporting to a player when a spectator
	 * clicks on a player head in the spectator inventory
	 *
	 */
	private class SpecInventoryTpAction extends Action{

		
		private HCPlayer target;

		public SpecInventoryTpAction(HCPlayer target){
			this.target = target;
		}
		
		@Override
		public void executeAction(Player player, SigPlayer sigPlayer) {
			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			if(hcPlayer != null && target.is(true, PlayerState.PLAYING)){
				player.closeInventory();
				
				Location loc = target.getPlayer().getLocation().clone();
				player.teleport(loc.subtract(loc.getDirection().multiply(1.5)));
				getSoundApi().play(hcPlayer, Sound.ENTITY_ENDERMAN_TELEPORT, 1, 1);
				getStringsApi().get("messages.spec.teleported-to-target").replace("%player%", target.getColor()+target.getName()).sendChat(hcPlayer);
				executeNextAction(player, sigPlayer,true);
			}else{
				getStringsApi().get("messages.spec.target-not-playing").replace("%player%", target.getName());
				executeNextAction(player, sigPlayer,false);
			}
		}
		
	}
	

	/**
	 * A team placeholder to display the members inside a team on an item
	 * Usage: {members.Rouge}
	 * @author Valentin
	 *
	 */
	private class TeamPlaceholder extends SimplePlaceholder{

		private HCTeam hcTeam;
		
		public TeamPlaceholder(String pattern, HCTeam hcTeam) {
			super(pattern);
			this.hcTeam = hcTeam;
		}
		
		@Override
		public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
			List<HCPlayer> members = hcTeam.getMembers();
			List<String> names = Lists.newArrayList();
			for(HCPlayer hcPlayer : members){
				if(hcPlayer.is(true, PlayerState.WAITING)){
					names.add(hcPlayer.getColor()+"- "+hcPlayer.getName());
				}
			}
			String replacement = String.join("<br>", names);
			return original.replaceAll(getPattern(), replacement);
		}

	}
	
	
}
