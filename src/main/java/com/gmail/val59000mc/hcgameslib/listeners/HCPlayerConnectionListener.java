package com.gmail.val59000mc.hcgameslib.listeners;

import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerAllowedToJoinEvent;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;


public class HCPlayerConnectionListener extends HCListener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent event){
		Player player = event.getPlayer();
		
//		if(player.isOp()){
//			player.getServer().dispatchCommand(player.getServer().getConsoleSender(), "deop "+player.getName());
//		}
		
		boolean isAllowedToJoin = ((PlayersManager) getPmApi()).isPlayerAllowedToJoin(player, getApi().getGameState());
		
		HCPlayerAllowedToJoinEvent customEvent = new HCPlayerAllowedToJoinEvent(getApi(), player, isAllowedToJoin);
		Bukkit.getPluginManager().callEvent(customEvent);
		
		if(customEvent.isAllowedToJoin()){
			if(event.getResult().equals(Result.KICK_FULL)){
				// if game is full, trying to login as moderator
				if(player.hasPermission("hardcraftpvp.login-full")){
					event.setResult(Result.ALLOWED);
				// else trying to login as a playing player
				}else{
					HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
					if(hcPlayer != null && hcPlayer.is(PlayerState.PLAYING)){
						event.setResult(Result.ALLOWED);
					}
				}
			}
		}else{
			event.setKickMessage(getStringsApi().get("messages.player-not-allowed-to-join").toString());
			event.setResult(Result.KICK_OTHER);
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(final PlayerJoinEvent event){
		if(getApi().getConfig().getBoolean("config.disable-join-and-quit-messages", Constants.DEFAULT_DISABLED_JOIN_AND_QUIT_MESSAGES)){
			event.setJoinMessage("");
		}
		
		Bukkit.getScheduler().runTaskLater(getPlugin(), new Runnable(){

			@Override
			public void run() {
				((PlayersManager) getPmApi()).playerJoinsTheGame(event.getPlayer());
			}
			
		},1);
		
	}
	
	
	
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDisconnect(PlayerQuitEvent event){
		if(getApi().getConfig().getBoolean("config.disable-join-and-quit-messages", Constants.DEFAULT_DISABLED_JOIN_AND_QUIT_MESSAGES)){
			event.setQuitMessage("");
		}
		
		((PlayersManager) getPmApi()).playerLeavesTheGame(event.getPlayer());
		
	}

}
