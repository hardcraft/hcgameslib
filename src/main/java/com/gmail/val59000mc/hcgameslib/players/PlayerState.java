package com.gmail.val59000mc.hcgameslib.players;

public enum PlayerState {
  WAITING, // waiting at lobby
  PLAYING, // playing the game
  SPECTATING, // spectating, never played
  DEAD, // spectating, having played before
  VANISHED; // not playing, invisible
}
