package com.gmail.val59000mc.hcgameslib.players;


import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;

/**
 * Represent a player's game session to temporarily stores the scores of a player to be able to save it to database regularly and lcear it
 *
 */
public class HCPlayerSession{

	private HCPlayer originalPlayer;
	private HCPlayer fakeSessionPlayer;

	// timestamp
	private long nextPersist;
	private long persistDelay;
	
	public HCPlayerSession(HCPlayer originalPlayer, HCPlayer fakeSessionPlayer, long persistDelay) {
		this.originalPlayer = originalPlayer;
		this.fakeSessionPlayer = fakeSessionPlayer;
		this.persistDelay = persistDelay;
		refreshNextPersist();
	}
	
	public void refreshNextPersist(){
		this.nextPersist = System.currentTimeMillis() + persistDelay;
		Log.debug("Next persist for "+originalPlayer.getName()+" in "+persistDelay+"ms");
	}
	
	public boolean isNextScheduledPersistPassed(){
		return nextPersist <= System.currentTimeMillis();
	}

	public HCPlayer calculateDiff(HCGameAPI api){
		HCPlayer diff = api.getCallbacksApi().newHCPlayer(originalPlayer);
		diff.subtractBy(fakeSessionPlayer);
		fakeSessionPlayer = api.getCallbacksApi().newHCPlayer(originalPlayer);
		return diff;
	}
	
}
