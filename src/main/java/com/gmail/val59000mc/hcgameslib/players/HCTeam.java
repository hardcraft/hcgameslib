package com.gmail.val59000mc.hcgameslib.players;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HCTeam {

    private String name;
    private ChatColor color;

    protected Location spawnpoint;

    protected List<HCPlayer> members;
    protected int kills;
    protected int deaths;
    protected int money;


    public HCTeam(String name, ChatColor color, Location spawnpoint) {
        this.name = name;
        this.color = color;
        this.spawnpoint = spawnpoint;

        this.members = new ArrayList<HCPlayer>();
        this.kills = 0;
        this.deaths = 0;
        this.money = 0;
    }

    public boolean contains(HCPlayer player) {
        return members.contains(player);
    }

    public synchronized List<HCPlayer> getMembers() {
        return members;
    }

    public List<HCPlayer> getMembers(Boolean online, PlayerState state) {
        List<HCPlayer> filteredPlayers = new ArrayList<HCPlayer>();
        for (HCPlayer hcPlayer : members) {
            if (online == null && state == null) {
                filteredPlayers.add(hcPlayer);
            } else if (online != null && state == null) {
                if (hcPlayer.isOnline() == online)
                    filteredPlayers.add(hcPlayer);
            } else if (online == null && state != null) {
                if (hcPlayer.is(state))
                    filteredPlayers.add(hcPlayer);
            } else if (online != null && state != null) {
                if (hcPlayer.is(online, state))
                    filteredPlayers.add(hcPlayer);
            }
        }
        return filteredPlayers;
    }

    public synchronized List<String> getMembersNames() {
        List<String> names = new ArrayList<String>();
        for (HCPlayer player : getMembers()) {
            names.add(player.getName());
        }
        return names;
    }

    protected void addKill() {
        this.kills++;
    }

    public void addDeath() {
        this.deaths++;
    }

    public int getKills() {
        return this.kills;
    }

    public int getDeaths() {
        return this.deaths;
    }

    public void addPlayer(HCPlayer hcPlayer) {
        getMembers().add(hcPlayer);
        hcPlayer.setTeam(this);
        updateVanillaTeams(hcPlayer);
    }

    public void removePlayer(HCPlayer hcPlayer) {
        getMembers().remove(hcPlayer);
        hcPlayer.setTeam(null);
        updateVanillaTeams(hcPlayer);
    }

    private void updateVanillaTeams(HCPlayer changedPlayer) {
        List<HCPlayer> teammates = getMembers();
        for (HCPlayer teammate : teammates) {
            teammate.updateVanillaTeam(teammates);
        }
        if (changedPlayer.getTeam() == null) {
            changedPlayer.updateVanillaTeam(Collections.emptyList());
        }
    }

    public boolean isAtLeastOnline() {
        for (HCPlayer gPlayer : getMembers()) {
            if (gPlayer.isOnline()) {
                return true;
            }
        }
        return false;
    }

    public boolean isAtLeast(PlayerState playerState) {
        for (HCPlayer gPlayer : getMembers()) {
            if (gPlayer.is(PlayerState.PLAYING)) {
                return true;
            }
        }
        return false;
    }


    public boolean isAsLeastOnlineAnd(PlayerState playerState) {
        for (HCPlayer gPlayer : getMembers()) {
            if (gPlayer.isOnline() && gPlayer.is(playerState)) {
                return true;
            }
        }
        return false;
    }

    public List<HCPlayer> getOtherMembers(HCPlayer excludedPlayer) {
        List<HCPlayer> otherMembers = new ArrayList<HCPlayer>();
        for (HCPlayer uhcPlayer : getMembers()) {
            if (!uhcPlayer.equals(excludedPlayer))
                otherMembers.add(uhcPlayer);
        }
        return otherMembers;
    }

    public ChatColor getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public String getColoredName() {
        return color + name;
    }

    public Location getSpawnpoint() {
        return spawnpoint;
    }


}
