package com.gmail.val59000mc.hcgameslib.players;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.boosters.Booster;
import com.gmail.val59000mc.hcgameslib.donations.Donation;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.scoreboards.GameScoreboard;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.spigotutils.Numbers;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.ref.WeakReference;
import java.util.*;

public class HCPlayer {

    protected int id;
    protected double rankHardcoinsMultiplier;
    protected String name;
    protected WeakReference<Player> player;
    protected UUID uuid;
    protected HCTeam team;
    protected PlayerState state;
    protected boolean globalChat;
    protected Stuff stuff;
    protected int kills;
    protected int deaths;
    protected double money;
    protected int wins;
    protected Location spawnpoint;
    protected GameScoreboard scoreboard;
    protected int timePlayed;
    protected int timeOffline;
    private long lastMovement;
    protected boolean canSeeVanishedPlayers;
    protected HCPlayerSession session;

    protected Booster activeBooster;
    protected List<Donation> donations;


    // Constructor

    public HCPlayer(HCPlayer hcPlayer) {
        this.id = hcPlayer.id;
        this.rankHardcoinsMultiplier = hcPlayer.rankHardcoinsMultiplier;
        this.name = hcPlayer.name;
        this.player = hcPlayer.player;
        this.uuid = hcPlayer.uuid;
        this.team = hcPlayer.team;
        this.state = hcPlayer.state;
        this.globalChat = hcPlayer.globalChat;
        this.stuff = hcPlayer.stuff;
        this.kills = hcPlayer.kills;
        this.deaths = hcPlayer.deaths;
        this.money = hcPlayer.money;
        this.wins = hcPlayer.wins;
        this.timePlayed = hcPlayer.timePlayed;
        this.timeOffline = hcPlayer.timeOffline;
        this.lastMovement = hcPlayer.lastMovement;
        this.spawnpoint = hcPlayer.spawnpoint;
        this.scoreboard = hcPlayer.scoreboard;
        this.canSeeVanishedPlayers = hcPlayer.canSeeVanishedPlayers;
        this.session = hcPlayer.session;
        this.activeBooster = hcPlayer.activeBooster;
        this.donations = hcPlayer.donations;
    }

    public void subtractBy(HCPlayer lastUpdatedSession) {
        this.kills -= lastUpdatedSession.kills;
        this.deaths -= lastUpdatedSession.deaths;
        this.money -= lastUpdatedSession.money;
        this.timePlayed -= lastUpdatedSession.timePlayed;
        this.wins -= lastUpdatedSession.wins;
    }

    public HCPlayer(Player player) {
        this.id = 0;
        this.rankHardcoinsMultiplier = 1;
        this.name = player.getName();
        this.uuid = player.getUniqueId();
        this.player = new WeakReference<Player>(player);
        this.team = null;
        this.state = PlayerState.WAITING;
        this.globalChat = true;
        this.stuff = null;
        this.kills = 0;
        this.deaths = 0;
        this.money = 0;
        this.wins = 0;
        this.timePlayed = 0;
        this.timeOffline = 0;
        this.lastMovement = 0;
        this.spawnpoint = null;
        this.scoreboard = null;
        this.canSeeVanishedPlayers = false;
        this.session = null;
        this.activeBooster = null;
        this.donations = new ArrayList<>(3);
    }

    // Accessors


    public HCPlayer() {
    }

    public String getName() {
        return name;
    }

    public String getColoredName() {
        return getColor() + name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRankHardcoinsMultiplier() {
        return rankHardcoinsMultiplier;
    }

    public void setRankHardcoinsMultiplier(double rankHardcoinsMultiplier) {
        this.rankHardcoinsMultiplier = rankHardcoinsMultiplier;
    }

    public boolean canSeeVanishedPlayers() {
        return canSeeVanishedPlayers;
    }

    public void setCanSeeVanishedPlayers(boolean canSeeVanishedPlayers) {
        this.canSeeVanishedPlayers = canSeeVanishedPlayers;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setWeakRefPlayer(Player player) {
        this.player = new WeakReference<Player>(player);
    }

    public GameScoreboard getScoreboard() {
        return scoreboard;
    }

    public void setScoreboard(GameScoreboard scoreboard) {
        this.scoreboard = scoreboard;
    }

    public HCPlayerSession getSession() {
        return session;
    }

    public void setSession(HCPlayerSession session) {
        this.session = session;
    }

    public Location getSpawnPoint() {
        return spawnpoint;
    }

    public void setSpawnPoint(Location location) {
        this.spawnpoint = location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasTeam() {
        return team != null;
    }

    public synchronized HCTeam getTeam() {
        return team;
    }

    public synchronized void setTeam(HCTeam team) {
        this.team = team;
    }

    public PlayerState getState() {
        return state;
    }

    protected void setState(HCGameAPI api, PlayerState state) {
        PlayerState oldState = this.state;
        this.state = state;
        Bukkit.getPluginManager().callEvent(new HCPlayerStateChangeEvent(api, this, oldState, this.state));
    }

    public double getMoney() {
        return Numbers.round(money, 2);
    }

    public void addMoney(double money) {
        this.money += money;
    }

    public boolean isGlobalChat() {
        return globalChat;
    }

    public void setGlobalChat(boolean globalChat) {
        this.globalChat = globalChat;
    }

    public int getTimePlayed() {
        return timePlayed;
    }

    public int getTimeOffline() {
        return timeOffline;
    }

    public void addTimePlayed(int time) {
        this.timePlayed += time;
    }

    public void addTimeOffline(int time) {
        this.timeOffline += time;
    }

    public void resetTimeOffline() {
        this.timeOffline = 0;
    }

    public int getKills() {
        return kills;
    }

    public void addKill() {
        this.kills++;
        if (hasTeam()) {
            getTeam().addKill();
        }
    }

    public int getWins() {
        return wins;
    }

    public void addWin() {
        this.wins++;
    }

    public int getDeaths() {
        return deaths;
    }

    public void addDeath() {
        this.deaths++;
        if (hasTeam()) {
            getTeam().addDeath();
        }
    }

    public ChatColor getColor() {
        if (getTeam() != null) {
            return getTeam().getColor();
        } else {
            return ChatColor.WHITE;
        }
    }

    // Methods


    public Player getPlayer() {
        return player.get();
    }

    public boolean isInTeamWith(HCPlayer player) {
        return (team != null && team.equals(player.getTeam()));
    }

    public boolean isTeam(HCTeam team) {
        return this.team != null && team.equals(this.team);
    }

    public void teleportToSpawnPoint() {
        if (isOnline() && getSpawnPoint() != null) {
            getPlayer().teleport(getSpawnPoint());
        }
    }

    public boolean is(PlayerState state) {
        return this.state.equals(state);
    }

    public boolean isAny(Set<PlayerState> states) {
        return states.contains(this.state);
    }

    public boolean is(boolean online, PlayerState state) {
        return isOnline() == online && this.state.equals(state);
    }

    public Boolean isOnline() {
        Player p = getPlayer();
        return p != null && p.isOnline();
    }

    public boolean isPlaying() {
        return isOnline() && is(PlayerState.PLAYING);
    }

    public Stuff getStuff() {
        return stuff;
    }

    public void setStuff(Stuff stuff) {
        this.stuff = stuff;
    }

    public long getLastMovementt() {
        return lastMovement;
    }

    public void refreshLastMovement() {
        this.lastMovement = System.currentTimeMillis();
    }

    public Booster getActiveBooster() {
        return activeBooster;
    }

    public void setActiveBooster(Booster activeBooster) {
        this.activeBooster = activeBooster;
    }

    public List<Donation> getDonations() {
        return donations;
    }

    public boolean hasDonated() {
        return !donations.isEmpty();
    }

    public int countDonations() {
        return donations.size();
    }

    public void setDonations(List<Donation> donations) {
        this.donations = donations;
    }

    public void updateVanillaTeam(Collection<HCPlayer> teammates) {
        if (scoreboard != null) {
            scoreboard.updateVanillaTeam(team, teammates);
        }
    }
}
