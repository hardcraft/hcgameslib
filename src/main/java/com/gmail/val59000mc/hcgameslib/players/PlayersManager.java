package com.gmail.val59000mc.hcgameslib.players;

import com.gmail.val59000mc.hcgameslib.api.*;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerPersistSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.*;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.scoreboards.GameScoreboard;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Time;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.projectiles.ProjectileSource;

import java.util.*;
import java.util.stream.Collectors;

public class PlayersManager implements HCPlayersManagerAPI {

    private HCGame game;

    private Map<UUID, HCPlayer> players;
    private Map<String, HCTeam> teams;
    private HCTaskScheduler startingTaskScheduler;
    private HCTaskScheduler countdownEndOfGame;

    private HCTeam winningTeam;

    public PlayersManager(HCGame game) {
        this.game = game;
        this.startingTaskScheduler = null;
        this.winningTeam = null;
        players = Collections.synchronizedMap(new LinkedHashMap<UUID, HCPlayer>());
        teams = Collections.synchronizedMap(new HashMap<String, HCTeam>());
    }

    // Accessors

    private HCGameAPI getGameApi() {
        return game;
    }

    private HCPlayersManagerAPI getPmApi() {
        return game.getPlayersManagerAPI();
    }

    private HCStringsAPI getStringsApi() {
        return game.getStringsAPI();
    }

    private HCBungeeAPI getBungeeApi() {
        return game.getBungeeAPI();
    }

    private HCBoostersAPI getBoostersApi() {
        return game.getBoostersAPI();
    }

    private HCPluginCallbacksAPI getCallbacksApi() {
        return game.getCallbacksApi();
    }

    private HCSoundAPI getSoundApi() {
        return game.getSoundAPI();
    }

    private HCDependenciesAPI getDependenciesApi() {
        return game.getDependencies();
    }

    // Internal

    public void initializeTeams(List<HCTeam> teams) {
        Preconditions.checkNotNull(teams, "teams cannot be null");
        for (HCTeam team : teams) {
            this.teams.put(team.getName(), team);
        }

    }

    public boolean isPlayerAllowedToJoin(Player player, GameState state) {

        boolean allowed = false;

        switch (state) {
            case WAITING:
            case PLAYING:
            case CONTINUOUS:
                allowed = true;
                break;
            case INITIALIZING:
            case STARTING:
            case LOADING:
            case ENDED:
            default:
                allowed = false;
                break;
        }

        if (game.getConfig().getBoolean("config.whitelist", Constants.DEFAULT_WHITELIST_ENABLE)) {
            if (!player.hasPermission("hardcraftpvp.whitelist." + game.getLowercaseName())) {
                allowed = false;
            }
        }

        return allowed;

    }

    public void playerJoinsTheGame(Player player) {
        HCPlayer hcPlayer = getHCPlayer(player);

        // add new unknown player
        if (hcPlayer == null) {
            hcPlayer = getCallbacksApi().newHCPlayer(player);
            hcPlayer.setScoreboard(new GameScoreboard(game, hcPlayer));
            hcPlayer.setSession(new HCPlayerSession(
                hcPlayer,
                getCallbacksApi().newHCPlayer(player),
                game.getConfig().getLong("mysql.persist-delay-millis", Constants.DEFAULT_PERSIST_DELAY_MILLIS)
            ));
            players.put(hcPlayer.getUuid(), hcPlayer);
        } else {
            hcPlayer.setWeakRefPlayer(player);
            hcPlayer.setName(player.getName());
        }

        hcPlayer.resetTimeOffline();

        switch (game.getGameState()) {
            case WAITING:
                hcPlayer.setState(game, PlayerState.WAITING);
                break;
            case INITIALIZING:
            case STARTING:
            case LOADING:
            case ENDED:
                // should not happen because a player cannot join during these states
                hcPlayer.setState(game, PlayerState.SPECTATING);
                break;
            case PLAYING:
                if (!hcPlayer.is(PlayerState.PLAYING)) {
                    if (hcPlayer.hasTeam()) {
                        hcPlayer.setState(game, PlayerState.DEAD);
                    } else {
                        hcPlayer.setState(game, PlayerState.SPECTATING);
                    }
                }
                break;
            case CONTINUOUS:
                hcPlayer.setState(game, PlayerState.PLAYING);
            default:
                break;
        }


        // check vanish permission for joining player
        if (!player.isOp() && player.hasPermission("hardcraftpvp.be-vanished")) {
            removePlayerFromTeamSilently(hcPlayer);
            hcPlayer.setState(game, PlayerState.VANISHED);
        }
        if (!player.isOp() && player.hasPermission("hardcraftpvp.see-vanished")) {
            hcPlayer.setCanSeeVanishedPlayers(true);
        } else {
            hcPlayer.setCanSeeVanishedPlayers(false);
        }

        Bukkit.getPluginManager().callEvent(new HCPlayerJoinEvent(game, hcPlayer));

        switch (hcPlayer.getState()) {
            case WAITING:
                waitPlayerAtLobby(hcPlayer);
                break;
            case PLAYING:
                relogPlayingPlayer(hcPlayer);
                break;
            case VANISHED:
                vanishPlayer(hcPlayer);
                break;
            case DEAD:
            case SPECTATING:
            default:
                spectatePlayer(hcPlayer);
                break;
        }

        refreshRankNameTag(hcPlayer);
        refreshRankHardcoinsMultiplier(hcPlayer);

        refreshVisiblePlayers(hcPlayer);
        updatePlayersScoreboards();

        if (game.getConfig().getBoolean("config.whitelist")) {
            game.getStringsAPI()
                .get("messages.command.wlist.activated-join-message")
                .sendChatP(hcPlayer);
        }

    }

    public void playerLeavesTheGame(Player player) {

        HCPlayer hcPlayer = getHCPlayer(player);

        if (hcPlayer != null) {

            switch (getGameApi().getGameState()) {
                case PLAYING:
                    if (hcPlayer.is(PlayerState.PLAYING)) {
                        disconnectWhilePlaying(hcPlayer);
                    }
                    break;
                case WAITING:
                    if (hcPlayer.is(PlayerState.WAITING)) {
                        removePlayerFromTeamSilently(hcPlayer);
                        players.remove(hcPlayer.getUuid());
                    }
                    break;
                case STARTING:
                case LOADING:
                case ENDED:
                case CONTINUOUS:
                default:
                    break;
            }

            Bukkit.getPluginManager().callEvent(new HCPlayerQuitEvent(getGameApi(), hcPlayer));

            if (!getGameApi().is(GameState.ENDED)) {
                dispatchPersistPlayer(hcPlayer);
            }

            Bukkit.getScheduler().runTaskLater(getGameApi().getPlugin(), new Runnable() {
                public void run() {
                    updatePlayersScoreboards();
                    checkIfLastTeamOnline();
                }
            }, 1);

        }

    }

    private void waitPlayerAtLobby(HCPlayer hcPlayer) {

        hcPlayer.setState(game, PlayerState.WAITING);
        hcPlayer.setGlobalChat(true);

        if (hcPlayer.isOnline()) {

            hcPlayer.getPlayer().setGameMode(GameMode.ADVENTURE);

        }

        game.getCallbacksApi().waitPlayerAtLobby(hcPlayer);

        checkEnoughPlayersToStart(hcPlayer);

        Bukkit.getPluginManager().callEvent(new HCPlayerWaitAtLobbyEvent(game, hcPlayer));

    }


    private void checkEnoughPlayersToStart(HCPlayer hcPlayer) {
        if (!game.isContinuous() && game.getConfig().getBoolean("config.start-scheduler.enabled", Constants.DEFAULT_START_SCHEDULER_ENABLED)) {

            int waiting = getPlayersCount(true, PlayerState.WAITING);
            Log.debug("checkEnoughPlayersToStart, waiting players=" + waiting);

            if (waiting >= getMinPlayers()) {
                startStartingTask();
            }

            if (waiting > getMaxPlayers()) {
                getCallbacksApi().alertGameIsFull(hcPlayer);
            }

        }


    }

    public void relogPlayingPlayer(HCPlayer hcPlayer) {

        hcPlayer.setState(game, PlayerState.PLAYING);

        if (game.isContinuous() && getPlayersCount(true, PlayerState.PLAYING) > getMaxPlayers()) {
            getCallbacksApi().alertGameIsFull(hcPlayer);
            spectatePlayer(hcPlayer);
        } else {
            game.getCallbacksApi().relogPlayingPlayer(hcPlayer);

            Bukkit.getPluginManager().callEvent(new HCPlayerRelogEvent(game, hcPlayer));
        }


    }

    public void spectatePlayer(HCPlayer hcPlayer) {

        if (hcPlayer.is(PlayerState.PLAYING) || hcPlayer.is(PlayerState.DEAD))
            hcPlayer.setState(game, PlayerState.DEAD);
        else
            hcPlayer.setState(game, PlayerState.SPECTATING);

        if (hcPlayer.isOnline()) {

            Player player = hcPlayer.getPlayer();
            player.setGameMode(GameMode.SPECTATOR);
        }

        game.getCallbacksApi().spectatePlayer(hcPlayer);

        Bukkit.getPluginManager().callEvent(new HCPlayerSpectateEvent(game, hcPlayer));

    }

    public void vanishPlayer(HCPlayer hcPlayer) {

        hcPlayer.setState(game, PlayerState.VANISHED);

        if (hcPlayer.isOnline()) {
            Player player = hcPlayer.getPlayer();
            player.setGameMode(GameMode.SPECTATOR);
        }

        game.getCallbacksApi().vanishedPlayer(hcPlayer);

        Bukkit.getPluginManager().callEvent(new HCPlayerVanishedEvent(game, hcPlayer));

    }

    @Override
    public void eliminatePlayer(HCPlayer hcPlayer) {
        spectatePlayer(hcPlayer);

        game.getCallbacksApi().eliminatedPlayer(hcPlayer);

        Bukkit.getPluginManager().callEvent(new HCPlayerEliminatedEvent(game, hcPlayer));

        updatePlayersScoreboards();

        checkIfLastTeamOnline();

    }

    /**
     * Start a task which eliminate the player after disconnecting when playing
     */
    public void disconnectWhilePlaying(HCPlayer hcPlayer) {
        if (game.getConfig().getBoolean("config.auto-eliminate.enabled", Constants.DEFAULT_AUTO_ELIMINATE_ENABLED)) {

            int timeToRecconect = game.getConfig().getInt("config.auto-eliminate.seconds-before-elimination", Constants.DEFAULT_SECONDS_BEFORE_ELIMINATION);

            game.buildTask("wait for player reconection", new HCTask() {

                @Override
                public void run() {
                    if (game.is(GameState.PLAYING)) {

                        if (hcPlayer.isOnline()) {
                            stop();
                        }

                    } else {
                        stop();
                    }
                }
            })
                .withDelay(0)
                .withInterval(20)
                .withIterations(timeToRecconect)
                .withFirstCallback(new HCTask() {
                    @Override
                    public void run() {
                        getStringsApi()
                            .get("messages.player-should-reconnect-before-elimination")
                            .replace("%player%", hcPlayer.getName())
                            .replace("%time%", Time.getFormattedTime(timeToRecconect))
                            .sendChatP();
                    }
                })
                .withLastCallback(new HCTask() {

                    @Override
                    public void run() {
                        getPmApi().eliminatePlayer(hcPlayer);
                    }
                })
                .build()
                .start();

        }


    }

    public void startAllPlayers() {

        Bukkit.getPluginManager().callEvent(new HCBeforeStartAllPlayerEvent(game));

        assignRandomTeamsToPlayers();


        for (HCPlayer hcPlayer : players.values()) {
            if (hcPlayer.getTeam() == null) {
                if (hcPlayer.is(PlayerState.VANISHED)) {
                    vanishPlayer(hcPlayer);
                } else {
                    spectatePlayer(hcPlayer);
                }
            } else {
                startPlayer(hcPlayer);
            }
            refreshVisiblePlayers(hcPlayer);
            refreshRankNameTag(hcPlayer, 0);
        }

        updatePlayersScoreboards();

        Bukkit.getPluginManager().callEvent(new HCAfterStartAllPlayerEvent(game));

    }


    public void startPlayerTimeTask() {

        game.buildTask("add timePlayer timeOffline and check persist", new HCTask() {

            @Override
            public void run() {

                for (HCPlayer hcPlayer : getPlayers(true, null)) {
                    if (hcPlayer.is(PlayerState.PLAYING)) {
                        hcPlayer.addTimePlayed(1);

                        // persist online and playing players regularly
                        if (hcPlayer.getSession().isNextScheduledPersistPassed() && hcPlayer.isOnline()) {
                            dispatchPersistPlayer(hcPlayer);
                        }
                    }
                }
                for (HCPlayer hcPlayer : getPlayers(false, null)) {
                    hcPlayer.addTimeOffline(1);
                    // Remove player if offline for 15 minutes
                    if (hcPlayer.getTimeOffline() >= 15) {
                        removePlayerFromTeamSilently(hcPlayer);
                        players.remove(hcPlayer.getUuid());
                    }
                }
            }

        })
            .withDelay(1200)
            .withInterval(1200)
            .addListener(new HCTaskListener() {

                @EventHandler
                public void onGameEnd(HCBeforeEndEvent e) {
                    getScheduler().stop();
                }

                @EventHandler
                public void onGameEnd(HCBeforeStopContinuousEvent e) {
                    getScheduler().stop();
                }

            })
            .build()
            .start();

    }

    public void dispatchPersistPlayer(HCPlayer hcPlayer) {
        if (game.getMySQLAPI().isEnabled()) {
            HCPlayer diff = hcPlayer.getSession().calculateDiff(game);
            hcPlayer.getSession().refreshNextPersist();
            Log.debug("Dispatch persist for " + hcPlayer.getName());
            Bukkit.getPluginManager().callEvent(new HCPlayerPersistSessionEvent(game, diff));
        }
    }

    public void endAllPlayers() {
        Bukkit.getPluginManager().callEvent(new HCBeforeEndAllPlayerEvent(game));

        if (winningTeam != null) {
            for (HCPlayer teammate : winningTeam.getMembers()) {
                getPmApi().rewardMoneyTo(teammate, getCallbacksApi().getWinReward(teammate));
            }
        }

        for (HCPlayer hcPlayer : players.values()) {
            hcPlayer.setGlobalChat(true);
            spectatePlayer(hcPlayer);

            getCallbacksApi().printEndMessage(hcPlayer, winningTeam);

            if (hcPlayer.isOnline()) {
                dispatchPersistPlayer(hcPlayer);
            }
        }

        getCallbacksApi().endGameMessages(winningTeam);

        Bukkit.getPluginManager().callEvent(new HCAfterEndAllPlayerEvent(game));
    }


    public void persistAllOnlinePlayersSessions() {
        for (HCPlayer hcPlayer : getPlayers(true, null)) {
            dispatchPersistPlayer(hcPlayer);
        }
    }

    private void assignRandomTeamsToPlayers() {

        List<HCPlayer> playersToRandomize = Lists.newArrayList(players.values());

        List<HCPlayer> randomizedPlayers = getCallbacksApi().getRandomizedPlayersAndAssignedToTeams(playersToRandomize);
        this.players = Collections.synchronizedMap(new LinkedHashMap<UUID, HCPlayer>());
        Log.debug("Randomized player order :");
        for (HCPlayer hcPlayer : randomizedPlayers) {
            Log.debug(hcPlayer.getName());
            this.players.put(hcPlayer.getUuid(), hcPlayer);
        }

    }

    private void startPlayer(HCPlayer hcPlayer) {
        Log.debug("Starting HCPlayer " + hcPlayer.getName());

        hcPlayer.setState(game, PlayerState.PLAYING);
        hcPlayer.setGlobalChat(false);

        if (hcPlayer.isOnline()) {
            hcPlayer.getPlayer().setGameMode(GameMode.SURVIVAL);
        }

        getCallbacksApi().assignStuffToPlayer(hcPlayer);

        hcPlayer.setSpawnPoint(getCallbacksApi().getFirstRespawnLocation(hcPlayer));

        getCallbacksApi().startPlayer(hcPlayer);

        Bukkit.getPluginManager().callEvent(new HCPlayerStartEvent(game, hcPlayer));

    }


    @Override
    public void revivePlayer(HCPlayer hcPlayer) {
        if (hcPlayer.hasTeam() && (hcPlayer.is(true, PlayerState.DEAD) || hcPlayer.is(true, PlayerState.SPECTATING))) {
            hcPlayer.setState(game, PlayerState.PLAYING);

            hcPlayer.setStuff(null);
            getCallbacksApi().assignStuffToPlayer(hcPlayer);

            hcPlayer.setSpawnPoint(getCallbacksApi().getFirstRespawnLocation(hcPlayer));

            game.getCallbacksApi().revivePlayer(hcPlayer);

            Bukkit.getPluginManager().callEvent(new HCPlayerReviveEvent(game, hcPlayer));
        }

    }

    public void respawnPlayer(HCPlayer hcPlayer) {

        getCallbacksApi().assignStuffToPlayer(hcPlayer);

        game.getCallbacksApi().respawnPlayer(hcPlayer);

        Bukkit.getPluginManager().callEvent(new HCPlayerRespawnEvent(game, hcPlayer));

    }


    // API

    @Override
    public void updatePlayersScoreboards() {

        if (game.getConfig().getBoolean("config.scoreboard.enable", Constants.DEFAULT_ENABLE_SCOREBOARDS)) {
            for (HCPlayer hcp : getPlayers()) {
                if (hcp.getScoreboard() != null)
                    hcp.getScoreboard().update();
            }
        }

    }

    @Override
    public HCTeam addTeam(String name, ChatColor color, Location spawnpoint) {
        HCTeam team = getCallbacksApi().newHCTeam(name, color, spawnpoint);
        if (teams.containsKey(name)) {
            throw new IllegalArgumentException("A team with that name is already present.");
        }
        this.teams.put(name, team);
        return team;
    }

    @Override
    public List<HCPlayer> getPlayers() {
        return Lists.newArrayList(players.values());
    }

    @Override
    public List<HCPlayer> getPlayers(Boolean online, PlayerState state) {
        List<HCPlayer> filteredPlayers = new ArrayList<HCPlayer>();
        for (HCPlayer hcPlayer : players.values()) {
            if (online == null && state == null) {
                filteredPlayers.add(hcPlayer);
            } else if (online != null && state == null) {
                if (hcPlayer.isOnline() == online)
                    filteredPlayers.add(hcPlayer);
            } else if (online == null && state != null) {
                if (hcPlayer.is(state))
                    filteredPlayers.add(hcPlayer);
            } else if (online != null && state != null) {
                if (hcPlayer.is(online, state))
                    filteredPlayers.add(hcPlayer);
            }
        }
        return filteredPlayers;
    }

    @Override
    public List<HCPlayer> getPlayersMultiplesStates(Boolean online, Set<PlayerState> states) {
        List<HCPlayer> filteredPlayers = new ArrayList<HCPlayer>();
        for (HCPlayer hcPlayer : players.values()) {
            if (online == null && states == null) {
                filteredPlayers.add(hcPlayer);
            } else if (online != null && states == null) {
                if (hcPlayer.isOnline() == online)
                    filteredPlayers.add(hcPlayer);
            } else if (online == null && states != null) {
                if (states.contains(hcPlayer.getState()))
                    filteredPlayers.add(hcPlayer);
            } else if (online != null && states != null) {
                if (hcPlayer.isOnline() == online && states.contains(hcPlayer.getState()))
                    filteredPlayers.add(hcPlayer);
            }
        }
        return filteredPlayers;
    }

    @Override
    public List<HCTeam> getTeams() {
        return Lists.newArrayList(teams.values());
    }

    @Override
    public boolean hasWon(HCTeam team) {
        return team != null && winningTeam != null && team.equals(winningTeam);
    }

    @Override
    public HCTeam getWinningTeam() {
        return winningTeam;
    }

    public void setWinningTeam(HCTeam winningTeam) {
        this.winningTeam = winningTeam;
        if (winningTeam != null) {
            for (HCPlayer hcPlayer : winningTeam.getMembers()) {
                if (hcPlayer.isOnline()) {
                    hcPlayer.addWin();
                }
            }
        }
    }

    @Override
    public List<HCTeam> getTeamsFromConfig() {
        ConfigurationSection cfg = game.getConfig().getConfigurationSection("teams");

        Preconditions.checkNotNull(cfg, "Configuration 'teams' section cannot be null to add teams from config");

        List<HCTeam> teams = new ArrayList<HCTeam>();

        for (String key : cfg.getKeys(false)) {
            ConfigurationSection teamSection = cfg.getConfigurationSection(key);

            String name = teamSection.getString("name");
            ChatColor color = ChatColor.valueOf(teamSection.getString("color"));

            Preconditions.checkNotNull(name, "'" + key + "' team's name cannot be null");
            Preconditions.checkNotNull(color, "'" + key + "' team's color cannot be null");

            String spawnpointStr = teamSection.getString("spawnpoint");
            Location spawnpoint = null;
            if (spawnpointStr != null)
                spawnpoint = Parser.parseLocation(game.getWorldConfig().getWorld(), spawnpointStr);
            teams.add(getCallbacksApi().newHCTeam(name, color, spawnpoint));
        }

        return teams;
    }

    @Override
    public void removePlayerFromTeam(HCPlayer hcPlayer) {
        if (hcPlayer.getTeam() != null) {
            HCTeam leftTeam = hcPlayer.getTeam();
            hcPlayer.getTeam().removePlayer(hcPlayer);
            hcPlayer.setSpawnPoint(null);
            refreshRankNameTag(hcPlayer, 0);
            updatePlayersScoreboards();
            Bukkit.getPluginManager().callEvent(new HCPlayerHasLeftTeamEvent(game, leftTeam, hcPlayer));
        }
    }

    @Override
    public void removePlayerFromTeamSilently(HCPlayer hcPlayer) {
        if (hcPlayer.hasTeam()) {
            hcPlayer.getTeam().removePlayer(hcPlayer);
            hcPlayer.setSpawnPoint(null);
            refreshRankNameTag(hcPlayer, 0);
            updatePlayersScoreboards();
        }
    }

    @Override
    public void addPlayerToTeam(HCPlayer hcPlayer, HCTeam hcTeam) {
        removePlayerFromTeam(hcPlayer);
        hcTeam.addPlayer(hcPlayer);
        hcPlayer.setSpawnPoint(hcTeam.getSpawnpoint());
        refreshRankNameTag(hcPlayer, 0);
        updatePlayersScoreboards();
        Bukkit.getPluginManager().callEvent(new HCPlayerHasJoinedTeamEvent(game, hcTeam, hcPlayer));
    }


    @Override
    public boolean canJoinTeam(HCPlayer hcPlayer, HCTeam hcTeam) {
        // deny join if too much player in chosen team
        double nbTeams = getPmApi().getTeams().size();
        double nbPlayers = getPmApi().getPlayers(true, null).stream().filter(
            (p) -> !p.is(PlayerState.VANISHED)
        ).collect(Collectors.toList()).size();
        double maxPlayerInATeam = Math.ceil(nbPlayers / nbTeams);
        double playersInTheTeam = hcTeam.getMembers(true, null).stream().filter(
            (p) -> !p.is(PlayerState.VANISHED)
        ).collect(Collectors.toList()).size();
        boolean allowedToJoin = playersInTheTeam + 1 <= maxPlayerInATeam;

        return allowedToJoin;
    }


    @Override
    public HCTeam getHCTeam(String name) {
        if (teams.containsKey(name)) {
            return teams.get(name);
        }
        return null;
    }

    @Override
    public HCTeam getMinMembersTeam() {
        if (getTeams().size() == 0)
            return null;

        HCTeam min = getTeams().get(0);
        for (HCTeam team : getTeams()) {
            if (team.getMembers().size() < min.getMembers().size()) {
                min = team;
            }
        }
        return min;
    }

    @Override
    public HCPlayer getHCPlayer(Player player) {
        return getHCPlayer(player.getUniqueId());
    }

    @Override
    public HCPlayer getHCPlayer(Entity entity) {
        return getHCPlayer(entity.getUniqueId());
    }

    @Override
    public HCPlayer getHCPlayer(UUID uuid) {
        if (players.containsKey(uuid)) {
            return players.get(uuid);
        }
        return null;
    }

    @Override
    public HCPlayer getHCPlayer(HCPlayer hcPlayer) {
        for (HCPlayer hcP : players.values()) {
            if (hcP.equals(hcPlayer))
                return hcPlayer;
        }
        return null;
    }


    @Deprecated
    @Override
    public HCPlayer getHCPlayer(String name) {
        for (HCPlayer gPlayer : players.values()) {
            if (gPlayer.getName().equals(name))
                return gPlayer;
        }

        return null;
    }

    @Override
    public int getPlayersCount() {
        return players.size();
    }

    @Override
    public int getPlayersCount(Boolean online, PlayerState state) {
        return getPlayers(online, state).size();
    }

    @Override
    public int getMaxPlayers() {
        return getCallbacksApi().getMaxPlayers();
    }

    @Override
    public int getMinPlayers() {
        return getCallbacksApi().getMinPlayers();
    }

    @Override
    public void refreshRankNameTag(HCPlayer hcPlayer) {

        refreshRankNameTag(hcPlayer, 20);

    }

    @Override
    public void refreshRankNameTag(HCPlayer hcPlayer, final long delay) {

        if (game.getConfig().getBoolean("config.ranks-name-tags", Constants.DEFAULT_ENABLE_RANKS_NAME_TAGS)) {

            Bukkit.getScheduler().runTaskLater(game.getPlugin(), new Runnable() {
                public void run() {

                    if (getDependenciesApi().isNametagEditEnabled() && getDependenciesApi().isPermissionsExEnabled() && hcPlayer.isOnline()) {

                        Player player = hcPlayer.getPlayer();

                        String prefix;
                        String color;
                        String donationStars = hcPlayer.hasDonated() ? Constants.STAR_ICON + " " : "";
                        if (game.getConfig().getBoolean("config.colored-ranks", Constants.DEFAULT_ENABLE_COLORED_RANKS)) {
                            prefix = getDependenciesApi().getShortPrefix(player);
                            color = getDependenciesApi().getSuffix(player);
                        } else {
                            prefix = "&7" + ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', getDependenciesApi().getShortPrefix(player)));
                            color = "&" + hcPlayer.getColor().getChar();
                        }

                        if (hcPlayer.is(PlayerState.VANISHED))
                            prefix = prefix.replace("[", "<<").replace("]", ">>");


//                        NametagEdit.getApi().setPrefix(player, prefix + donationStars + color);
//						NametagAPI.setPrefix(player.getName(), prefix+donationStars+color);
                    }

                }
            }, delay);

        }

    }


    @Override
    public void refreshRankNameTags(final long delay) {

        for (HCPlayer hcPlayer : getPlayers()) {
            refreshRankNameTag(hcPlayer, delay);
        }

    }

    @Override
    public void refreshVisiblePlayers(HCPlayer hcPlayer) {

        if (hcPlayer.isOnline()) {

            Player player = hcPlayer.getPlayer();

            for (HCPlayer otherHcPlayer : getPlayers(true, null)) {

                Player otherPlayer = otherHcPlayer.getPlayer();

                // Skip when self == other
                if (hcPlayer.equals(otherHcPlayer)) {
                    continue;
                }

                // hide/show self to others
                otherPlayer.hidePlayer(player);
                if (!hcPlayer.is(PlayerState.VANISHED) || (hcPlayer.is(PlayerState.VANISHED) && otherHcPlayer.canSeeVanishedPlayers())) {
                    otherPlayer.showPlayer(player);
                }

                // hide/show others to self
                player.hidePlayer(otherPlayer);
                if (!otherHcPlayer.is(PlayerState.VANISHED) || (otherHcPlayer.is(PlayerState.VANISHED) && hcPlayer.canSeeVanishedPlayers())) {
                    player.showPlayer(otherPlayer);
                }
            }

        }

    }

    @Override
    public void startStartingTask() {
        startStartingTask(game.getTimeBeforeStart());
    }

    @Override
    public void startStartingTask(int delay) {

        if (!game.isContinuous() && startingTaskScheduler == null && game.getGameState().equals(GameState.WAITING)) {

            game.setRemainingTimeBeforeStart(delay);

            startingTaskScheduler = game.buildTask("countdown for starting game", new HCTask() {

                @Override
                public void run() {
                    int time;
                    if (game.getRemainingTimeBeforeStart() > 10 && getPlayersCount(true, PlayerState.WAITING) >= getMaxPlayers()) {
                        game.setRemainingTimeBeforeStart(10);
                        time = 10;
                    } else {
                        time = game.decreaseTimeBeforeStart();
                    }
                    updatePlayersScoreboards();
                    getCallbacksApi().timeBeforeStartDecreased(time);
                    if (time == 0) {
                        game.startGame();
                    }
                }
            }).addListener(new HCTaskListener() {
                @EventHandler
                public void onStart(HCBeforeStartEvent e) {
                    getScheduler().stop();
                }

                @EventHandler
                public void onPlayerQuit(HCPlayerQuitEvent e) {

                    Bukkit.getScheduler().runTask(getPlugin(), new Runnable() {

                        @Override
                        public void run() {

                            if (getPlayersCount(true, PlayerState.WAITING) < getMinPlayers() && game.getRemainingTimeBeforeStart() > 10) {
                                getScheduler().stop();
                                game.setRemainingTimeBeforeStart(game.getTimeBeforeStart());
                                getStringsApi()
                                    .get("messages.cancel-starting-game")
                                    .sendChatP();
                                getSoundApi().play(Sound.BLOCK_NOTE_BLOCK_HARP, 2, 0.5f);
                                startingTaskScheduler = null;

                            }
                            ;

                        }
                    });
                }
            })
                .withDelay(20)
                .build();

            updatePlayersScoreboards();
            getCallbacksApi().timeBeforeStartDecreased(game.getRemainingTimeBeforeStart());
            startingTaskScheduler.start();
        }

    }


    @Override
    public void stopStartingTask() {
        if (startingTaskScheduler != null && game.getGameState().equals(GameState.WAITING)) {
            startingTaskScheduler.stop();
            startingTaskScheduler = null;
        }
    }


    @Override
    public boolean isStartingTaskScheduled() {
        return startingTaskScheduler != null;
    }

    @Override
    public boolean isJoinTeamWhilePlayingAllowed() {
        return getGameApi().getConfig().getBoolean("config.allow-join-when-running-game", Constants.DEFAULT_ALLOW_JOIN_WHEN_RUNNING_GAME);
    }

    public void startPreventFarAwayPlayersWaiting() {

        // DISTANCE
        String type = game.getConfig().getString("config.prevent-far-away.waiting.type", Constants.DEFAULT_PREVENT_FAR_AWAY_TYPE);
        final Double squared = game.getConfig().getDouble("config.prevent-far-away.waiting.squared-distance", Constants.DEFAULT_PREVENT_FAR_AWAY_WAITING);

        // BOUNDS
        String min = game.getConfig().getString("config.prevent-far-away.waiting.min", Constants.DEFAULT_PREVENT_FAR_AWAY_MIN);
        String max = game.getConfig().getString("config.prevent-far-away.waiting.max", Constants.DEFAULT_PREVENT_FAR_AWAY_MAX);
        final World world = game.getWorldConfig().getWorld();
        final LocationBounds bounds = new LocationBounds(Parser.parseLocation(world, min), Parser.parseLocation(world, max));

        // prenvet below 0
        final boolean preventBelowZero = game.getConfig().getBoolean("config.prevent-far-away.waiting.prevent-below-zero", Constants.DEFAULT_PREVENT_BELOW_ZERO);

        if (game.is(GameState.WAITING) && game.getConfig().getBoolean("config.prevent-far-away.waiting.enabled", Constants.DEFAULT_PREVENT_FAR_AWAY_ENABLED_WAITING)) {
            Location refLocation = game.getWorldConfig().getLobby();
            game.buildTask("prevent far away waiting", new HCTask() {

                @Override
                public void run() {
                    for (HCPlayer hcPlayer : getPlayers(true, null)) {
                        Player player = hcPlayer.getPlayer();

                        if (player.getWorld().equals(world)) {

                            // below zero
                            if ((preventBelowZero && player.getLocation().getY() < 0)

                                // far from bounds
                                || (type.equals("BOUNDS") && !bounds.contains(player.getLocation()))

                                // far from ref location
                                || (type.equals("DISTANCE") && player.getLocation().distanceSquared(refLocation) > squared)
                            ) {

                                getStringsApi().get("messages.dont-leave-map").sendChatP(hcPlayer);
                                getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 1, 1.3f);
                                player.teleport(refLocation);

                            }
                        }
                    }
                }
            })
                .withInterval(35)
                .addListener(new HCTaskListener() {
                    @EventHandler
                    public void onGameStateChange(HCBeforeStartEvent e) {
                        getScheduler().stop();
                    }
                }).build().start();
        }

    }

    public void startPreventFarAwayPlayersPlaying() {

        // DISTANCE
        String type = game.getConfig().getString("config.prevent-far-away.playing.type", Constants.DEFAULT_PREVENT_FAR_AWAY_TYPE);
        final Double squared = game.getConfig().getDouble("config.prevent-far-away.playing.squared-distance", Constants.DEFAULT_PREVENT_FAR_AWAY_PLAYING);

        // BOUNDS
        String min = game.getConfig().getString("config.prevent-far-away.playing.min", Constants.DEFAULT_PREVENT_FAR_AWAY_MIN);
        String max = game.getConfig().getString("config.prevent-far-away.playing.max", Constants.DEFAULT_PREVENT_FAR_AWAY_MAX);
        final World world = game.getWorldConfig().getWorld();
        final LocationBounds bounds = new LocationBounds(Parser.parseLocation(world, min), Parser.parseLocation(world, max));

        // prenvet below 0
        final boolean preventBelowZero = game.getConfig().getBoolean("config.prevent-far-away.playing.prevent-below-zero", Constants.DEFAULT_PREVENT_BELOW_ZERO);

        if ((game.is(GameState.PLAYING) || game.is(GameState.CONTINUOUS)) && game.getConfig().getBoolean("config.prevent-far-away.playing.enabled", Constants.DEFAULT_PREVENT_FAR_AWAY_ENABLED_PLAYING)) {
            Location refLocation = game.getWorldConfig().getCenter();
            game.buildTask("prevent far away playing", new HCTask() {

                @Override
                public void run() {
                    for (HCPlayer hcPlayer : getPlayers(true, null)) {
                        Player player = hcPlayer.getPlayer();

                        if (player.getWorld().equals(world)) {

                            // below zero
                            if ((preventBelowZero && player.getLocation().getY() < 0)

                                // far from bounds
                                || (type.equals("BOUNDS") && !bounds.contains(player.getLocation()))

                                // far from ref location
                                || (type.equals("DISTANCE") && player.getLocation().distanceSquared(refLocation) > squared)
                            ) {

                                if (hcPlayer.is(PlayerState.PLAYING)) {
                                    if (player.getHealth() > 0) {
                                        player.setLastDamageCause(null);
                                        player.setHealth(0);
                                        getSoundApi().play(hcPlayer, Sound.ENTITY_PLAYER_HURT, 1, 1);
                                    }
                                } else {
                                    player.teleport(refLocation);
                                    getStringsApi().get("messages.dont-leave-map").sendChatP(hcPlayer);
                                    getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 1, 1.3f);
                                }

                            }
                        }

                    }
                }
            })
                .withInterval(35)
                .addListener(new HCTaskListener() {
                    @EventHandler
                    public void onGameStateChange(HCBeforeEndEvent e) {
                        getScheduler().stop();
                    }
                }).build().start();
        }

    }

    @Override
    public void autoRespawnPlayerAfter20Ticks(Player player) {

        Bukkit.getScheduler().runTaskLater(game.getPlugin(), new Runnable() {

            public void run() {
                player.spigot().respawn();
            }
        }, 20);
    }

    @Override
    public double rewardMoneyNoBonusTo(HCPlayer hcPlayer, double amount) {
        double rewarded = 0; // reward can be 0 if vault isn't loaded or if an error happens

        if (hcPlayer.isOnline()) {

            if (game.getDependencies().isVaultEnabled()) {
                Player player = hcPlayer.getPlayer();
                rewarded = game.getDependencies().addMoney(player, amount);
            }

            getCallbacksApi().moneyRewardedTo(hcPlayer, rewarded);

            hcPlayer.addMoney(rewarded);

        }

        return rewarded;
    }

    @Override
    public double rewardMoneyTo(HCPlayer hcPlayer, double amount) {
        double rewarded = 0; // reward can be 0 if vault isn't loaded or if an error happens

        if (hcPlayer.isOnline()) {

            if (game.getDependencies().isVaultEnabled()) {
                Player player = hcPlayer.getPlayer();

                Double moneyToReward = getAmountAfterBonusPercentage(amount, getRankBonusPercentage(hcPlayer));

                //
                HCPlayerRewardMoneyEvent rewardMoneyEvent = new HCPlayerRewardMoneyEvent(getGameApi(), hcPlayer, moneyToReward);
                getGameApi().callEvent(rewardMoneyEvent);

                rewarded = game.getDependencies().addMoney(player, rewardMoneyEvent.getMoney());
            }

            getCallbacksApi().moneyRewardedTo(hcPlayer, rewarded);

            hcPlayer.addMoney(rewarded);

        }

        return rewarded;
    }

    private double getAmountAfterBonusPercentage(Double amount, Double bonusPercentage) {
        return amount * bonusPercentage / 100;
    }

    private double getRankBonusPercentage(HCPlayer hcPlayer) {
        double percentage = 100;
        double multipliers = hcPlayer.getRankHardcoinsMultiplier();
        multipliers += getBoostersApi().getCurrentBoosterMultiplier();
        multipliers += getCallbacksApi().getExtraHardcoinsMultiplier(hcPlayer);

        HCPlayerRewardBonusMultiplierEvent rewardPercentageEvent = new HCPlayerRewardBonusMultiplierEvent(getGameApi(), hcPlayer);
        getGameApi().callEvent(rewardPercentageEvent);
        multipliers += rewardPercentageEvent.getMultiplier();

        return percentage * multipliers;
    }

    @Override
    public void refreshRankHardcoinsMultiplier(HCPlayer hcPlayer) {
        double base = Constants.HARDCOINS_MEMBER_BASE;

        Player player = hcPlayer.getPlayer();

        if (player != null) {
            // Normal reward for ranks
            if (player.isOp() || player.hasPermission("hardcraftpvp.reward.administrateur")) {
                base = Constants.HARDCOINS_ADMINISTRATOR_BASE;
            } else if (player.hasPermission("hardcraftpvp.reward.moderateur")) {
                base = Constants.HARDCOINS_MODERATOR_BASE;
            } else if (player.hasPermission("hardcraftpvp.reward.helper")) {
                base = Constants.HARDCOINS_HELPER_BASE;
            } else if (player.hasPermission("hardcraftpvp.reward.community")) {
                base = Constants.HARDCOINS_COMMUNITY_BASE;
            } else if (player.hasPermission("hardcraftpvp.reward.builder")) {
                base = Constants.HARDCOINS_BUILDER_BASE;
            } else if (player.hasPermission("hardcraftpvp.reward.vip+")) {
                base = Constants.HARDCOINS_VIP_PLUS_BASE;
            } else if (player.hasPermission("hardcraftpvp.reward.vip")) {
                base = Constants.HARDCOINS_VIP_BASE;
            }
        }

        hcPlayer.setRankHardcoinsMultiplier(base);
    }


    @Override
    public void checkIfLastTeamOnline() {
        if (!game.isContinuous() && game.getConfig().getBoolean("config.win-if-last-online-team", Constants.DEFAULT_WIN_IF_LAST_ONLINE_TEAM)) {

            List<HCTeam> onlineTeams = new ArrayList<HCTeam>();
            for (HCTeam hcTeam : getPmApi().getTeams()) {
                if (hcTeam.isAsLeastOnlineAnd(PlayerState.PLAYING)) {
                    onlineTeams.add(hcTeam);
                }
            }
            if (onlineTeams.size() == 1) {
                game.endGame(onlineTeams.get(0));
            } else if (onlineTeams.size() == 0) {
                game.endGame(null);
            } // else keep playing

        }
    }

    @Override
    public void forceDeclareWinner() {
        if (!game.isContinuous()) {

            List<HCTeam> onlineTeams = new ArrayList<HCTeam>();
            for (HCTeam hcTeam : getPmApi().getTeams()) {
                if (hcTeam.isAsLeastOnlineAnd(PlayerState.PLAYING)) {
                    onlineTeams.add(hcTeam);
                }
            }
            HCTeam winner = game.getCallbacksApi().getForcedOnlineWinningTeam(onlineTeams);
            game.endGame(winner);
        }
    }

    public void startCountdownEndOfGame() {
        if (countdownEndOfGame == null) {
            if (game.isCountdownEndOfGameEnabled()) {

                updatePlayersScoreboards();

                countdownEndOfGame = game.buildTask("countdown end of game", new HCTask() {

                    private int remainingTime = game.getRemainingTimeBeforeEnd();
                    private int timeElapsed = 0;

                    @Override
                    public void run() {
                        remainingTime = game.decreaseTimeBeforeEnd();
                        timeElapsed++;
                        getCallbacksApi().remainingTimeBeforeEnd(remainingTime, timeElapsed);
                        if (remainingTime == 0) {
                            forceDeclareWinner();
                        }
                    }

                })
                    .addListener(new HCTaskListener() {

                        @EventHandler
                        public void beforeEnd(HCBeforeEndEvent e) {
                            getScheduler().stop();
                        }

                    })
                    .withDelay(20)
                    .withInterval(20)
                    .withIterations(-1)
                    .build();

                countdownEndOfGame.start();
            }
        }

    }

    @Override
    public HCPlayer getLastDamagingPlayerArrowShooter(HCPlayer hcPlayer) {

        EntityDamageByEntityEvent event = getLastDamageByEntityEvent(hcPlayer);
        if (event == null)
            return null;

        Entity entity = getArrowShooterEntityDamager(event);

        if (entity == null)
            return null;

        return getHCPlayer(entity);
    }

    @Override
    public Entity getLastDamagingEntityArrowShooter(HCPlayer hcPlayer) {
        EntityDamageByEntityEvent event = getLastDamageByEntityEvent(hcPlayer);
        if (event == null)
            return null;

        return getArrowShooterEntityDamager(event);
    }

    @Override
    public TNTPrimed getLastDamagingTnt(HCPlayer hcPlayer) {
        EntityDamageByEntityEvent event = getLastDamageByEntityEvent(hcPlayer);
        if (event == null)
            return null;

        return getTntDamager(event);
    }

    @Override
    public Fireball getLastDamagingFireball(HCPlayer hcPlayer) {
        EntityDamageByEntityEvent event = getLastDamageByEntityEvent(hcPlayer);
        if (event == null)
            return null;

        return getFireballDamager(event);
    }

    @Override
    public Entity getLastDamagingEntity(HCPlayer hcPlayer) {
        EntityDamageByEntityEvent event = getLastDamageByEntityEvent(hcPlayer);
        if (event == null)
            return null;

        return event.getDamager();
    }

    /**
     * Get last damager entity for this hcPlayer if relevant
     *
     * @param hcPlayer
     * @return
     */
    private EntityDamageByEntityEvent getLastDamageByEntityEvent(HCPlayer hcPlayer) {
        if (hcPlayer == null || !hcPlayer.isOnline())
            return null;

        EntityDamageEvent event = hcPlayer.getPlayer().getLastDamageCause();

        if (event instanceof EntityDamageByEntityEvent)
            return (EntityDamageByEntityEvent) event;

        return null;
    }

    private Entity getArrowShooterEntityDamager(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Arrow) {
            ProjectileSource source = ((Arrow) event.getDamager()).getShooter();
            if (source instanceof Entity)
                return (Entity) source;
        }
        return null;
    }

    private TNTPrimed getTntDamager(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof TNTPrimed) {
            return (TNTPrimed) event.getDamager();
        }
        return null;
    }

    private Fireball getFireballDamager(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Fireball) {
            return (Fireball) event.getDamager();
        }
        return null;
    }


}
