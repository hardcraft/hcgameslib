package com.gmail.val59000mc.hcgameslib.scoreboards;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class GameScoreboard {

    private HCGameAPI api;
    private SimpleScoreboard sc;
    private HCPlayer hcPlayer;

    public GameScoreboard(HCGameAPI api, HCPlayer hcPlayer) {
        this.api = api;
        this.hcPlayer = hcPlayer;
        this.sc = new SimpleScoreboard(api.getStringsAPI().get("messages.scoreboard.title", api.getName()).replace("%game%", api.getName()).toString());

        setupScores();
    }

    private void setupScores() {
        Scoreboard scoreboard = sc.getBukkitScoreboard();

        if (api.getConfig().getBoolean("config.scoreboard.life", false)) {
            Objective life = scoreboard.registerNewObjective(ChatColor.DARK_RED + "\u2764", "health", ChatColor.DARK_RED + "\u2764");
            life.setDisplaySlot(DisplaySlot.BELOW_NAME);

            for (HCPlayer hcp : api.getPlayersManagerAPI().getPlayers()) {
                if (hcp.isOnline()) {
                    life.getScore(hcp.getName()).setScore((int) Math.round(hcp.getPlayer().getHealth()));
                }
            }
        }

        if (api.getConfig().getBoolean("config.scoreboard.kills", false)) {
            Objective kills = scoreboard.registerNewObjective("kills", "playerKillCount", "kills");
            kills.setDisplaySlot(DisplaySlot.PLAYER_LIST);

            for (HCPlayer hcp : api.getPlayersManagerAPI().getPlayers()) {
                kills.getScore(hcp.getName()).setScore(hcp.getKills());
            }
        }

    }

    public void updateVanillaTeam(HCTeam hcTeam, Collection<HCPlayer> teammates) {
        Set<String> teammateNames = teammates.stream()
            .map(HCPlayer::getName)
            .collect(Collectors.toSet());
        sc.updateTeam(hcTeam == null ? "none" : hcTeam.getName(), teammateNames);
    }

    public void update() {
        if (hcPlayer.isOnline()) {

            List<String> lines;

            switch (api.getGameState()) {
                case INITIALIZING:
                case LOADING:
                case WAITING:
                case STARTING:
                    lines = api.getCallbacksApi().updateWaitingScoreboard(hcPlayer);
                    break;
                default:
                case PLAYING:
                    lines = api.getCallbacksApi().updatePlayingScoreboard(hcPlayer);
                    break;
                case ENDED:
                    lines = api.getCallbacksApi().updateEndedScoreboard(hcPlayer);
                    break;
            }

            sc.clear();
            sc.addAll(lines);
            sc.draw();
            sc.send(hcPlayer.getPlayer());
        }
    }
}
