package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;

public class HCPlayerStateChangeEvent extends HCEvent{
	 
	private HCPlayer hcPlayer;
	private PlayerState oldState;
	private PlayerState newState;
	
	
	public HCPlayerStateChangeEvent(HCGameAPI api, HCPlayer hcPlayer, PlayerState oldState, PlayerState newState) {
		super(api);
		this.hcPlayer = hcPlayer;
		this.oldState = oldState;
		this.newState = newState;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}

	public PlayerState getOldState(){
		return oldState;
	}

	public PlayerState getNewState(){
		return newState;
	}

}
