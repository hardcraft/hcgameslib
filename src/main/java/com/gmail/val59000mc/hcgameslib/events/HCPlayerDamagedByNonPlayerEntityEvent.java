package com.gmail.val59000mc.hcgameslib.events;

import org.bukkit.entity.Entity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCPlayerDamagedByNonPlayerEntityEvent extends HCEvent{

	private HCPlayer hcDamaged;
	private Entity entity;
	private EntityDamageByEntityEvent wrapped;
	
	public HCPlayerDamagedByNonPlayerEntityEvent(HCGameAPI api, HCPlayer hcDamaged, Entity entity, EntityDamageByEntityEvent wrapped) {
		super(api);
		this.hcDamaged = hcDamaged;
		this.entity = entity;
		this.wrapped = wrapped;
	}

	public HCPlayer getHcDamaged() {
		return hcDamaged;
	}

	public Entity getEntity() {
		return entity;
	}

	public EntityDamageByEntityEvent getWrapped() {
		return wrapped;
	}
	
	



}
