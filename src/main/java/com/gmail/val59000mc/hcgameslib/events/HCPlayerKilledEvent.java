package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

/**
 * Fired when a player is killed by any other reason than an ennemy player
 * @author Valentin
 *
 */
public class HCPlayerKilledEvent extends HCEvent{

	private HCPlayer hcPlayer;
	
	public HCPlayerKilledEvent(HCGameAPI api, HCPlayer hcPlayer) {
		super(api);
		this.hcPlayer = hcPlayer;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}
}
