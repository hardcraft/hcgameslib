package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.GameState;

public class HCGameStateChangeEvent extends HCEvent{
	 
	private GameState oldState;
	private GameState newState;
	
	
	public HCGameStateChangeEvent(HCGameAPI api, GameState oldState, GameState newState) {
		super(api);
		this.oldState = oldState;
		this.newState = newState;
	}


	public GameState getOldState(){
		return oldState;
	}

	public GameState getNewState(){
		return newState;
	}

}
