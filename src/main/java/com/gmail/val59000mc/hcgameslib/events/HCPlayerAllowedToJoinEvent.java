package com.gmail.val59000mc.hcgameslib.events;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;

public class HCPlayerAllowedToJoinEvent extends HCEvent{

	private Player player;
	private boolean allowed;
	public HCPlayerAllowedToJoinEvent(HCGameAPI api, Player player, boolean allowed) {
		super(api);
		this.player = player;
		this.allowed = allowed;
	}
	public void setAllowedToJoin(boolean allowed) {
		this.allowed = allowed;
	}
	public Player getPlayer() {
		return player;
	}
	public boolean isAllowedToJoin() {
		return allowed;
	}
	
	
	
	 
}
