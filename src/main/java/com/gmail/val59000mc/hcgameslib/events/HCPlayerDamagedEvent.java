package com.gmail.val59000mc.hcgameslib.events;

import org.bukkit.event.entity.EntityDamageEvent;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCPlayerDamagedEvent extends HCEvent{

	private HCPlayer hcDamaged;
	private EntityDamageEvent wrapped;
	
	public HCPlayerDamagedEvent(HCGameAPI api, HCPlayer hcDamaged, EntityDamageEvent wrapped) {
		super(api);
		this.hcDamaged = hcDamaged;
		this.wrapped = wrapped;
	}

	public HCPlayer getHcDamaged() {
		return hcDamaged;
	}

	public EntityDamageEvent getWrapped() {
		return wrapped;
	}
	
	

}
