package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

/**
 * This event is fired when a player is rewarded money thorugh the player managers api
 * It allows listeners to add an extra bonus multiplier based on various conditions
 *
 */
public class HCPlayerRewardBonusMultiplierEvent extends HCEvent{

	private HCPlayer hcPlayer;
	private double multiplier;
	
	public HCPlayerRewardBonusMultiplierEvent(HCGameAPI api, HCPlayer hcPlayer) {
		super(api);
		this.hcPlayer = hcPlayer;
		this.multiplier = 0;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}

	public double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}

	public void addMultiplier(double multiplier) {
		this.multiplier += multiplier;
	}
	
	
}
