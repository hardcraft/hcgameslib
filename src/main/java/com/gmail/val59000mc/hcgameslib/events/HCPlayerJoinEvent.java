package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCPlayerJoinEvent extends HCEvent{

	private HCPlayer hcPlayer;
	
	public HCPlayerJoinEvent(HCGameAPI api, HCPlayer hcPlayer) {
		super(api);
		this.hcPlayer = hcPlayer;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}
	
}
