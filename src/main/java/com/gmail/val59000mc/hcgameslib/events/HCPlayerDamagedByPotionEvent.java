package com.gmail.val59000mc.hcgameslib.events;

import org.bukkit.event.entity.PotionSplashEvent;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCPlayerDamagedByPotionEvent extends HCEvent{

	private HCPlayer hcDamaged;
	private HCPlayer hcDamager;
	private PotionSplashEvent wrapped;
	
	public HCPlayerDamagedByPotionEvent(HCGameAPI api, HCPlayer hcDamaged, HCPlayer hcDamager, PotionSplashEvent wrapped) {
		super(api);
		this.hcDamaged = hcDamaged;
		this.hcDamager = hcDamager;
		this.wrapped = wrapped;
	}

	public HCPlayer getHcDamaged() {
		return hcDamaged;
	}

	public HCPlayer getHcDamager() {
		return hcDamager;
	}

	public PotionSplashEvent getWrapped() {
		return wrapped;
	}

}
