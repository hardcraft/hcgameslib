package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

/**
 * This class specifies that a player has killed a player of a different team
 * Either both player has a team and the two teams are different
 * Or at least one of the two players doesn't have a team
 * This event is never fired when a player kills a player of his own team
 * @author Valentin
 *
 */
public class HCPlayerKilledByPlayerEvent extends HCEvent{

	private HCPlayer hcKilled;
	private HCPlayer hcKiller;
	
	public HCPlayerKilledByPlayerEvent(HCGameAPI api, HCPlayer hcKilled, HCPlayer hcKiller) {
		super(api);
		this.hcKilled = hcKilled;
		this.hcKiller = hcKiller;
	}


	public HCPlayer getHcKiller() {
		return hcKiller;
	}
	

	public HCPlayer getHCKilled() {
		return hcKilled;
	}

}
