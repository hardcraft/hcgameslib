package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;

public class HCPlayerHasLeftTeamEvent extends HCEvent{

	private HCPlayer hcPlayer;
	private HCTeam hcTeam;
	
	public HCPlayerHasLeftTeamEvent(HCGameAPI api, HCTeam hcTeam, HCPlayer hcPlayer) {
		super(api);
		this.hcPlayer = hcPlayer;
		this.hcTeam = hcTeam;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}

	public HCTeam getHcTeam() {
		return hcTeam;
	}	
	
}
