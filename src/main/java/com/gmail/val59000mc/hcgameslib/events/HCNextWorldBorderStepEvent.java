package com.gmail.val59000mc.hcgameslib.events;

import org.bukkit.event.Cancellable;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.worlds.WorldBorderStep;

public class HCNextWorldBorderStepEvent extends HCEvent implements Cancellable{
	
	private WorldBorderStep step;
	private boolean cancelled;
	
	public HCNextWorldBorderStepEvent(HCGameAPI api, WorldBorderStep step) {
		super(api);
		this.step = step;
		this.cancelled = false;
	}

	public WorldBorderStep getStep() {
		return step;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}

}
