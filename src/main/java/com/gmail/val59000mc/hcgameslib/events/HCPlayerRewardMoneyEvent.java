package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

/**
 * This event is fired when a player is rewarded money thorugh the player managers api
 * It allows to change the total rewarded money amount
 * It is fired after {@link HCPlayerRewardBonusMultiplierEvent} and overrites the final amount
 *
 */
public class HCPlayerRewardMoneyEvent extends HCEvent{

	private HCPlayer hcPlayer;
	private Double money;
	
	public HCPlayerRewardMoneyEvent(HCGameAPI api, HCPlayer hcPlayer, Double money) {
		super(api);
		this.hcPlayer = hcPlayer;
		this.money = money;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}
	
	public void addMoney(double amount){
		this.money += amount;
	}
	
	
	
}
