package com.gmail.val59000mc.hcgameslib.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

/**
 * This event is fired every time a player dies
 * when the game is playing
 * @author Valentin
 *
 */
public class HCPlayerDeathEvent extends HCEvent{

	private HCPlayer hcPlayer;
	
	public HCPlayerDeathEvent(HCGameAPI api, HCPlayer hcPlayer) {
		super(api);
		this.hcPlayer = hcPlayer;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}
}
