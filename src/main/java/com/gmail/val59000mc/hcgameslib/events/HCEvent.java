package com.gmail.val59000mc.hcgameslib.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCBungeeAPI;
import com.gmail.val59000mc.hcgameslib.api.HCDependenciesAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCItemsAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.api.HCPlayersManagerAPI;
import com.gmail.val59000mc.hcgameslib.api.HCPluginCallbacksAPI;
import com.gmail.val59000mc.hcgameslib.api.HCSoundAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;

public abstract class HCEvent extends Event{


	private static final HandlerList handlers = new HandlerList();
	
	private HCGameAPI api;
	
	public HCEvent(HCGameAPI api){
		this.api = api;
	}
	
	protected HCGameAPI getApi(){
		return api;
	}

	protected HCPlayersManagerAPI getPmApi(){
		return api.getPlayersManagerAPI();
	}

	protected HCStringsAPI getStringsApi(){
		return api.getStringsAPI();
	}

	protected HCBungeeAPI getBungeeApi(){
		return api.getBungeeAPI();
	}

	protected HCPluginCallbacksAPI getCallbacksApi(){
		return api.getCallbacksApi();
	}

	protected HCDependenciesAPI getDependenciesApi(){
		return api.getDependencies();
	}

	protected HCItemsAPI getItemsApi(){
		return api.getItemsAPI();
	}

	protected HCSoundAPI getSoundApi(){
		return api.getSoundAPI();
	}

	protected HCMySQLAPI getMySQLAPI(){
		return api.getMySQLAPI();
	}

	protected JavaPlugin getPlugin(){
		return api.getPlugin();
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
        return handlers;
    }
}
