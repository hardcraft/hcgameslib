package com.gmail.val59000mc.hcgameslib.database;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCPlayerPersistSessionEvent extends HCEvent{

	/**
	 * This is a temporary hcPlayer not tracked by the plaeyr manager
	 * It stores the difference of incremental statistics between now and the last time the actual player was persisted
	 * To get the actual player, one must use getPmApi().getHCPlayer(uuid)
	 */
	private HCPlayer hcPlayer;
	
	public HCPlayerPersistSessionEvent(HCGameAPI api, HCPlayer hcPlayer) {
		super(api);
		this.hcPlayer = hcPlayer;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}

	
}
