package com.gmail.val59000mc.hcgameslib.database;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCPlayerDBInsertedEvent extends HCEvent{

	private HCPlayer hcPlayer;
	
	public HCPlayerDBInsertedEvent(HCGameAPI api, HCPlayer hcPlayer) {
		super(api);
		this.hcPlayer = hcPlayer;
	}

	public HCPlayer getHcPlayer() {
		return hcPlayer;
	}

	
}
