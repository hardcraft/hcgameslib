package com.gmail.val59000mc.hcgameslib.database;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;

public class HCStatsMonthChangeEvent extends HCEvent{

	private String month;
	private String year;
	
	public HCStatsMonthChangeEvent(HCGameAPI api, String newMonth, String newYear) {
		super(api);
		this.month = newMonth;
		this.year = newYear;
	}

	public String getMonth() {
		return month;
	}

	public String getYear() {
		return year;
	}
	
	
}
