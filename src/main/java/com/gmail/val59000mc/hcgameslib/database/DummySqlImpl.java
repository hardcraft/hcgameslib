package com.gmail.val59000mc.hcgameslib.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;

public class DummySqlImpl implements HCMySQLAPI{

	@Override
	public boolean isEnabled() {
		return false;
	}

	@Override
	public String readQueryFromResource(JavaPlugin plugin, String resourceName) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet query(PreparedStatement preparedStatement) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet executeWithGeneratedIds(final PreparedStatement preparedStatement){
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public int execute(PreparedStatement preparedStatement) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public PreparedStatement prepareStatement(String query, Object... vars) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public void handleException(SQLException e) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet selectGameByName(String name) {
		throw new IllegalArgumentException("mysql is disabled");
	}
	
	@Override
	public CachedRowSet selectPlayerByUUID(String uuid) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet selectPlayerById(int id) throws SQLException {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet selectPlayerByName(String name) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet selectMultiplePlayersByUUID(List<String> uuid) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public void refreshMonthAndYear() {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public String getMonth() {
		return "1";
	}

	@Override
	public String getYear() {
		return "1970";
	}

	@Override
	public String selectGlobalConfig(String key) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public void writeGlobalConfig(String key, String value) {
		throw new IllegalArgumentException("mysql is disabled");
	}


}
