package com.gmail.val59000mc.hcgameslib.stuff;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

public class Stuff {

	private String name;
	private List<ItemStack> inventory;
	private List<ItemStack> armor;
	
	public Stuff(String name, List<ItemStack> inventory, List<ItemStack> armor){
		this.name = name;
		this.inventory = inventory;
		this.armor = armor;
	}

	public String getName() {
		return name;
	}

	public List<ItemStack> getInventory() {
		return inventory;
	}

	public List<ItemStack> getArmor() {
		return armor;
	}
	
	public static class Builder{
		private String name;
		private List<ItemStack> inventory;
		private List<ItemStack> armor;
		
		public Builder(String name){
			this.name = name;
			this.inventory = new ArrayList<ItemStack>();
			this.armor = new ArrayList<ItemStack>();
		}
		
		public Builder addInventoryItem(ItemStack item){
			this.inventory.add(item);
			return this;
		}
		
		public Builder addInventoryItems(List<ItemStack> items){
			this.inventory.addAll(items);
			return this;
		}
		
		public Builder addInventoryItems(ItemStack[] items){
			this.inventory.addAll(Lists.newArrayList(items));
			return this;
		}
		
		/**
		 * From head to foot (4 items)
		 * @param item
		 * @return
		 */
		public Builder addArmorItem(ItemStack item){
			this.armor.add(item);
			return this;
		}
		
		/**
		 * From head to foot (4 items)
		 * @param items
		 * @return
		 */
		public Builder addArmorItems(List<ItemStack> items){
			this.armor.addAll(items);
			return this;
		}
		
		/**
		 * From head to foot (4 items)
		 * @param items
		 * @return
		 */
		public Builder addArmorItems(ItemStack[] items){
			this.armor.addAll(Lists.newArrayList(items));
			return this;
		}
		
		public Stuff build(){
			return new Stuff(this.name, this.inventory, this.armor);
		}
	}
	
	
}
