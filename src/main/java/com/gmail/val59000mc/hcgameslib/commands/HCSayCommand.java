package com.gmail.val59000mc.hcgameslib.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class HCSayCommand extends HCCommand{

	// permission = hardcraftpvp.say
	public HCSayCommand(JavaPlugin plugin) {
		super(plugin, "say");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
			
		getStringsApi().getRaw(String.join(" ", args)).sendChatP();
		return true;
	}

}
