package com.gmail.val59000mc.hcgameslib.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;

/**
 * Allows a spec to join the game while the game is running
 *
 */
public class HCPlayCommand extends HCCommand{
	
	public HCPlayCommand(JavaPlugin plugin) {
		super(plugin, "play");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;

			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			
			if(hcPlayer != null){
				
				if(getPmApi().isJoinTeamWhilePlayingAllowed()){
					if(getAPi().is(GameState.PLAYING) && (hcPlayer.is(PlayerState.DEAD) || hcPlayer.is(PlayerState.SPECTATING))){
						
						getCallbacksApi().performPlayCommand(hcPlayer);
						
					}else{
						getStringsApi().get("messages.command.play.cannot-use-now").sendChatP(hcPlayer);
					}
				}else{
					getStringsApi().get("messages.command.play.not-enabled").sendChatP(hcPlayer);
				}
				
			}
			
		}
		return true;
	}

}
