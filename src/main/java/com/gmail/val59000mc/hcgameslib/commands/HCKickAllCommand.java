package com.gmail.val59000mc.hcgameslib.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.events.HCKickAllEvent;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCKickAllCommand extends HCCommand{

	// permission = hardcraftpvp.kickall
	public HCKickAllCommand(JavaPlugin plugin) {
		super(plugin, "kickall");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
			
		boolean isBungee = getAPi().getConfig().getBoolean("config.bungee.enabled");
		String server = getAPi().getConfig().getString("config.bungee.server",Constants.DEFAULT_LOBBY_SERVER);
		
		HCMessage kickMessage = isBungee 
			? getStringsApi().get("messages.command.kickall.to-lobby")
			: getStringsApi().get("messages.command.kickall.from-server");

		Bukkit.getPluginManager().callEvent(new HCKickAllEvent(getAPi()));
			
		for(HCPlayer hcPlayer : getPmApi().getPlayers(true, null)){
			if(isBungee){
				kickMessage.sendChatP(hcPlayer);
				getBungeeApi().sendToServer(hcPlayer,server);
			}else{
				hcPlayer.getPlayer().kickPlayer(kickMessage.toString());
			}
		}
		
		return true;
	}

}
