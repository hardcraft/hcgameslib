package com.gmail.val59000mc.hcgameslib.commands;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.events.HCGameStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;

public class HCVanishCommand extends HCCommand{
	
	public HCVanishCommand(JavaPlugin plugin) {
		super(plugin, "vanish");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;
			
			if(player.hasPermission("hardcraftpvp.set-vanished")){

				HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
				
				if(hcPlayer != null){
					
					switch(getAPi().getGameState()){
						case INITIALIZING:
						case LOADING:
						case ENDED:
						case STARTING:
							 // no vanish at this state
							getStringsApi()
								.get("messages.command.vanish.not-allowed-now")
								.sendChatP(hcPlayer);
							return true;
						case WAITING:
						case PLAYING:
						case CONTINUOUS:
							// allow vanish at this state
							break;
					}
					
					if(hcPlayer.is(PlayerState.VANISHED)){
						getStringsApi()
							.get("messages.command.vanish.already-vanish")
							.sendChatP(hcPlayer);
						return true;
					}
					
					HCTypeVanishCommandEvent typeCommandEvent = new HCTypeVanishCommandEvent(getAPi(), hcPlayer);
					Bukkit.getPluginManager().callEvent(typeCommandEvent);
					if(!typeCommandEvent.isCancelled()){
						
						getAPi().buildTask("command vanish for "+hcPlayer.getName(), new HCTask() {
							
							private int countdown = 5;
							
							@Override
							public void run() {
								
								getStringsApi()
									.get("messages.command.vanish.countdown-title")
									.replace("%time%", String.valueOf(countdown))
									.sendTitle(hcPlayer, 0, 21, 0);
								getSoundApi().play(hcPlayer, Sound.BLOCK_NOTE_BLOCK_SNARE, 1, 2);
								
								countdown--;
								
							}
							
						})
						.withIterations(5)
						.withLastCallback(new HCTask() {
							
							@Override
							public void run() {
								stop();
								PlayersManager pm = (PlayersManager) getAPi().getPlayersManagerAPI();
								pm.removePlayerFromTeamSilently(hcPlayer);
								pm.vanishPlayer(hcPlayer);
								pm.refreshVisiblePlayers(hcPlayer);
								pm.refreshRankNameTag(hcPlayer,0);
								pm.checkIfLastTeamOnline();
							}	
						}).addListener(new HCTaskListener(){
							
							@EventHandler
							public void onCancelVanish(HCCancelVanishTimeoutEvent e){
								if(e.getHcPlayer().equals(hcPlayer)){
									cancelVanish(hcPlayer);
								}
							}
							
							@EventHandler
							public void onGameStateChange(HCGameStateChangeEvent e){
								cancelVanish(hcPlayer);
							}
							
							@EventHandler
							public void onTypeCommandAgain(HCTypeVanishCommandEvent e){
								if(e.getHcPlayer().equals(hcPlayer)){
									cancelVanish(hcPlayer);
									e.setCancelled(true);
								}
							}

							@EventHandler
							public void onPlayerStateGame(HCPlayerStateChangeEvent e){
								if(e.getHcPlayer().equals(hcPlayer)){
									cancelVanish(hcPlayer);
								}
							}

							@EventHandler
							public void onQuit(HCPlayerQuitEvent e){
								if(e.getHcPlayer().equals(hcPlayer)){
									cancelVanish(hcPlayer);
								}
							}
							
							private void cancelVanish(HCPlayer hcPlayer){
								getScheduler().stop();
								getStringsApi()
									.get("messages.command.vanish.cancel-timeout")
									.sendChatP(hcPlayer);
							}
							
						})
						.build()
						.start();
						
					}
					
				}
				
			}
		}
		return true;
	}

	private class HCCancelVanishTimeoutEvent extends HCEvent{

		private HCPlayer hcPlayer;

		public HCCancelVanishTimeoutEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}
		
	}
	
	private class HCTypeVanishCommandEvent extends HCEvent implements Cancellable{

		private HCPlayer hcPlayer;
		private boolean isCancelled;

		public HCTypeVanishCommandEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}

		@Override
		public boolean isCancelled() {
			return isCancelled;
		}

		@Override
		public void setCancelled(boolean cancel) {
			this.isCancelled = cancel;
		}
		
	}

}
