package com.gmail.val59000mc.hcgameslib.commands;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.events.HCGameStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;

public class HCSpecCommand extends HCCommand{
	
	public HCSpecCommand(JavaPlugin plugin) {
		super(plugin, "spec");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;

			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			
			if(hcPlayer != null){
				
				switch(getAPi().getGameState()){
					case INITIALIZING:
					case LOADING:
					case ENDED:
					case STARTING:
					case WAITING:
						 // no vanish at this state
						getStringsApi()
							.get("messages.command.spec.not-allowed-now")
							.sendChatP(hcPlayer);
						return true;
					case PLAYING:
					case CONTINUOUS:
						// allow vanish at this state
						break;
				}
				
				if(hcPlayer.is(PlayerState.SPECTATING) || hcPlayer.is(PlayerState.DEAD)){
					getStringsApi()
						.get("messages.command.spec.already-spec")
						.sendChatP(hcPlayer);
					return true;
				}
				
				HCTypeSpecCommandEvent typeCommandEvent = new HCTypeSpecCommandEvent(getAPi(), hcPlayer);
				Bukkit.getPluginManager().callEvent(typeCommandEvent);
				if(!typeCommandEvent.isCancelled()){
					
					getAPi().buildTask("command spec for "+hcPlayer.getName(), new HCTask() {
						
						private int countdown = 5;
						
						@Override
						public void run() {
							
							if(!getCallbacksApi().isAllowedToPerformSpecCommand(hcPlayer)){
								Bukkit.getPluginManager().callEvent(new HCCancelSpecTimeoutEvent(getAPi(),hcPlayer));
							}else{
								getStringsApi()
									.get("messages.command.spec.countdown-title")
									.replace("%time%", String.valueOf(countdown))
									.sendTitle(hcPlayer, 0, 21, 0);
								getSoundApi().play(hcPlayer, Sound.BLOCK_NOTE_BLOCK_SNARE, 1, 2);
								
								countdown--;
							}
							
							
						}
						
					})
					.withIterations(5)
					.withLastCallback(new HCTask() {
						
						@Override
						public void run() {
							stop();
							PlayersManager pm = (PlayersManager) getAPi().getPlayersManagerAPI();
							if(hcPlayer.is(PlayerState.PLAYING)){
								getStringsApi()
									.get("messages.command.spec.broadcast")
									.replace("%player%", hcPlayer.getColoredName())
									.sendChatP();
							}
							pm.spectatePlayer(hcPlayer);
							pm.refreshVisiblePlayers(hcPlayer);
							pm.refreshRankNameTag(hcPlayer,0);
							pm.checkIfLastTeamOnline();
						}
						
					}).addListener(new HCTaskListener(){
						
						@EventHandler
						public void onCancelSpec(HCCancelSpecTimeoutEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpec(hcPlayer);
							}
						}
						
						@EventHandler
						public void onGameStateChange(HCGameStateChangeEvent e){
							cancelSpec(hcPlayer);
						}
						
						@EventHandler
						public void onTypeCommandAgain(HCTypeSpecCommandEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpec(hcPlayer);
								e.setCancelled(true);
							}
						}

						@EventHandler
						public void onPlayerStateGame(HCPlayerStateChangeEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpec(hcPlayer);
							}
						}

						@EventHandler
						public void onQuit(HCPlayerQuitEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpec(hcPlayer);
							}
						}
						
						private void cancelSpec(HCPlayer hcPlayer){
							getScheduler().stop();
							getStringsApi()
								.get("messages.command.spec.cancel-timeout")
								.sendChatP(hcPlayer);
						}
						
					})
					.build()
					.start();

				}
				
			}
			
		}
		return true;
	}

	private class HCCancelSpecTimeoutEvent extends HCEvent{

		private HCPlayer hcPlayer;

		public HCCancelSpecTimeoutEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}
		
	}
	
	private class HCTypeSpecCommandEvent extends HCEvent implements Cancellable{

		private HCPlayer hcPlayer;
		private boolean isCancelled;

		public HCTypeSpecCommandEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}

		@Override
		public boolean isCancelled() {
			return isCancelled;
		}

		@Override
		public void setCancelled(boolean cancel) {
			this.isCancelled = cancel;
		}
		
	}

}
