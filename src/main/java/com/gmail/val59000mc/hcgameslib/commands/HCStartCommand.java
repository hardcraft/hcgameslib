package com.gmail.val59000mc.hcgameslib.commands;

import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class HCStartCommand extends HCCommand {

    private static final int DEFAULT_SECONDS_BEFORE_START = 10;

    public HCStartCommand(JavaPlugin plugin) {
        super(plugin, "start");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {

            Player player = (Player) sender;

            HCPlayer hcPlayer = getPmApi().getHCPlayer(player);

            int secondsBeforeStart = DEFAULT_SECONDS_BEFORE_START;

            if (args.length > 0) {
                try {
                    secondsBeforeStart = Integer.parseInt(args[0]);
                    if (secondsBeforeStart < 0) {
                        throw new NumberFormatException();
                    }
                } catch (NumberFormatException e) {
                    getStringsApi().get("messages.command.start.syntax-error").sendChat(hcPlayer);
                    return true;
                }
            }

            if (hcPlayer != null) {

                switch (getAPi().getGameState()) {
                    case WAITING:
                        if (Bukkit.getOnlinePlayers().size() < 2) {
                            getStringsApi().get("messages.command.start.not-enough-players").sendChat(hcPlayer);
                        } else {
                            getPmApi().startStartingTask(secondsBeforeStart);
                            ((HCGame) getAPi()).setRemainingTimeBeforeStart(secondsBeforeStart);
                            getStringsApi().get("messages.command.start.forcing-game-start").sendChat(hcPlayer);
                        }
                        break;
                    default:
                        getStringsApi().get("messages.command.start.game-already-running").sendChat(hcPlayer);
                        break;
                }
            }
        }
        return true;
    }

}
