package com.gmail.val59000mc.hcgameslib.commands;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCBoostersAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.boosters.Booster;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;

public class HCBoosterCommand extends HCCommand{

	private HCBoostersAPI boosters;
	private Map<UUID,Long> rateLimiter; // store last time a player saw his boosters to prevent mysql spam
	
	// permission = hardcraftpvp.say
	public HCBoosterCommand(JavaPlugin plugin) {
		super(plugin, "booster");
		this.rateLimiter = new HashMap<>();
	}
	
	@Override
	public void onRegister(HCGameAPI api){
		this.boosters = api.getBoostersAPI();
		
		api.buildTask("clear rate limiter", new HCTask(){

			@Override
			public void run() {
				clearRateLimiter();
			}

			private void clearRateLimiter() {
				Long now = System.currentTimeMillis();
				synchronized (rateLimiter) {
					Iterator<Map.Entry<UUID,Long>> it = rateLimiter.entrySet().iterator();
					while(it.hasNext()){
						Entry<UUID,Long> entry = it.next();
						if(now-entry.getValue() > 5000){
							it.remove();
						}
					}
				}
				
			}
		})
		.withDelay(12000) // 10 minutes
		.withIterations(-1)
		.withInterval(12000)
		.build()
		.start();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if(args.length == 0){
			commandBooster(sender);
		}else if(args.length > 0){
			switch(args[0]){
				case "create":
					commandBoosterCreate(sender,args);
					break;
				case "remove":
					commandBoosterRemove(sender,args);
					break;
				case "activate":
					commandBoosterActivate(sender,args);
					break;
				default:
					commandBoosterOther(sender, args);
					break;
			}
		}
		
		return true;
	}

	private void commandBooster(CommandSender sender) {
		if(checkPlayer(sender)){
			HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) sender);
			if(hcPlayer == null || hcPlayer.getId() == 0) return;
			if(checkRateLimit(hcPlayer)){
				getAPi().async(()->boosters.findPlayerCurrentBoosters(hcPlayer.getId()), (response)->{
					if(response.isSuccess()){
						boosters.displayBoostersTo(hcPlayer.getName(), hcPlayer, response.getBoosters());
					}else{
						response.getMessage().sendChatP(hcPlayer);
					}
				});
			}
		}
	}
	
	private void commandBoosterOther(CommandSender sender, String[] args) {
		if(checkAdminPermission(sender) && checkPlayer(sender)){
			HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) sender);
			if(hcPlayer == null || hcPlayer.getId() == 0) return;
			if(checkRateLimit(hcPlayer)){
				if(args.length == 1){
					String owner = args[0];
					getAPi().async(()->boosters.findPlayerCurrentBoostersByPlayerName(owner), (response)->{
						if(response.isSuccess()){
							boosters.displayBoostersTo(owner, hcPlayer, response.getBoosters());
						}else{
							response.getMessage().sendChatP(hcPlayer);
						}
					});
				}else{
					getMessage("messages.command.boosters.syntax-error-other");
				}
			}
		}
	}

	private void commandBoosterCreate(CommandSender sender, String[] args) {
		if(checkAdminPermission(sender)){
			if(args.length == 4){
				try{
					String playerName = args[1];
					double multiplier = Double.parseDouble(args[2]);
					int minutesDuration = Integer.parseInt(args[3]);
					getAPi().async(()->boosters.createBooster(playerName, multiplier, minutesDuration), (response)->{
						sender.sendMessage(response.getMessage().toString());
					});
				}catch(Exception e){
					syntaxErrorCreate(sender);
				}
			}else{
				syntaxErrorCreate(sender);
			}
		}
	}

	private void commandBoosterRemove(CommandSender sender, String[] args) {
		if(checkAdminPermission(sender)){
			if(args.length == 2){
				try{
					int id = Integer.parseInt(args[1]);
					getAPi().async(()->boosters.removeBooster(id), (response)->{
						sender.sendMessage(response.getMessage().toString());
					});
				}catch(Exception e){
					syntaxErrorRemove(sender);
				}
			}else{
				syntaxErrorRemove(sender);
			}
		}
	}

	private void commandBoosterActivate(CommandSender sender, String[] args) {
		if(checkAdminPermission(sender)){
			if(args.length == 2){
				try{
					int id = Integer.parseInt(args[1]);
					getAPi().async(()->boosters.findBoosterById(id), (response)->{
						if(response.isSuccess()){
							Booster booster = response.getBooster();
							getAPi().async(()->boosters.activateBooster(booster), (activationResponse)->{
								sender.sendMessage(activationResponse.getMessage().toString());
								if(activationResponse.isSuccess()){
									HCPlayer hcPlayer = getPmApi().getHCPlayer(booster.getPlayerUUID());
									if(hcPlayer != null){
										boosters.loadActiveBooster(hcPlayer);
									}
								}
							});
						}else{
							sender.sendMessage(response.getMessage().toString());
						}
					});
				}catch(Exception e){
					syntaxErrorActivate(sender);
				}
			}else{
				syntaxErrorActivate(sender);
			}
		}
	}
	
	private boolean checkAdminPermission(CommandSender sender){
		if(!sender.hasPermission("hardcraftpvp.boosters.admin")){
			sender.sendMessage(getMessage("messages.command.boosters.no-permission").toString());
			return false;
		}
		return true;
	}
	
	private boolean checkPlayer(CommandSender sender){
		if(!(sender instanceof Player)){
			sender.sendMessage(getMessage("messages.command.boosters.must-run-as-player").toString());
			return false;
		}
		return true;
	}
	
	private boolean checkRateLimit(HCPlayer hcPlayer){
		// allow up to fetch booster every 5s
		Long now = System.currentTimeMillis();
		Long lastUsed = rateLimiter.get(hcPlayer.getUuid());
		if(lastUsed != null && now-lastUsed < 5000){
			getMessage("messages.boosters.wait-a-few-seconds").sendChatP(hcPlayer);
			return false;
		}
		rateLimiter.put(hcPlayer.getUuid(), now);
		return true;
	}

	private void syntaxErrorCreate(CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.boosters.syntax-error-create").toString());
	}
	
	private void syntaxErrorRemove(CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.boosters.syntax-error-remove").toString());
	}
	
	private void syntaxErrorActivate(CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.boosters.syntax-error-activate").toString());
	}

	private HCMessage getMessage(String code){
		return getStringsApi().get(code);
	}

}
