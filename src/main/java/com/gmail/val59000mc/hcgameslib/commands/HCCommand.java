package com.gmail.val59000mc.hcgameslib.commands;

import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCBungeeAPI;
import com.gmail.val59000mc.hcgameslib.api.HCDependenciesAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCItemsAPI;
import com.gmail.val59000mc.hcgameslib.api.HCPlayersManagerAPI;
import com.gmail.val59000mc.hcgameslib.api.HCPluginCallbacksAPI;
import com.gmail.val59000mc.hcgameslib.api.HCSoundAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;

public abstract class HCCommand implements CommandExecutor{

	private String name;
	private HCGameAPI api;
	private JavaPlugin plugin;
	
	public void onRegister(HCGameAPI api){
		// override to do something with
	}
	
	public HCCommand(JavaPlugin plugin,String name){
		this.plugin = plugin;
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public JavaPlugin getPlugin() {
		return plugin;
	}

	public void setApi(HCGameAPI api) {
		this.api = api;
	}

	protected HCGameAPI getAPi(){
		return api;
	}

	protected HCPlayersManagerAPI getPmApi(){
		return api.getPlayersManagerAPI();
	}
	
	protected HCStringsAPI getStringsApi(){
		return api.getStringsAPI();
	}
	
	protected HCBungeeAPI getBungeeApi(){
		return api.getBungeeAPI();
	}
	
	protected HCPluginCallbacksAPI getCallbacksApi(){
		return api.getCallbacksApi();
	}

	protected HCDependenciesAPI getDependenciesApi(){
		return api.getDependencies();
	}

	protected HCItemsAPI getItemsApi(){
		return api.getItemsAPI();
	}

	protected HCSoundAPI getSoundApi(){
		return api.getSoundAPI();
	}
	
}
