package com.gmail.val59000mc.hcgameslib.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCWhitelistCommand extends HCCommand{

	public HCWhitelistCommand(JavaPlugin plugin) {
		super(plugin, "wlist");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			boolean newState = !getAPi().getConfig().getBoolean("config.whitelist",Constants.DEFAULT_WHITELIST_ENABLE);
			getAPi().getConfig().set("config.whitelist", newState);
			getStringsApi()
				.get("messages.command.wlist.change-state")
				.replace("%state%", newState ? "ON" : "OFF")
				.sendChatP(getPmApi().getHCPlayer((Player) sender));
			getAPi().saveConfig();
			
			if(newState){
				// inform and kick all player who don't have the required permissions to stay
				getStringsApi().get("messages.command.wlist.activated-kick-message").sendChatP();
				
				boolean isBungee = getAPi().getConfig().getBoolean("config.bungee.enabled");
				String server = getAPi().getConfig().getString("config.bungee.server",Constants.DEFAULT_LOBBY_SERVER);
				
					for(HCPlayer hcPlayer : getPmApi().getPlayers(true, null)){
						Player player = hcPlayer.getPlayer();
						if(!player.hasPermission("hardcraftpvp.whitelist."+getAPi().getLowercaseName())){

							if(isBungee){
								getBungeeApi().sendToServer(hcPlayer,server);
							}else{
								player.kickPlayer("");
							}
							
						}
					}
			}
		}
		return false;
	}

}
