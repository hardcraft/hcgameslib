package com.gmail.val59000mc.hcgameslib.commands;

import com.gmail.val59000mc.hcgameslib.api.HCDonationsAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.donations.Donation;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;
import java.util.Locale;

public class HCDonationCommand extends HCCommand{

	private HCDonationsAPI donations;
	private DateTimeFormatter dateTimeFormat = new DateTimeFormatterBuilder().appendPattern("d MMM YYYY à HH:mm").toFormatter(Locale.FRANCE);

	public HCDonationCommand(JavaPlugin plugin) {
		super(plugin, "donation");
	}

	@Override
	public void onRegister(HCGameAPI api){
		this.donations = api.getDonationsAPI();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if(args.length == 0){
			commandDonationInfo(sender);
		}else if(args.length > 0){
			switch(args[0]){
				case "create":
					commandDonationCreate(sender,args);
					break;
				case "list":
					commandDonationList(sender, args);
					break;
				default:
					unknownSubCommand(sender);
					break;
			}
		}
		
		return true;
	}

	private void commandDonationInfo(CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.donations.info").toString());
	}


	private void unknownSubCommand(CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.donations.unknown-sub-command").toString());
	}


	private void commandDonationCreate(CommandSender sender, String[] args) {
		if(checkAdminPermission(sender)){
			if(args.length == 2){
				try{
					String playerName = args[1];
					getAPi().async(()->donations.createDonation(playerName), (response)->{
						sender.sendMessage(response.getMessage().toString());

                        // refresh owner of donation if online
                        HCPlayer hcPlayer = getPmApi().getHCPlayer(playerName);
                        if(hcPlayer != null)
                            getAPi().async(()->donations.refreshDonations(hcPlayer));

					});
				}catch(Exception e){
					syntaxErrorCreate(sender);
				}
			}else{
				syntaxErrorCreate(sender);
			}
		}
	}

	private void commandDonationList(CommandSender sender, String[] args) {
		if(args.length == 1){
			if(sender instanceof Player){
				HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) sender);
				getAPi().async(()->donations.findDonations(hcPlayer), (donations)->{
					displayDonationsOfPlayerToCommandSender(hcPlayer.getName(), donations, hcPlayer.getPlayer());
				});
			}else{
				mustBeRunAsPlayer(sender);
			}
		}else if(args.length == 2){
			if(checkAdminPermission(sender)) {
				String playerName = args[1];
				getAPi().async(() -> donations.findDonations(args[1]), (donations) -> {
					displayDonationsOfPlayerToCommandSender(playerName, donations, sender);
				});
			}
		}else{
			syntaxErrorList(sender);
		}
	}

	private void displayDonationsOfPlayerToCommandSender(String playerName, List<Donation> donations, CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.donations.list-header").replace("%player%", playerName).toString());
		if(donations.isEmpty()){
			sender.sendMessage(getMessage("messages.command.donations.list-content-empty").toString());
		}else{
			for(Donation donation : donations){
				String content = getMessage("messages.command.donations.list-content").replace("%date%", formatDate(donation.getDonationDate())).toString();
				sender.sendMessage(ChatColor.WHITE + Constants.STAR_ICON + " " + content);
			}
		}
	}

	private void syntaxErrorCreate(CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.donations.syntax-error-create").toString());
	}

	private void syntaxErrorList(CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.donations.syntax-error-list").toString());
	}


	private void mustBeRunAsPlayer(CommandSender sender) {
		sender.sendMessage(getMessage("messages.command.must-run-as-player").toString());
	}
	
	private boolean checkAdminPermission(CommandSender sender){
		if(!sender.hasPermission("hardcraftpvp.donations.admin")){
			sender.sendMessage(getMessage("messages.command.no-permission").toString());
			return false;
		}
		return true;
	}

	private String formatDate(ZonedDateTime date){
		return date.withZoneSameInstant(ZoneId.of("Europe/Paris")).format(dateTimeFormat);
	}

	private HCMessage getMessage(String code){
		return getStringsApi().get(code);
	}

}
