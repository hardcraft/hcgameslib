package com.gmail.val59000mc.hcgameslib.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class HCChatCommand extends HCCommand{

	public HCChatCommand(JavaPlugin plugin) {
		super(plugin, "chat");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) sender);
			
			if(hcPlayer != null){
				hcPlayer.setGlobalChat(!hcPlayer.isGlobalChat());
				getStringsApi().get("messages.command.chat.global-chat."+hcPlayer.isGlobalChat()).sendChatP(hcPlayer);
				return true;
			}
		}
		return false;
	}

}
