package com.gmail.val59000mc.hcgameslib.common;

public class Constants {

	public static final int DEFAULT_WORLD_TIME_TICKS = 6000;
	public static final boolean DEFAULT_REMOVE_LOBBY_AFTER_START = false;
	public static final int DEFAULT_REMOVE_LOBBY_MAX_BLOCKS_PER_SECOND = 250;
	public static final int DEFAULT_REMOVE_LOBBY_TICKS_INTERVAL = 10;
	
	public final static int DEFAULT_MAX_PLAYERS = 20;
	public static final boolean DEFAULT_START_SCHEDULER_ENABLED = true;
	public final static int DEFAULT_MIN_PLAYERS_TO_START = 4;
	public final static int DEFAULT_SECONDS_TO_START = 60;
	public static final int DEFAULT_SECONDS_BEFORE_ELIMINATION = 300;
	
	public static final boolean DEFAULT_IS_CONTINUOUS_GAME = false;
	
	public static final double DEFAULT_MEMBER_RANK_BONUS_PERCENTAGE = 100;
	public static final double DEFAULT_MODERATOR_RANK_BONUS_PERCENTAGE = 200;
	public static final double DEFAULT_ADMINISTRATOR_RANK_BONUS_PERCENTAGE = 200;
	public static final double DEFAULT_HELPER_RANK_BONUS_PERCENTAGE = 200;
	public static final double DEFAULT_BUILDER_RANK_BONUS_PERCENTAGE = 200;
	public static final double DEFAULT_VIP_PLUS_RANK_BONUS_PERCENTAGE = 170;
	public static final double DEFAULT_VIP_RANK_BONUS_PERCENTAGE = 135;
	
	public static double HARDCOINS_MEMBER_BASE = 1;
	public static double HARDCOINS_VIP_BASE = 1.35;
	public static double HARDCOINS_VIP_PLUS_BASE = 1.70;
	public static double HARDCOINS_HELPER_BASE = 2;
	public static double HARDCOINS_COMMUNITY_BASE = 2;
	public static double HARDCOINS_BUILDER_BASE = 2;
	public static double HARDCOINS_MODERATOR_BASE = 2;
	public static double HARDCOINS_ADMINISTRATOR_BASE = 2;
	
	public static final String DEFAULT_LOBBY_SERVER = "Lobby1";
	
	public static final String DEFAULT_STUFF_NAME = "default-stuff";
	
	public static final boolean DEFAULT_DEBUG_STATE = false;
	
	public static final boolean DEFAULT_MYSQL_ENABLED = false;
	
	public static final double DEFAULT_KILL_REWARD = 1;
	public static final double DEFAULT_WIN_REWARD = 20;
	public static final boolean DEFAULT_BUNGEE_ENABLED = false;
	public static final boolean DEFAULT_AUTO_ELIMINATE_ENABLED = true;
	
	
	public static final String DEFAULT_PREVENT_FAR_AWAY_TYPE = "DISTANCE";
	public static final boolean DEFAULT_PREVENT_FAR_AWAY_ENABLED_WAITING = true;
	public static final boolean DEFAULT_PREVENT_FAR_AWAY_ENABLED_PLAYING = true;
	public static final double DEFAULT_PREVENT_FAR_AWAY_WAITING = 10000;
	public static final double DEFAULT_PREVENT_FAR_AWAY_PLAYING = 35000;
	public static final String DEFAULT_PREVENT_FAR_AWAY_MIN = "-150 0 -150 0 0";
	public static final String DEFAULT_PREVENT_FAR_AWAY_MAX = "150 255 150 0 0";
	public static final boolean DEFAULT_PREVENT_BELOW_ZERO = true;
	
	public static boolean DEFAULT_DISABLED_JOIN_AND_QUIT_MESSAGES = true;

	public static final boolean DEFAULT_WIN_IF_LAST_ONLINE_TEAM = true;
	public static final String DEFAULT_MYSQL_GAME_NAME = "hcgameslib";
	
	public static final String DEFAULT_SPECTATOR_INV_NAME = "spec-hcgameslib";
	public static final String DEFAULT_WAITING_INV_NAME = "wait-hcgameslib";
	public static boolean DEFAULT_ENABLE_CHOOSE_TEAM = true;
	public static final String DEFAULT_TEAM_INV_NAME = "team-hcgameslib";
	public static final String DEFAULT_WAIT_INVENTORY_CHOOSE_TEAM_MATERIAL = "DIAMOND_SWORD";
	
	public static final String DEFAULT_SPECTATOR_INV_NAME_PLAYER_PREFIX = "spec-hcgameslib-";

	public static final String STAR_ICON = "\u272A";
	
	public static final boolean DEFAULT_ENABLE_RANKS_NAME_TAGS = true;
	public static final boolean DEFAULT_ENABLE_COLORED_RANKS = false;
	public static final boolean DEFAULT_ENABLE_SCOREBOARDS = true;
	
	public static final long DEFAULT_PERSIST_DELAY_MILLIS = 600000; // 10 minutes
	
	public static final boolean DEFAULT_COUNTDOWN_ENABLED = false;
	public static final int DEFAULT_COUNTDOWN_SECONDS = 900;
	
	public static final boolean DEFAULT_WHITELIST_ENABLE = false;
	
	public static final boolean DEFAULT_ENABLE_FRIENDLY_FIRE = false;
	public static final boolean DEFAULT_DISABLE_FIREWORK_DAMAGE = true;

	public static final boolean DEFAULT_AUTO_RESTART_ENABLE = true;
	public static final int DEFAULT_AUTO_RESTART_DELAY_SECONDS = 43200;
	
	public static final boolean DEFAULT_REMOVE_FILES_BEFORE_START = true;
	
	public static final boolean DEFAULT_ENABLE_KICK_IF_AFK = false;
	public static final long DEFAULT_KICK_IF_AFK_SECONDS = 180;
	
	public static final boolean DEFAULT_ENABLE_GAME_ROTATION = false;
	public static final boolean DEFAULT_ENABLE_AUTO_REBOOT_AFTER_END = true;
	public static final int DEFAULT_AUTO_REBOOT_AFTER_END_SECONDS_DELAY = 8;
	
	public static boolean DEFAULT_BROADCAST_BOOSTER_CHANGE = true;
	
	public static final boolean DEFAULT_ALLOW_JOIN_WHEN_RUNNING_GAME = false;
	public static final boolean DEFAULT_DISPLAY_HINT_RUNNING_GAME_ON_JOIN = true;
	
	public static String BOOSTERS_INVENTORY_OWN_NAME(String ownerName){ return "BOOSTER_OWN_INV_"+ownerName;}
	public static String BOOSTERS_INVENTORY_VIEWER_NAME(String ownerName){ return "BOOSTER_VIEWER_INV_"+ownerName;}
	
}
