package com.gmail.val59000mc.hcgameslib.common;

import java.util.logging.Logger;

public class Log {

	private static Logger logger;
	private static boolean debugNormal;
	private static boolean debugTasks;
	private static boolean debugSql;
	
	public static void setup(Logger alogger, boolean aDebugNormal, boolean aDebugTasks, boolean aDebugSql){
		logger = alogger;
		debugNormal = aDebugNormal;
		debugTasks = aDebugTasks;
		debugSql = aDebugSql;
	}
	
	public static void severe(String s){
		logger.severe(s);
	}
	
	public static void warn(String s){
		logger.warning(s);
	}
	
	public static void info(String s){
		logger.info(s);
	}
	
	public static void debug(String s){
		if(debugNormal)
			logger.info("[DEBUG] "+s);
	}

	public static void debugTasks(String s){
		if(debugTasks)
			logger.info("[DEBUG] "+s);
	}
	
	public static void debugSQL(String s){
		if(debugSql)
			logger.info("[DEBUG] "+s);
	}
}
