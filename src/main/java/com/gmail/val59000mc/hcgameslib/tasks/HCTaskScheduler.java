package com.gmail.val59000mc.hcgameslib.tasks;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public class HCTaskScheduler implements Runnable {

    public static final int TICKS_PER_SECOND = 20;

    /**
     * Duration in ticks before first execution
     * 0 by default
     */
    private int delay;

    /**
     * Duration in ticks between each execution
     * 20 ticks by default
     */
    private int interval;


    /**
     * Max iterations (-1 to disable)
     * -1 by default
     */
    private int iterations;

    /**
     * The first callback to execute. (null to disable)
     * null by default
     */
    private HCTask firstCallback;

    /**
     * The callback to execute at each iteration.
     * Must not be null.
     */
    private HCTask callback;


    /**
     * The callback to execute after all the iterations (null to disable)
     * null by default
     */
    private HCTask lastCallback;


    /**
     * The callback to execute when the task is stopped
     * null by default
     */
    private HCTask onStop;


    /**
     * Is the task scheduled to run for next iteration ?
     */
    private boolean running;

    /**
     * Should this tag produce task log
     * Automatically disable logging for fast tasks (<= 10 s)
     */
    private boolean logDebugTask;

    /**
     * Contains all of the tasks id created by the scheduler.
     * Used internally to be able to kill all remaining tasks
     */
    private Set<Integer> tasksIds;


    /**
     * Task description for debugging
     */
    private String summary;

    /**
     * Task name for debugging
     */
    private String name;

    /**
     * Game api
     */
    private HCGameAPI api;

    /**
     * Task listeners
     */
    private List<HCTaskListener> listeners;


    private HCTaskScheduler(final Builder builder) {
        this.api = builder.api;
        this.callback = builder.callback;
        this.name = builder.name;

        this.delay = builder.delay;
        this.interval = builder.interval;
        this.iterations = builder.iterations;
        this.firstCallback = builder.firstCallback;
        this.lastCallback = builder.lastCallback;
        this.onStop = builder.onStop;
        this.running = false;
        this.tasksIds = new HashSet<Integer>();
        this.listeners = builder.listeners;

        this.logDebugTask = this.interval >= 200;

        Preconditions.checkNotNull(callback, "callback must not be null");
        Preconditions.checkArgument(interval > 0, "interval must be > 0");
        Preconditions.checkArgument(iterations == -1 || iterations >= 1, "iterations must be == -1 or >= 1");
        Preconditions.checkArgument(delay >= 0, "delay must be >= 0");
        Preconditions.checkNotNull(name, "name cannot be null");

        this.summary = "Task '" + name + "' created on " + new Date().toString() + " ; " +
            "delay=" + delay + " ; " +
            "interval=" + interval + " ; " +
            "firstCallback=" + (firstCallback != null) + " ; " +
            "lastCallback=" + (lastCallback != null) + " ; ";

        callback.scheduler = this;
        if (firstCallback != null) firstCallback.scheduler = this;
        if (lastCallback != null) lastCallback.scheduler = this;
    }


    // Accessors

    public boolean isRunning() {
        return running;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }


    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }


    // Builder


    public static class Builder {

        private HCGameAPI api;
        private HCTask callback;

        private int delay;
        private int interval;
        private int iterations;
        private HCTask firstCallback;
        private HCTask lastCallback;
        private HCTask onStop;
        private String name;
        private List<HCTaskListener> listeners;

        public Builder(HCGameAPI api, String name, HCTask callback) {
            this.api = api;
            this.callback = callback;
            this.name = name;

            this.delay = 0;
            this.interval = 20;
            this.iterations = -1;
            this.firstCallback = null;
            this.lastCallback = null;
            this.onStop = null;
            this.listeners = new ArrayList<HCTaskListener>();
        }

        /**
         * Duration in ticks before first execution
         * 0 by default
         */
        public Builder withDelay(int delay) {
            this.delay = delay;
            return this;
        }

        /**
         * Duration in seconds before first execution
         * 0 by default
         */
        public Builder withSecondsDelay(int secondsDelay) {
            return withDelay(TICKS_PER_SECOND * secondsDelay);
        }

        /**
         * Duration in ticks between each execution
         * 20 ticks by default
         */
        public Builder withInterval(int interval) {
            this.interval = interval;
            return this;
        }

        /**
         * Duration in ticks between each execution
         * 20 ticks by default
         */
        public Builder withSecondsInterval(int secondsInterval) {
            return withInterval(TICKS_PER_SECOND * secondsInterval);
        }

        /**
         * Max iterations (-1 to disable)
         * -1 by default
         */
        public Builder withIterations(int iterations) {
            this.iterations = iterations;
            return this;
        }

        /**
         * The first callback to execute. (null to disable)
         * null by default
         */
        public Builder withFirstCallback(HCTask firstCallback) {
            this.firstCallback = firstCallback;
            return this;
        }

        public Builder withFirstCallback(IHCTask firstCallback) {
            this.firstCallback = new HCTask() {

                @Override
                public void run() {
                    firstCallback.run(this);
                }
            };
            return this;
        }

        /**
         * The callback to execute after all the iterations (null to disable)
         * 0 by default
         */
        public Builder withLastCallback(HCTask lastCallback) {
            this.lastCallback = lastCallback;
            return this;
        }

        public Builder withLastCallback(IHCTask lastCallback) {
            this.lastCallback = new HCTask() {

                @Override
                public void run() {
                    lastCallback.run(this);
                }
            };
            return this;
        }

        public Builder withOnStop(IHCTask onStop) {
            this.onStop = new HCTask() {

                @Override
                public void run() {
                    onStop.run(this);
                }
            };
            return this;
        }

        /**
         * Add a task listener to be auto started and stoped with the task
         *
         * @param listener
         * @return
         */
        public Builder addListener(HCTaskListener listener) {
            this.listeners.add(listener);
            return this;
        }

        /**
         * Add a list of task listeners to be auto started and stoped with the task
         *
         * @param listeners
         * @return
         */
        public Builder addListeners(List<HCTaskListener> listeners) {
            this.listeners.addAll(listeners);
            return this;
        }

        /**
         * Build the task sceduler
         *
         * @return
         */
        public HCTaskScheduler build() {
            return new HCTaskScheduler(this);
        }
    }

    /**
     * Start scheduled tasks
     */
    public void start() {
        this.running = true;
        Log.debugTasks("STARTING : " + summary);

        // Register scheduler
        ((HCGame) api).registerScheduler(this);

        // Register task listeners
        for (HCTaskListener listener : listeners) {
            listener.setTaskScheduler(this);
            api.registerListener(listener);
        }

        Bukkit.getScheduler().runTask(api.getPlugin(), this);
    }

    public void stop() {
        if (running) {

            if (onStop != null) {
                onStop.run();
            }

            this.running = false;
            Log.debugTasks("STOPPING : " + summary);

            // Unregister task listeners
            for (HCTaskListener listener : listeners) {
                api.unregisterListener(listener);
            }


            for (Integer id : tasksIds) {
                Bukkit.getScheduler().cancelTask(id);
            }
        }
    }

    @Override
    public void run() {

        if (!running) {
            if (logDebugTask) {
                Log.debugTasks("ABORTED : " + summary);
            }
            return;
        }

        if (firstCallback != null) {
            // Running first callback=
            if (logDebugTask) {
                Log.debugTasks("RUNNING firstcallback : " + summary);
            }
            BukkitTask task = Bukkit.getScheduler().runTaskLater(api.getPlugin(), firstCallback, delay);
            tasksIds.add(task.getTaskId());
            firstCallback = null;
        } else {
            if (iterations == -1 || iterations >= 1) {
                // Running normal callback
                if (logDebugTask) {
                    Log.debugTasks("RUNNING callback : " + summary);
                }
                BukkitTask task = Bukkit.getScheduler().runTaskLater(api.getPlugin(), callback, delay);
                tasksIds.add(task.getTaskId());
                if (iterations >= 1)
                    iterations--;
            } else {
                if (lastCallback != null) {
                    // Running last callback
                    if (logDebugTask) {
                        Log.debugTasks("RUNNING lastCallback : " + summary);
                    }
                    BukkitTask task = Bukkit.getScheduler().runTaskLater(api.getPlugin(), lastCallback, delay);
                    tasksIds.add(task.getTaskId());
                    lastCallback = null;
                } else {
                    stop();
                }
            }

        }

        Bukkit.getScheduler().runTaskLater(api.getPlugin(), this, delay + interval);

        delay = 0;

    }


}
