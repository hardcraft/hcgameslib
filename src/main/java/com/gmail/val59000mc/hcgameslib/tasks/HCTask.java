package com.gmail.val59000mc.hcgameslib.tasks;

public abstract class HCTask implements Runnable{

	protected HCTaskScheduler scheduler;
	
	public void stop(){
		scheduler.stop();
	}
	
	public HCTaskScheduler getScheduler(){
		return scheduler;
	}

}
