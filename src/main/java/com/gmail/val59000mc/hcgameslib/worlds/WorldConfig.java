package com.gmail.val59000mc.hcgameslib.worlds;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.google.common.base.Preconditions;

public class WorldConfig {

	/**
	 * The world where the game will be played
	 */
	private World world;
	
	/**
	 * The lobby where players arrive while waiting for start
	 */
	private Location lobby;
	
	/**
	 * The center of the map where spectators will be teleported when the game is running
	 */
	private Location center;
	
	/**
	 * Optional list of spawnpoints
	 */
	private List<Location> spawnpoints;
	
	/**
	 * An optional world border scenario
	 */
	private WorldBorderScenario wbScenario;
	


	private WorldConfig(Builder builder){
		Preconditions.checkNotNull(builder.world);
		Preconditions.checkNotNull(builder.lobby);
		Preconditions.checkNotNull(builder.center);
		
		this.world = builder.world;
		this.lobby = builder.lobby;
		this.center = builder.center;
		this.spawnpoints = builder.spawnpoints;
		this.wbScenario = builder.wbScenario;
	}
	
	public static class Builder{
		private World world;
		private Location lobby;
		private Location center;
		private List<Location> spawnpoints;
		private WorldBorderScenario wbScenario;
		
		public Builder(World world){
			this.world = world;
			this.lobby = world.getSpawnLocation();
			this.center = world.getSpawnLocation();
			this.spawnpoints = null;
			this.wbScenario = null;
		}
		
		public Builder withWorld(World world){
			this.world = world;
			return this;
		}
		
		public Builder withLobby(Location lobby){
			this.lobby = lobby;
			return this;
		}
		
		public Builder withCenter(Location center){
			this.center = center;
			return this;
		}
		
		public Builder withSpawnpoints(List<Location> spawnpoints){
			this.spawnpoints = spawnpoints;
			return this;
		}
		
		public Builder withWorldBorderScenario(WorldBorderScenario wbScenario){
			this.wbScenario = wbScenario;
			return this;
		}
		
		public WorldConfig build(){
			return new WorldConfig(this);
		}
	}
	
	private WorldConfig(World world, Location lobby, Location center) {
		super();
		this.world = world;
		this.lobby = lobby;
		this.center = center;
		this.wbScenario = null;
	}

	public World getWorld() {
		return world;
	}

	public Location getLobby() {
		return lobby;
	}

	public Location getCenter() {
		return center;
	}

	public List<Location> getSpawnpoints() {
		return spawnpoints;
	}

	public void startWorldBorderScenario(HCGameAPI api) {
		if(wbScenario != null){
			wbScenario.start((HCGame) api, world);
		}
	}

	public void nextWorldBorderbStep(HCGameAPI api) {
		if(wbScenario != null){
			wbScenario.next((HCGame) api, world);
		}
	}

	public void stopWorldBorderScenario(HCGameAPI api) {
		if(wbScenario != null){
			wbScenario.stop((HCGame) api, world);
		}
	}
	
	
	
}
