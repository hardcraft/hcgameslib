package com.gmail.val59000mc.hcgameslib.worlds;

import com.google.common.base.Preconditions;

public class WorldBorderStep {
	/**
	 * Start size
	 */
	private int start;
	
	/**
	 * End size
	 */
	private int end;
	
	/**
	 * Duration from start to end in seconds.
	 * Should be positive or 0
	 */
	private int duration;
	
	/**
	 * Center of the border along X axis
	 */
	private double centerX;
	
	/**
	 * Center of the border along Z axis
	 */
	private double centerZ;
	
	/**
	 * If true, will move automatically to next step.
	 * If false, will required a manual intervention to move to next step
	 */
	private boolean autoNext;
	
	public WorldBorderStep(int start, int end, double centerX, double centerZ, int duration, boolean autoNext) {
		Preconditions.checkArgument(start >= 0);
		Preconditions.checkArgument(end >= 0);
		Preconditions.checkArgument(duration >= 0);

		this.start = start;
		this.end = end;
		this.centerX = centerX;
		this.centerZ = centerZ;
		this.duration = duration;
		this.autoNext = autoNext;
		
	}
	

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public int getDuration() {
		return duration;
	}

	public boolean isAutoNext() {
		return autoNext;
	}

	public double getCenterX() {
		return centerX;
	}

	public double getCenterZ() {
		return centerZ;
	}	
	
}
