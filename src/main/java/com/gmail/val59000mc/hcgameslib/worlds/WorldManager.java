package com.gmail.val59000mc.hcgameslib.worlds;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.google.common.collect.Sets;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Set;


public class WorldManager {

    private HCGameAPI api;

    public WorldManager(HCGameAPI api) {
        this.api = api;
    }

    public HCGameAPI getApi() {
        return api;
    }

    public boolean deleteFiles(File file) {

        File[] flist = null;

        if (file == null || !file.exists()) {
            return false;
        }

        if (file.isFile()) {
            return file.delete();
        }

        if (!file.isDirectory()) {
            return false;
        }

        flist = file.listFiles();
        if (flist != null && flist.length > 0) {
            for (File f : flist) {
                if (!deleteFiles(f)) {
                    return false;
                }
            }
        }

        return file.delete();
    }


    public void deleteOldPlayersFiles(File file) {
        Log.debug("deleteOldPlayersFiles world=" + file.getName());
        if (Bukkit.getServer().getWorlds().size() > 0) {
            // Deleting old players files
            File playersData = new File(file, "playerdata");
            deleteFiles(playersData);
            playersData.mkdirs();

            // Deleting old players stats
            File playersStats = new File(file, "stats");
            deleteFiles(playersStats);
            playersStats.mkdirs();
        }
    }

    private void recursiveCopy(File fSource, File fDest, Set<String> ignoredFiles) {

        try {
            if (fSource.isDirectory()) {
                // A simple validation, if the destination is not exist then create it
                if (!fDest.exists()) {
                    fDest.mkdirs();
                }

                // Create list of files and directories on the current source
                // Note: with the recursion 'fSource' changed accordingly
                String[] fList = fSource.list();

                for (int index = 0; index < fList.length; index++) {
                    File dest = new File(fDest, fList[index]);
                    File source = new File(fSource, fList[index]);

                    // Recursion call take place here
                    recursiveCopy(source, dest, ignoredFiles);
                }
            } else {
                // Found a file. Copy it into the destination, which is already created in 'if' condition above
                if (ignoredFiles.contains(fSource.getName())) {
                    return;
                }


                // Open a file for read and write (copy)
                FileInputStream fInStream = new FileInputStream(fSource);
                FileOutputStream fOutStream = new FileOutputStream(fDest);

                // Read 2K at a time from the file
                byte[] buffer = new byte[2048];
                int iBytesReads;

                // In each successful read, write back to the source
                while ((iBytesReads = fInStream.read(buffer)) >= 0) {
                    fOutStream.write(buffer, 0, iBytesReads);
                }

                // Safe exit
                if (fInStream != null) {
                    fInStream.close();
                }

                if (fOutStream != null) {
                    fOutStream.close();
                }
            }
        } catch (Exception ex) {
            // Please handle all the relevant exceptions here
        }
    }

    public World loadOrCreateWorld(String name) {
        return Bukkit.getServer().createWorld(new WorldCreator(name));
    }

    public void copyFiles(File source, File dest, String... ignoredFiles) {
        if (source.exists()) {
            recursiveCopy(source, dest, Sets.newHashSet(ignoredFiles));
        } else {
            throw new IllegalArgumentException("Source path " + source.getName() + " doesn't exist");
        }
    }
}
