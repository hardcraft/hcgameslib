package com.gmail.val59000mc.hcgameslib.worlds;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.val59000mc.hcgameslib.events.HCNextWorldBorderStepEvent;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.google.common.base.Preconditions;


public class WorldBorderScenario {
	
	private List<WorldBorderStep> steps;
	private int currentStep;
	private int taskId;
	
	private WorldBorderScenario(Builder builder){
		Preconditions.checkArgument(builder.steps.size() > 0 ,"A WorldBorderScenario must have at least one step.");
		this.steps = builder.steps;
		this.currentStep = -1;
		this.taskId = -1;
	}
	
	public static class Builder{
		private List<WorldBorderStep> steps;
		
		public Builder(){
			this.steps = new ArrayList<WorldBorderStep>();
		}
		
		public Builder addStep(int start, int end, double centerX, double centerZ, int duration, boolean autoNext){
			this.steps.add(new WorldBorderStep(start, end, centerX, centerZ, duration, autoNext));
			return this;
		}
		
		public WorldBorderScenario build(){
			return new WorldBorderScenario(this);
		}
	}
	
	protected void start(HCGame game, World world){
		Preconditions.checkArgument(steps.size() > 0);
		next(game, world);
	}
	
	protected void stop(HCGame game, World world){
		if(taskId != -1){
			Bukkit.getScheduler().cancelTask(taskId);
			taskId = -1;
			WorldBorder wb = world.getWorldBorder();
			wb.setSize(wb.getSize());
		}
	}

	protected void next(HCGame game, World world) {
		int newStep = currentStep + 1;
		
		if(taskId != -1){
			// Force cancel task
			Bukkit.getScheduler().cancelTask(taskId);
			taskId = -1;
		}
		
		if(newStep < steps.size()){

			WorldBorderStep step = steps.get(newStep);
			
			HCNextWorldBorderStepEvent event = new HCNextWorldBorderStepEvent(game, step);
			Bukkit.getPluginManager().callEvent(event);
			if(!event.isCancelled()){
				currentStep = newStep;
				WorldBorder wb = world.getWorldBorder();
				wb.setSize(step.getStart());
				wb.setCenter(step.getCenterX(), step.getCenterZ());
				wb.setSize(step.getEnd(), step.getDuration());
				
				if(step.isAutoNext()){
					BukkitRunnable runnable = new BukkitRunnable() {
						
						@Override
						public void run() {
							taskId = -1;
							next(game, world);
						}
						
					};
					
					runnable.runTaskLater(game.getPlugin(), step.getDuration());
					taskId = runnable.getTaskId();
				}
			}
			
		}
	}
}
