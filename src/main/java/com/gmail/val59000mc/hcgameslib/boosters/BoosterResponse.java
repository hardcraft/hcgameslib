package com.gmail.val59000mc.hcgameslib.boosters;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;

public class BoosterResponse {

	private boolean success;
	private HCMessage message;
	private Booster booster;
	
	public BoosterResponse(boolean success, HCMessage message, Booster booster){
		this.success = success;
		this.message = message;
		this.booster = booster;
	}

	public boolean isSuccess() {
		return success;
	}

	public HCMessage getMessage() {
		return message;
	}

	public Booster getBooster() {
		return booster;
	}
	
	
}
