package com.gmail.val59000mc.hcgameslib.boosters;

import java.time.ZonedDateTime;
import java.util.UUID;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class Booster {
	private int id;
	private String playerName;
	private UUID playerUUID;
	private int playerId;
	private double multiplier;
	private int minutesDuration;
	private ZonedDateTime dateBought;
	private ZonedDateTime dateStart;
	private ZonedDateTime dateEnd;
	
	public Booster(int id, String playerName, UUID playerUUID, int playerId, double multiplier, int minutesDuration){
		this.id = id;
		this.playerName = playerName;
		this.playerUUID = playerUUID;
		this.playerId = playerId;
		this.multiplier = multiplier;
		this.minutesDuration = minutesDuration;
	}
	
	public int getId() {
		return id;
	}
	public String getPlayerName() {
		return playerName;
	}
	public UUID getPlayerUUID() {
		return playerUUID;
	}
	public int getPlayerId() {
		return playerId;
	}
	public double getMultiplier() {
		return multiplier;
	}
	public int getMinutesDuration() {
		return minutesDuration;
	}
	public ZonedDateTime getDateBought() {
		return dateBought;
	}
	public void setDateBought(ZonedDateTime dateBought) {
		this.dateBought = dateBought;
	}
	public ZonedDateTime getDateStart() {
		return dateStart;
	}
	public void setDateStart(ZonedDateTime dateStart) {
		this.dateStart = dateStart;
	}
	public ZonedDateTime getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(ZonedDateTime dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	@Override
    public boolean equals(Object obj) {
		if(obj instanceof Booster){
			return this.id == ((Booster) obj).id;
		}
        return false;
    }
	
	@Override
	public String toString(){
		return MoreObjects.toStringHelper(this.getClass())
			.add("id", id)
			.add("playerName", playerName)
			.add("playerUUID", playerUUID)
			.add("playerId", playerId)
			.add("multiplier", multiplier)
			.add("minutesDuration", minutesDuration)
			.add("dateBought", dateBought)
			.add("dateStart", dateStart)
			.add("dateEnd", dateEnd)
			.toString();
	}
	
	
}
