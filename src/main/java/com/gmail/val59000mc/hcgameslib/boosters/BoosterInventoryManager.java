package com.gmail.val59000mc.hcgameslib.boosters;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;

import java.util.List;

public class BoosterInventoryManager {

    private HCGameAPI api;

    public BoosterInventoryManager(HCGameAPI api) {
        this.api = api;
    }

    public void destroyInventory(String playerName) {
        closeAndDestroyInventory(Constants.BOOSTERS_INVENTORY_OWN_NAME(playerName));
        closeAndDestroyInventory(Constants.BOOSTERS_INVENTORY_VIEWER_NAME(playerName));
    }

    private void closeAndDestroyInventory(String invName) {
        InventoryManager manager = InventoryManager.instance();
        Inventory inventory = manager.getInventoryByName(invName);
        if (inventory == null) return;

        String title = inventory.getTitle();

        for (Player player : Bukkit.getOnlinePlayers()) {
            InventoryView view = player.getOpenInventory();
            if (view != null) {
                org.bukkit.inventory.Inventory inv = view.getTopInventory();
                if (inv != null && view.getTitle() != null && view.getTitle().equals(title)) {
                    inv.clear();
                    player.closeInventory();
                }
                manager.removeInventory(inventory);
            }
        }
    }

    public void createAndDisplay(String owner, HCPlayer displayTo, List<Booster> boostersList) {
        String invName = owner.equals(displayTo.getName())
            ? Constants.BOOSTERS_INVENTORY_OWN_NAME(owner)
            : Constants.BOOSTERS_INVENTORY_VIEWER_NAME(displayTo.getName());
        closeAndDestroyInventory(invName);
        BoosterInventory boosterInventory = new BoosterInventory(invName, api, owner, displayTo, boostersList);
        boosterInventory.buildInventory();

        if (displayTo.isOnline()) {
            SigPlayer sigPlayer = PlayersManager.getSigPlayer(displayTo.getPlayer());
            if (sigPlayer != null) {
                boosterInventory.getInventory().openInventory(displayTo.getPlayer(), sigPlayer);
            }
        }
    }

}
