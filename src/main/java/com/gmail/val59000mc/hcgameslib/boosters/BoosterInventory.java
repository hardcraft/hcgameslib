package com.gmail.val59000mc.hcgameslib.boosters;

import java.lang.reflect.Field;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.bukkit.Material;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.simpleinventorygui.actions.CloseInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.actions.SendMessageAction;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;
import com.google.common.collect.Lists;

public class BoosterInventory {

	private String invName;
	private Inventory inventory;
	private HCGameAPI api;
	private String owner;
	private HCPlayer displayTo;
	private List<Booster> boosters;
	private HCStringsAPI s;
	private DateTimeFormatter dateTimeFormat;
	
	public BoosterInventory(String invName, HCGameAPI api, String owner, HCPlayer displayTo,List<Booster> boosters){
		this.invName = invName;
		this.api = api;
		this.owner = owner;
		this.displayTo = displayTo;
		this.boosters = boosters;
		this.s = api.getStringsAPI();
		this.dateTimeFormat = new DateTimeFormatterBuilder()
				.appendPattern("d MMM à HH:mm")
				.toFormatter(Locale.FRANCE);
	}
	
	public void buildInventory(){
		
		String title = s("title").replace("%player%", owner);
		
		this.inventory = new Inventory(invName, title, 2);
		
		// help book
		Item help = new Item.ItemBuilder(Material.BOOK)
				.withName(s("help-name"))
				.withLore(Lists.newArrayList(s("help-lore")))
				.withPosition(0)
				.withRequireUniqueKey(true)
				.build();
		help.setActions(Lists.newArrayList(
			new SendMessageAction(s("help-link")),
			new CloseInventoryAction()
		));
		inventory.addItem(help);
		
		// best-booster
		Booster bestBooster = api.getBoostersAPI().getBestBooster();
		ItemBuilder best = preBuildItem(bestBooster);
		
		if(best != null){
			try {
				Field field = ItemBuilder.class.getDeclaredField("material");
				field.setAccessible(true);
				field.set(best, Material.GOLD_BLOCK);
			} catch (Exception e) {
			}
		}
		
		best.withName(s("best-booster-name"));
		best.withPosition(1);
		inventory.addItem(best.build());
		
		// player boosters
		Iterator<Booster> it = boosters.iterator();
		for(int i = 9 ; i < 18 ; i++){
			Booster booster = null;
			
			if(it.hasNext())
				booster = it.next();
			else
				booster = null;
			
			ItemBuilder boosterItemBuilder = preBuildItem(booster);
			boosterItemBuilder.withPosition(i);
			Item boosterItem = boosterItemBuilder.build();
			
			if(booster != null && booster.getDateStart() == null && displayTo.getName().equals(owner)){
				boosterItem.setActions(Lists.newArrayList(
					new UseBoosterAction(api, booster)
				));
			}
			
			inventory.addItem(boosterItem);
		}
		

	}
	
	private String s(String code){
		return s.get("messages.boosters.inventory."+code).toString();
	}
	
	private ItemBuilder preBuildItem(Booster booster){
		ItemBuilder item;
		if(booster == null){
			item = new Item.ItemBuilder(Material.STONE)
				.withName(s("no-booster-name"))
				.withLore(Lists.newArrayList(s("no-booster-lore")));
		}else{
			
			if(booster.getDateStart() == null){
				// not enabled yet
				item = new Item.ItemBuilder(Material.IRON_BLOCK)
					.withName(s("booster-name"))
					.withLore(Lists.newArrayList(
						s("not-enabled-booster-lore")
						.replace("%player%", booster.getPlayerName())
						.replace("%bought%", String.valueOf(formatDate(booster.getDateBought())))
						.replace("%multiplier%", String.valueOf(booster.getMultiplier()))
						.replace("%time%", String.valueOf(booster.getMinutesDuration()))
				));
			}else{
				// already enabled
				item = new Item.ItemBuilder(Material.EMERALD_BLOCK)
					.withName(s("booster-name"))
					.withLore(Lists.newArrayList(
						s("enabled-booster-lore")
						.replace("%player%", booster.getPlayerName())
						.replace("%bought%", String.valueOf(formatDate(booster.getDateBought())))
						.replace("%multiplier%", String.valueOf(booster.getMultiplier()))
						.replace("%time%", String.valueOf(booster.getMinutesDuration()))
						.replace("%start%", String.valueOf(formatDate(booster.getDateStart())))
						.replace("%end%", String.valueOf(formatDate(booster.getDateEnd())))
				));
			}
				
		}
		return item;
	}

	private String formatDate(ZonedDateTime date){
		return date.withZoneSameInstant(ZoneId.of("Europe/Paris")).format(dateTimeFormat);
	}
	
	public Inventory getInventory(){
		return inventory;
	}
}
