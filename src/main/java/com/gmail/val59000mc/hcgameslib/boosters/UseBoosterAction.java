package com.gmail.val59000mc.hcgameslib.boosters;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class UseBoosterAction extends Action{

	private HCGameAPI api;
	private Booster booster;
	
	public UseBoosterAction(HCGameAPI api, Booster booster) {
		super();
		this.api = api;
		this.booster = booster;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		HCPlayer hcPlayer = api.getPlayersManagerAPI().getHCPlayer(player);
		if(hcPlayer == null){
			executeNextAction(player, sigPlayer, false);
			return;
		}
		
		api.async(()->api.getBoostersAPI().activateBooster(booster), (responseActivate) -> {
			responseActivate.getMessage().sendChatP(hcPlayer);
			if(responseActivate.isSuccess()){
				api.async(()->api.getBoostersAPI().loadActiveBooster(hcPlayer), (responseLoad) -> {
					executeNextAction(player, sigPlayer, true);
				});
			}else{
				executeNextAction(player, sigPlayer, false);
			}
			player.closeInventory();
		});
	}

}
