package com.gmail.val59000mc.hcgameslib.boosters;

import java.util.List;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;

public class BoosterListResponse {

	private boolean success;
	private HCMessage message;
	private List<Booster> boosters;
	
	public BoosterListResponse(boolean success, HCMessage message, List<Booster> boosters){
		this.success = success;
		this.message = message;
		this.boosters = boosters;
	}

	public boolean isSuccess() {
		return success;
	}

	public HCMessage getMessage() {
		return message;
	}

	public List<Booster> getBoosters() {
		return boosters;
	}


	
}
