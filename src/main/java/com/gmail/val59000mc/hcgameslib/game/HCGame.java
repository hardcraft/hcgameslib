package com.gmail.val59000mc.hcgameslib.game;

import com.gmail.val59000mc.hcgameslib.HCGamesLib;
import com.gmail.val59000mc.hcgameslib.api.*;
import com.gmail.val59000mc.hcgameslib.api.impl.*;
import com.gmail.val59000mc.hcgameslib.commands.*;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.DummySqlImpl;
import com.gmail.val59000mc.hcgameslib.events.*;
import com.gmail.val59000mc.hcgameslib.listeners.*;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.hcgameslib.tasks.IHCTask;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.hcgameslib.worlds.WorldManager;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.function.Consumer;

public class HCGame implements HCGameAPI {

	// Customization
	private String name;
	private String lowercaseName;
	private String mysqlGameName;
	private JavaPlugin plugin;

	private File configFile;
	private File langFile;
	private FileConfiguration config;
	private FileConfiguration lang;

	// Internal
	private boolean isContinuous;
	private GameState state;
	private Set<HCListener> listeners;
	private Set<HCCommand> commands;
	private Set<HCTaskScheduler> schedulers;
	private PlayersManager pm;
	private WorldConfig worldConfig;
	private int remainingTimeBeforeStart;
	private int remainingTimeBeforeEnd;

	// APIS
	private HCStringsAPI strings;
	private HCBungeeAPI bungee;
	private HCSoundAPI sound;
	private HCPluginCallbacksAPI callbacks;
	private HCDependenciesAPI dependencies;
	private HCItemsAPI items;
	private HCBoostersAPI boosters;
	private HCDonationsAPI donations;
	private HCFlowsAPI flows;

	////////////////////////////////////////////////////////////////////////////////

	// Constructor

	private HCGame(Builder builder) {
		Preconditions.checkNotNull(builder.name, "Game name cannot be null");
		Preconditions.checkNotNull(builder.plugin, "Plugin reference cannot be null");
		Preconditions.checkNotNull(builder.configFile, "config file cannot be null");
		Preconditions.checkNotNull(builder.langFile, "lang file cannot be null");
		Preconditions.checkArgument(!builder.configFile.getAbsolutePath().equals(builder.langFile.getAbsolutePath()),
				"config gile and lang file cannot be the same file");
		Preconditions.checkNotNull(builder.listeners, "listeners list cannot be null");
		Preconditions.checkNotNull(builder.callbacks, "plugin's callbacks cannot be null");
		Preconditions.checkNotNull(builder.items, "items cannot be null");

		// Configuration
		this.name = builder.name;

		this.lowercaseName = Normalizer.normalize(ChatColor.stripColor(this.name).toLowerCase(), Normalizer.Form.NFD)
				.replaceAll("[^\\p{ASCII}]", "").replaceAll(" ", "");

		this.plugin = builder.plugin;

		// Loading config file
		this.configFile = builder.configFile;
		this.configFile.getParentFile().mkdirs();
		try {
			this.configFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.config = YamlConfiguration.loadConfiguration(configFile);
		saveDefaultConfig();

		// Loading lang file
		this.langFile = builder.langFile;
		this.langFile.getParentFile().mkdirs();
		try {
			this.langFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.lang = YamlConfiguration.loadConfiguration(langFile);
		saveDefaultLang();

		Log.setup(plugin.getLogger(), config.getBoolean("config.debug.normal", Constants.DEFAULT_DEBUG_STATE),
				config.getBoolean("config.debug.tasks", Constants.DEFAULT_DEBUG_STATE),
				config.getBoolean("config.debug.sql", Constants.DEFAULT_DEBUG_STATE));

		// APIS
		this.strings = new Strings(this);
		this.bungee = new BungeeSender(this);
		this.sound = new Sound(this);
		this.callbacks = builder.callbacks;
		this.items = builder.items;
		this.dependencies = new Dependencies(this);
		this.boosters = new Boosters(this);
		this.donations = new Donations(this);
		this.flows = new Flows(this);

		this.state = GameState.INITIALIZING;
		this.isContinuous = false;
		this.listeners = builder.listeners;
		this.commands = builder.commands;
		this.schedulers = new HashSet<HCTaskScheduler>();
		this.pm = new PlayersManager(this);
		this.remainingTimeBeforeStart = getTimeBeforeStart();
		this.remainingTimeBeforeEnd = getTimeBeforeEnd();

	}

	////////////////////////////////////////////////////////////////////////////////

	// Builder

	public static class Builder {

		// Configuration
		private String name;
		private JavaPlugin plugin;
		private File configFile;
		private File langFile;

		// Internal
		private Set<HCListener> listeners;
		private Set<HCCommand> commands;

		// APIs
		private HCPluginCallbacksAPI callbacks;
		private HCItemsAPI items;

		public Builder(String name, JavaPlugin plugin, File configFile, File langFile) {
			this.name = name;
			this.plugin = plugin;
			this.configFile = configFile;
			this.langFile = langFile;
			this.callbacks = new DefaultPluginCallbacks();
			this.items = new Items();
			this.listeners = getDefaultListeners();
			this.commands = getDefaultCommands();
		}

		private Set<HCCommand> getDefaultCommands() {
			Set<HCCommand> defaultCommands = new HashSet<HCCommand>();
			defaultCommands.add(new HCChatCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCStartCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCVanishCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCPlayCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCSpecCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCWhitelistCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCKickAllCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCSayCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCBoosterCommand(HCGamesLib.getPlugin()));
			defaultCommands.add(new HCDonationCommand(HCGamesLib.getPlugin()));
			return defaultCommands;
		}

		private Set<HCListener> getDefaultListeners() {
			Set<HCListener> defaultListeners = new HashSet<HCListener>();
			defaultListeners.add(new HCPlayerConnectionListener());
			defaultListeners.add(new HCChatListener());
			defaultListeners.add(new HCPingListener());
			defaultListeners.add(new HCPlayerDamageListener());
			defaultListeners.add(new HCPlayerDeathListener());
			defaultListeners.add(new HCMySQLListener());
			defaultListeners.add(new HCSIGListener());
			defaultListeners.add(new HCAFKListener());
			defaultListeners.add(new HCBoosterListener());
			defaultListeners.add(new HCRemoveLobbyListener());
			return defaultListeners;
		}

		public Builder withDefaultListenersAnd(Set<HCListener> listeners) {
			this.listeners.addAll(listeners);
			return this;
		}

		public Builder withListeners(Set<HCListener> listeners) {
			this.listeners = listeners;
			return this;
		}

		public Builder withDefaultCommandsAnd(Set<HCCommand> commands) {
			this.commands.addAll(commands);
			return this;
		}

		public Builder withCommands(Set<HCCommand> commands) {
			this.commands = commands;
			return this;
		}

		public Builder withPluginCallbacks(HCPluginCallbacksAPI callbacks) {
			this.callbacks = callbacks;
			return this;
		}

		public Builder withItems(HCItemsAPI items) {
			this.items = items;
			return this;
		}

		public HCGameAPI build() {
			return new HCGame(this);
		}
	}

	////////////////////////////////////////////////////////////////////////////////

	// HGGame

	/**
	 * Save default lang config
	 */
	private void saveDefaultLang() {

		lang.setDefaults(YamlConfiguration
				.loadConfiguration(new InputStreamReader(HCGamesLib.getPlugin().getResource("lang.yml"), Charset.forName("UTF-8"))));

		lang.options().copyDefaults(true);

		saveLang();
	}

	/**
	 * Save default game config
	 */
	private void saveDefaultConfig() {

		config.setDefaults(YamlConfiguration
				.loadConfiguration(new InputStreamReader(HCGamesLib.getPlugin().getResource("config.yml"), Charset.forName("UTF-8"))));

		config.options().copyDefaults(true);

		saveConfig();
	}

	/**
	 * @param command
	 */
	private void registerCommand(HCCommand command) {
		command.setApi(this);
		this.commands.add(command);
		command.getPlugin().getCommand(command.getName()).setExecutor(command);
		command.onRegister(this);
	}

	/**
	 * @param callbacks
	 */
	private void registerCallbacks(HCPluginCallbacksAPI callbacks) {
		callbacks.setApi(this);
	}

	/**
	 * Called just after load
	 */
	private void waitForPlayers() {
		if (state.equals(GameState.LOADING)) {
			Bukkit.getPluginManager().callEvent(new HCBeforeWaitEvent(this));

			this.state = GameState.WAITING;

			Bukkit.getPluginManager().callEvent(new HCGameStateChangeEvent(this, GameState.LOADING, GameState.WAITING));

			pm.startPreventFarAwayPlayersWaiting();

			pm.startPlayerTimeTask();

			getCallbacksApi().waitForPlayersMessages();

			Bukkit.getPluginManager().callEvent(new HCAfterWaitEvent(this));
		}
	}

	/**
	 * Called just after load for a continuous game
	 */
	private void continuousGame() {
		if (state.equals(GameState.LOADING)) {
			Bukkit.getPluginManager().callEvent(new HCBeforeContinuousEvent(this));

			this.state = GameState.CONTINUOUS;

			Bukkit.getPluginManager()
					.callEvent(new HCGameStateChangeEvent(this, GameState.LOADING, GameState.CONTINUOUS));

			pm.updatePlayersScoreboards();

			pm.startPreventFarAwayPlayersPlaying();

			pm.startPlayerTimeTask();

			Bukkit.getPluginManager().callEvent(new HCAfterContinuousEvent(this));
		}
	}

	/**
	 * Called just after start
	 */
	private void playGame() {
		if (state.equals(GameState.STARTING)) {
			Bukkit.getPluginManager().callEvent(new HCBeforePlayEvent(this));

			this.state = GameState.PLAYING;

			Bukkit.getPluginManager()
					.callEvent(new HCGameStateChangeEvent(this, GameState.STARTING, GameState.PLAYING));

			getCallbacksApi().playGameMessages();

			worldConfig.startWorldBorderScenario(this);

			pm.updatePlayersScoreboards();

			pm.startPreventFarAwayPlayersPlaying();

			pm.checkIfLastTeamOnline();

			Bukkit.getPluginManager().callEvent(new HCAfterPlayEvent(this));
		}
	}

	public FileConfiguration getLangConfig() {
		return lang;
	}

	public void saveLang() {
		try {
			lang.save(langFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int decreaseTimeBeforeStart() {
		if (this.remainingTimeBeforeStart > 0) {
			this.remainingTimeBeforeStart--;
		}
		return remainingTimeBeforeStart;
	}

	public int decreaseTimeBeforeEnd() {
		if (this.remainingTimeBeforeEnd > 0) {
			this.remainingTimeBeforeEnd--;
		}
		return remainingTimeBeforeEnd;
	}

	public void setRemainingTimeBeforeStart(int remainingTimeBeforeStart) {
		this.remainingTimeBeforeStart = remainingTimeBeforeStart;
	}

	public void registerScheduler(HCTaskScheduler hcTaskScheduler) {
		schedulers.add(hcTaskScheduler);
	}

	private void stopAllSchedulers() {
		for (HCTaskScheduler scheduler : schedulers) {
			scheduler.stop();
		}
	}

	////////////////////////////////////////////////////////////////////////////////

	// HCGameAPI

	/**
	 * Entry point Load a new game
	 */
	@Override
	public void loadGame() {

		if (state.equals(GameState.INITIALIZING)) {

			this.state = GameState.LOADING;

			// Remove files
			removeFilesBeforeStartup();

			// Register isContinuous game
			this.isContinuous = getConfig().getBoolean("config.continuous-game", Constants.DEFAULT_IS_CONTINUOUS_GAME);

			// Register Bungee channel
			if (getConfig().getBoolean("config.bungee.enabled", Constants.DEFAULT_BUNGEE_ENABLED)) {
				Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(getPlugin(), "BungeeCord");
			}

			// Register callbacks
			registerCallbacks(callbacks);

			callbacks.beforeLoad();

			// Registering listeners
			for (HCListener listener : listeners) {
				registerListener(listener);
			}

			Bukkit.getPluginManager()
					.callEvent(new HCGameStateChangeEvent(this, GameState.INITIALIZING, GameState.LOADING));

			// Registering commands
			for (HCCommand command : commands) {
				registerCommand(command);
			}

			// Load world
			this.worldConfig = getCallbacksApi().configureWorld(new WorldManager(this));

			// Load Players Manager
			pm.initializeTeams(getCallbacksApi().createTeams());
			
			// Load Flow Manager if enabled
			flows.randomizeNextGame();
			
			getItemsAPI().addStuffs(getCallbacksApi().createStuffs());

			startAutoRestartScheduler();

			Bukkit.getPluginManager().callEvent(new HCAfterLoadEvent(this));

			if (isContinuous()) {
				continuousGame();
			} else {
				waitForPlayers();
			}

		}
	}

	private void removeFilesBeforeStartup() {
		if (getConfig().getBoolean("config.remove-files-before-start.enable",
				Constants.DEFAULT_REMOVE_FILES_BEFORE_START)) {
			Log.debug("Removing log files ...");
			List<Map<?, ?>> paths = getConfig().getMapList("config.remove-files-before-start.paths");
			if (paths != null) {
				for (Map<?, ?> map : paths) {
					String path = (String) map.get("path");
					String extension = (String) map.get("extension");
					Integer olderThanDays = (Integer) map.get("older-than-days");
					if (path != null && extension != null && olderThanDays != null) {
						Log.debug("Removing files in '" + path + "' with extension '" + extension + "' and older than "
								+ olderThanDays + " days");
						File directory = new File(path);
						if (directory.exists() && directory.isDirectory()) {
							Log.debug("Directory '" + path + "' found");
							Long nowMinusDaysTimestamp = 1000
									* LocalDateTime.now().minusDays(olderThanDays).toEpochSecond(ZoneOffset.UTC);
							File[] files = directory.listFiles((file) -> {
								boolean hasExtension = Files.getFileExtension(file.getName())
										.equalsIgnoreCase(extension);
								boolean isOlderThanDays = file.lastModified() < nowMinusDaysTimestamp;
								return hasExtension && isOlderThanDays;
							});
							Log.debug(files.length + " files marked for removal");
							for (File file : files) {
								Log.debug("Removing file '" + file.getName() + "'");
								file.delete();
							}
						} else {
							Log.debug("Directory '" + path + "' not found");
						}
					}
				}
			}
		}

	}

	private void startAutoRestartScheduler() {
		if (getConfig().getBoolean("config.auto-restart.enable", Constants.DEFAULT_AUTO_RESTART_ENABLE)) {
			int delayTicksMin = 20 * getConfig().getInt("config.auto-restart.delay-seconds-min",
					Constants.DEFAULT_AUTO_RESTART_DELAY_SECONDS);
			int delayTicksMax = 20 * getConfig().getInt("config.auto-restart.delay-seconds-max",
					Constants.DEFAULT_AUTO_RESTART_DELAY_SECONDS);
			int delayTicks = Randoms.randomInteger(delayTicksMin, delayTicksMax);
			buildTask("auto-restart every " + delayTicks + " ticks", new HCTask() {

				@Override
				public void run() {
					int count = getPlayersManagerAPI().getPlayersCount(true, null);
					if (count == 0 && (is(GameState.WAITING) || is(GameState.CONTINUOUS))) {
						HCGame.this.state = GameState.ENDED;
						getCallbacksApi().rebootServer();
					}
				}
			}).withDelay(delayTicks).withInterval(delayTicks).build().start();
		}

	}

	/**
	 * Start or force start game
	 */
	@Override
	public void startGame() {
		if (state.equals(GameState.WAITING)) {
			Bukkit.getPluginManager().callEvent(new HCBeforeStartEvent(this));

			this.remainingTimeBeforeStart = 0;
			this.state = GameState.STARTING;

			Bukkit.getPluginManager()
					.callEvent(new HCGameStateChangeEvent(this, GameState.WAITING, GameState.STARTING));

			worldConfig.startWorldBorderScenario(this);

			pm.startAllPlayers();

			pm.startCountdownEndOfGame();

			getCallbacksApi().startGameMessages();

			Bukkit.getPluginManager().callEvent(new HCAfterStartEvent(this));

			playGame();
		}
	}

	@Override
	public void stopContinuousGame() {
		if (state.equals(GameState.CONTINUOUS)) {

			Bukkit.getPluginManager().callEvent(new HCBeforeStopContinuousEvent(this));

			this.state = GameState.ENDED;

			Bukkit.getPluginManager()
					.callEvent(new HCGameStateChangeEvent(this, GameState.CONTINUOUS, GameState.ENDED));

			worldConfig.stopWorldBorderScenario(this);

			pm.persistAllOnlinePlayersSessions();

			pm.updatePlayersScoreboards();

			stopAllSchedulers();

			Bukkit.getPluginManager().callEvent(new HCAfterStopContinuousEvent(this));

			getCallbacksApi().rebootServer();
		}

	}

	@Override
	public void endGame(HCTeam winningTeam) {
		if (state.equals(GameState.PLAYING)) {

			Bukkit.getPluginManager().callEvent(new HCBeforeEndEvent(this));

			this.state = GameState.ENDED;

			pm.setWinningTeam(winningTeam);

			Bukkit.getPluginManager().callEvent(new HCGameStateChangeEvent(this, GameState.PLAYING, GameState.ENDED));

			worldConfig.stopWorldBorderScenario(this);

			pm.endAllPlayers();

			pm.updatePlayersScoreboards();

			stopAllSchedulers();

			Bukkit.getPluginManager().callEvent(new HCAfterEndEvent(this));

			getCallbacksApi().rebootServer();

		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends HCCommand> T getRegisteredCommand(Class<T> clazz){
		for(HCCommand command : commands){
			if(command.getClass().equals(clazz)){
				return (T) command;
			}
		}
		return null;
	}
	
	/**
	 * @param listener
	 */
	@Override
	public void registerListener(HCListener listener) {
		listener.setApi(this);
		this.listeners.add(listener);
		Bukkit.getPluginManager().registerEvents(listener, plugin);
	}

	/**
	 * Unregister listener and remove it from internal list
	 * 
	 * @param listener
	 */
	@Override
	public void unregisterListener(HCListener listener) {
		HandlerList.unregisterAll(listener);
		this.listeners.remove(listener);
	}

	@Override
	public WorldConfig getWorldConfig() {
		return worldConfig;
	}

	@Override
	public HCPlayersManagerAPI getPlayersManagerAPI() {
		return pm;
	}

	@Override
	public HCBoostersAPI getBoostersAPI() {
		return boosters;
	}

	@Override
	public HCDonationsAPI getDonationsAPI(){
		return donations;
	}

	@Override
	public HCFlowsAPI getFlowsAPI() {
		return flows;
	}

	@Override
	public HCStringsAPI getStringsAPI() {
		return strings;
	}

	@Override
	public HCSoundAPI getSoundAPI() {
		return sound;
	}

	@Override
	public HCBungeeAPI getBungeeAPI() {
		return bungee;
	}

	@Override
	public HCDependenciesAPI getDependencies() {
		return dependencies;
	}

	@Override
	public HCItemsAPI getItemsAPI() {
		return items;
	}

	@Override
	public HCMySQLAPI getMySQLAPI() {
		return new DummySqlImpl();
	}

	@Override
	public JavaPlugin getPlugin() {
		return plugin;
	}

	@Override
	public GameState getGameState() {
		return state;
	}

	@Override
	public int getTimeBeforeStart() {
		return getConfig().getInt("config.start-scheduler.time-before-start", Constants.DEFAULT_SECONDS_TO_START);
	}

	@Override
	public int getTimeBeforeEnd() {
		return getConfig().getInt("config.countdown-end-of-game.seconds", Constants.DEFAULT_COUNTDOWN_SECONDS);
	}
	
	@Override
	public boolean isCountdownEndOfGameEnabled() {
		return getConfig().getBoolean("config.countdown-end-of-game.enable", Constants.DEFAULT_COUNTDOWN_ENABLED);
	}

	@Override
	public int getRemainingTimeBeforeStart() {
		return remainingTimeBeforeStart;
	}

	@Override
	public int getRemainingTimeBeforeEnd() {
		return remainingTimeBeforeEnd;
	}

	@Override
	public HCPluginCallbacksAPI getCallbacksApi() {
		return callbacks;
	}

	@Override
	public FileConfiguration getConfig() {
		return config;
	}

	@Override
	public void saveConfig() {
		try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getLowercaseName() {
		return lowercaseName;
	}

	@Override
	public String getMySQLGameName() {
		return mysqlGameName;
	}

	@Override
	public HCTaskScheduler.Builder buildTask(String name, HCTask callback) {
		return new HCTaskScheduler.Builder(this, name, callback);
	}

	@Override
	public HCTaskScheduler.Builder buildTask(String name, IHCTask callback) {
		return new HCTaskScheduler.Builder(this, name, new HCTask() {
			
			@Override
			public void run() {
				callback.run(this);
			}
		});
	}

	@Override
	public boolean is(GameState state) {
		return this.state.equals(state);
	}

	@Override
	public boolean isContinuous() {
		return isContinuous;
	}

	@Override
	public void sync(Runnable runnable) {
		getPlugin().getServer().getScheduler().runTask(getPlugin(), runnable);
	}

	@Override
	public void sync(Runnable runnable, int delayTicks) {
		getPlugin().getServer().getScheduler().runTaskLater(getPlugin(), runnable, delayTicks);
	}

	@Override
	public void async(Runnable runnable) {
		getPlugin().getServer().getScheduler().runTaskAsynchronously(getPlugin(), runnable);
	}

	@Override
	public void async(Runnable runnable, int delayTicks) {
		getPlugin().getServer().getScheduler().runTaskLaterAsynchronously(getPlugin(), runnable, delayTicks);
	}
	
	@Override
    public <T> void async(Callable<T> callable, Consumer<T> consumer) {
		FutureTask<T> futureTask = new FutureTask<>(callable);
	     
        async(()-> {
         
			try {
				T result = callable.call();
	        	sync(()->consumer.accept(result));
			} catch (Exception e) {
				Logger.severe("Could not perfom callback");
				e.printStackTrace();
			}
        	
        });
    }

	@Override
    public <T> void async(Callable<T> callable, Consumer<T> consumer, int delayTicks) {
        FutureTask<T> futureTask = new FutureTask<>(callable);
     
        async(()-> {
         
			try {
				T result = callable.call();
	        	sync(()->consumer.accept(result));
			} catch (Exception e) {
				Logger.severe("Could not perfom callback");
				e.printStackTrace();
			}
        	
        }, delayTicks);
    }
	
	@Override
	public <T extends Event> T callEvent(T event){
		getPlugin().getServer().getPluginManager().callEvent(event);
		return event;
	}

}
