package com.gmail.val59000mc.hcgameslib.game;

public enum GameState {
	INITIALIZING,
	LOADING,
	WAITING,
	STARTING,
	PLAYING,
	ENDED,
	
	CONTINUOUS;
}
