package com.gmail.val59000mc.hcgameslib.util;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Util {

	public static boolean isPluginLoaded(String plugin){
		JavaPlugin pl = (JavaPlugin) Bukkit.getPluginManager().getPlugin(plugin);
		return pl != null;
	}
}
