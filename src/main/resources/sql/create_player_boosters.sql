CREATE TABLE IF NOT EXISTS `player_boosters` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `player_id` INT(11) NOT NULL,
  `multiplier` DECIMAL(5,2) NOT NULL,
  `minutes_duration` INT(11) NOT NULL,
  `date_bought` DATETIME NOT NULL,
  `date_start` DATETIME NULL DEFAULT NULL,
  `date_end` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_boosters_player_id_idx` (`player_id` ASC),
  CONSTRAINT `fk_boosters_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;