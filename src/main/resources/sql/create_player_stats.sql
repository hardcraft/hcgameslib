CREATE TABLE IF NOT EXISTS `player_stats` (
  `player_id` INT(11) NOT NULL,
  `month` TINYINT(4) NOT NULL DEFAULT 1,
  `year` SMALLINT(6) NOT NULL DEFAULT 1970,
  `play_time` INT(11) NOT NULL DEFAULT 0,
  `kills` INT(11) NOT NULL DEFAULT 0,
  `deaths` INT(11) NOT NULL DEFAULT 0,
  `coins_earned` DECIMAL(19,4) NOT NULL DEFAULT 0,
  `wins` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`, `month`, `year`),
  CONSTRAINT `fk_player_stats_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;